/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// TGUI - Texus's Graphical User Interface
// Copyright (C) 2012-2015 Bruno Van de Velde (vdv_b@tgui.eu)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <TGUI/Transformable.hpp>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace tgui
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Transformable::Transformable(const Transformable& copy)
    {
        setPosition(copy.m_position);
        setSize(copy.m_size);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Transformable& Transformable::operator=(const Transformable& right)
    {
        if (this != &right)
        {
            setPosition(right.m_position);
            setSize(right.m_size);
        }

        return *this;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Transformable::setPosition(const Layout& position)
    {
        m_position = position;
        m_position.setCallbackFunction(std::bind(&Transformable::updatePosition, this, false));
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Transformable::move(const Layout& offset)
    {
        setPosition(m_position + offset);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Transformable::move(const Layout1d& x, const Layout1d& y)
    {
        setPosition({m_position.x + x, m_position.y + y});
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Transformable::setSize(const Layout& size)
    {
        m_size = size;
        m_size.recalculateValue();

        if (m_size.getValue().x < 0)
        {
            m_size.x = m_size.x * -1;
            m_size.recalculateValue();
        }

        if (m_size.getValue().y < 0)
        {
            m_size.y = m_size.y * -1;
            m_size.recalculateValue();
        }

        m_size.setCallbackFunction(std::bind(&Transformable::updateSize, this, false));
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Transformable::scale(const Layout& factors)
    {
        setSize({m_size.x * factors.x, m_size.y * factors.y});
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Transformable::scale(const Layout1d& x, const Layout1d& y)
    {
        setSize({m_size.x * x, m_size.y * y});
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Transformable::updatePosition(bool forceUpdate)
    {
        if (forceUpdate)
        {
            m_position.recalculateValue();
            setPosition(m_position);
        }
        else
        {
            sf::Vector2f oldPosition = getPosition();

            m_position.recalculateValue();

            if (oldPosition != getPosition())
                setPosition(m_position);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Transformable::updateSize(bool forceUpdate)
    {
        if (forceUpdate)
        {
            m_size.recalculateValue();
            setSize(m_size);
        }
        else
        {
            sf::Vector2f oldSize = getSize();

            m_size.recalculateValue();

            if (oldSize != getSize())
                setSize(m_size);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
