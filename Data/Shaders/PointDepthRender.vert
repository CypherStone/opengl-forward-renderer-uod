#version 430

layout(location=0) in vec3 inPosition;

uniform mat4 depthM;
uniform mat4 depthMVP;

out vec3 worldPosition;

void main(void)
{
	gl_Position = depthMVP * vec4(inPosition, 1.0f);
	
    worldPosition = (depthM * vec4(inPosition, 1.0f)).xyz;    
}
