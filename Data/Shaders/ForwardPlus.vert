#version 430

layout(location=0) in vec3 inPosition;
layout(location=1) in vec3 inNormal;
layout(location=2) in vec3 inTangent;
layout(location=3) in vec3 inBinormal;
layout(location=4) in vec2 inTexCoord;

out vec3 position;
out vec3 normal;
out vec3 tangent;
out vec3 binormal;
out vec2 texCoord;

uniform mat4 modelMatrix;
uniform mat4 projectionViewMatrix;

void main(void)
{
	position = (modelMatrix * vec4(inPosition, 1.0f)).xyz;
	normal = normalize(modelMatrix * vec4(inNormal, 0.0f)).xyz;
	tangent = normalize(modelMatrix * vec4(inTangent, 0.0f)).xyz;
	binormal = normalize(modelMatrix * vec4(inBinormal, 0.0f)).xyz;
	texCoord = inTexCoord;
	
	gl_Position = projectionViewMatrix * vec4(position, 1.0f);
}