#version 430
#define TILE_RES 16
#define LIGHT_INDEX_BUFFER_SENTINEL 0x7fffffff

struct PointLight {
	vec4 colour;
	vec4 position;
};

layout (r32ui, binding = 1) uniform uimageBuffer perTileLightIndexBuffer;

layout(binding = 3, std140) buffer PointLightBuffer {
    PointLight pointLights[];
};

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform sampler2D specularTexture;
uniform sampler2D opacityTexture;

in vec3 position;
in vec3 normal;
in vec3 tangent;
in vec3 binormal;
in vec2 texCoord;

uniform vec2 windowSize;
uniform uint maxNumLightsPerTile;
uniform vec3 cameraPosition;

uniform vec3 globalAmbient = vec3(0.1f);
uniform float bias = 0.000001f;

out vec4 finalColor;

uint getLightCountInTile(uint tileIndex)
{
    uint lightsInTile = 0;
    uint index = maxNumLightsPerTile * tileIndex;
    uint nextLightIndex = imageLoad(perTileLightIndexBuffer, int(index));

    while (nextLightIndex != LIGHT_INDEX_BUFFER_SENTINEL)
    {
        lightsInTile++;
        index++;
        nextLightIndex = imageLoad(perTileLightIndexBuffer, int(index));
    }

    return lightsInTile;
}

uint getTileIndex(in vec2 screenPosition)
{
    float tileResolution = float(TILE_RES);
    uint cellXCount = uint((windowSize.x + TILE_RES - 1) / TILE_RES);
    uint tileIndex = uint(floor(screenPosition.x / tileResolution) + floor(screenPosition.y / tileResolution) * cellXCount);
    return tileIndex;
}

float getAttenuation(in float distance, in float radius)
{
	/* 
		Attenuation formula
		Constant + (Linear * Distance) + (Quadratic * (Distance * Distance)) 
	*/
	float x = distance / radius;
	return -0.05f + 1.05f / (1.0f + 20.0f * x * x);
}

float calculateSpecularFactor(in vec3 viewDirection, in vec3 normal, in vec3 lightDirection, float specularIntensity)
{
	return pow(clamp(dot(normalize(viewDirection + lightDirection), normal), 0.0f, 1.0f), specularIntensity);
}

vec3 calculateLightDirectionDistance(in vec3 lightPosition, in vec3 worldPosition, out float lightDistance)
{
	vec3 lightDirection = lightPosition - worldPosition;
	lightDistance = length(lightDirection);
	return normalize(lightDirection);
}

void calculateBlinnPhongPointLight(in vec3 viewDirection, in vec3 normal, in vec3 lightColor, in vec3 lightDirection, out vec3 lightDiffuse, out vec3 lightSpecular)
{		
	lightDiffuse = lightColor * clamp(dot(normal, lightDirection), 0.0f, 1.0f);
	lightSpecular = lightColor * calculateSpecularFactor(viewDirection, normal, lightDirection, 32.0f);
}

float beckmannSpecular(in float x, in float roughness)
{
	float NdotH = max(x, 0.01f);
	float cos2Alpha = NdotH * NdotH;
	float tan2Alpha = (cos2Alpha - 1.0f) / cos2Alpha;
	float roughnessSQ = roughness * roughness;
	float denominator = 3.14f * roughnessSQ * cos2Alpha * cos2Alpha;
	
	return exp(tan2Alpha / roughnessSQ) / denominator;
}

float cookTorranceSpecular(in vec3 viewDirection, in vec3 inNormal, in vec3 lightDirection, in float roughness, in float fresnel)
{
	float VdotN = max(dot(viewDirection, inNormal), 0.0f);
	float LdotN = max(dot(lightDirection, inNormal), 0.0f);

	vec3 halfAngle = normalize(lightDirection + viewDirection);

	float NdotH = max(dot(inNormal, halfAngle), 0.0f);
	float VdotH = max(dot(viewDirection, halfAngle), 0.01f);
	float LdotH = max(dot(lightDirection, halfAngle), 0.01f);
	float G1 = (2.0f * NdotH * VdotN) / VdotH;
	float G2 = (2.0f * NdotH * LdotN) / LdotH;
	float G = min(1.0f, min(G1, G2));

	float D = beckmannSpecular(NdotH, roughness);

	float F = pow(1.0f - VdotN, fresnel);

	return  G * F * D / max(3.14f * VdotN, 0.01f);
}

void calculateCookTorrancePointLight(in vec3 viewDirection, in vec3 inNormal, in vec3 lightColor, in vec3 lightDirection, out vec3 lightDiffuse, out vec3 lightSpecular)
{		
	lightDiffuse = lightColor * clamp(dot(inNormal, lightDirection), 0.0f, 1.0f);
	lightSpecular = lightColor * cookTorranceSpecular(viewDirection, inNormal, lightDirection, 0.5f, 0.1f);
}

void main(void)
{
	float alpha = texture(opacityTexture, texCoord).r;
	if (alpha < 0.01f)
	{
		finalColor = vec4(0.0f);
		return;
	}
	
	vec3 lightDiffuseAccumulation = vec3(0.0f);
	vec3 lightSpecularAccumulation = vec3(0.0f);
	
	vec3 diffuseBuffer = texture(diffuseTexture, texCoord).rgb;
	vec3 normalBuffer = (texture(normalTexture, texCoord).rgb * 2.0f) - vec3(1.0f);
	vec3 specularBuffer = texture(specularTexture, texCoord).rgb;
	vec3 viewDirection = normalize(cameraPosition - position);
	
	normalBuffer = normalize(normal + normalBuffer.x * tangent + normalBuffer.y * binormal);
	
	uint tileIndex = getTileIndex(gl_FragCoord.xy);
	uint index = maxNumLightsPerTile * tileIndex;
	uint lightIndex = imageLoad(perTileLightIndexBuffer, int(index));

	while (lightIndex != LIGHT_INDEX_BUFFER_SENTINEL)
	{
		float lightDistance = 0.0f;
		vec3 lightDiffuse = vec3(0.0f);
		vec3 lightSpecular = vec3(0.0f);
		vec4 lightPosition = pointLights[lightIndex].position;
		vec3 lightDirection = calculateLightDirectionDistance(lightPosition.xyz, position, lightDistance); 
		float radius = lightPosition.w;
		
		if (lightDistance < radius)
		{
			//calculateBlinnPhongPointLight(viewDirection, normalBuffer, pointLights[lightIndex].colour.xyz, lightDirection, lightDiffuse, lightSpecular);
			calculateCookTorrancePointLight(viewDirection, normalBuffer, pointLights[lightIndex].colour.xyz, lightDirection, lightDiffuse, lightSpecular);
			
			const float falloff = getAttenuation(lightDistance, radius);
			lightDiffuse *= falloff;
			lightSpecular *= falloff;
		}
				
		lightDiffuseAccumulation += lightDiffuse;
		lightSpecularAccumulation += lightSpecular;
		
		index++;
		lightIndex = imageLoad(perTileLightIndexBuffer, int(index));	
	}
	
	lightDiffuseAccumulation *= 2;
	lightSpecularAccumulation *= 8;
	
	float ambientBlend = 0.5f * normalBuffer.y + 0.5f;
    vec3 ambientCubemap = vec3(0.01f) * ambientBlend + vec3(0.15f) * (1.0f - ambientBlend);
	
	vec3 ambientDiffuse = ambientCubemap + lightDiffuseAccumulation;
	finalColor = vec4(diffuseBuffer * (ambientDiffuse + lightSpecularAccumulation * specularBuffer), alpha);
}



