#version 430
#define MAX_DIRECTIONAL_LIGHTS 150
#define MAX_SPOT_LIGHTS 150
#define MAX_POINT_LIGHTS 150

struct DirectionalLight {
	mat4 shadowMatrix;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec3 direction;
	float specularIntensity;
};

struct PointLight {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec3 attenuation;
	float specularIntensity;
};

struct SpotLight {
	mat4 shadowMatrix;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec4 direction;
	vec4 attenuation;
	vec3 cone;
	float specularIntensity;
};

layout(binding = 0, std140) buffer PointLightBuffer {
    PointLight pointLights[];
};

layout(binding = 1, std140) buffer SpotLightBuffer {
    SpotLight spotLights[];
};

layout(binding = 2, std140) buffer DirectionalLightBuffer {
    DirectionalLight directionalLights[];
};

in vec2 textureCoords;

uniform sampler2D positionTarget;
uniform sampler2D diffuseTarget; 
uniform sampler2D normalsTarget;
uniform sampler2D specularTarget;

uniform sampler2DShadow directionalLightShadows[MAX_DIRECTIONAL_LIGHTS];
uniform sampler2DShadow spotLightShadows[MAX_SPOT_LIGHTS];
uniform samplerCubeShadow pointLightShadows[MAX_POINT_LIGHTS];

uniform mat4 cameraViewMatrix;
uniform vec3 cameraPosition;

uniform vec3 globalAmbient = vec3(0.15f);
uniform float bias = 0.000001f;
uniform int directionalLightCount = 0;
uniform int pointLightCount = 0;
uniform int spotLightCount = 0;
uniform int shadowSamples = 1;

out vec4 finalColor;

/* -------------------- Lighting Functions ----------------------- */

float calculateSpecularFactor(in vec3 worldPosition, in vec3 normal, in vec3 lightDirection, float specularIntensity)
{
	const vec3 vertexToEye = normalize(cameraPosition - worldPosition);
	const vec3 lightReflect = normalize(reflect(-lightDirection, normal));
	return pow(dot(vertexToEye, lightReflect), specularIntensity);
}

vec3 calculateLightDirectionDistance(in vec3 lightPosition, in vec3 worldPosition, out float lightDistance)
{
	vec3 lightDirection = lightPosition - worldPosition;
	lightDistance = length(lightDirection);
	return normalize(lightDirection);
}

float getAttenuation(in float distance, in vec3 attenuation)
{
	/* 
		Attenuation formula
		Constant + (Linear * Distance) + (Quadratic * (Distance * Distance)) 
	*/
	return attenuation.x + (attenuation.y * distance) + (attenuation.z * (distance * distance));
}


/* -------------------- Shadow Functions -------------------- */

vec3 getScreenSpaceProjection(in mat4 viewProjection, in vec3 position)
{
	mat4 inverseCameraView = cameraViewMatrix * inverse(cameraViewMatrix);
	vec4 projection = (viewProjection * inverseCameraView) * vec4(position, 1.0f);
	projection.xyz /= projection.w;
	projection.z += bias;
	
	return projection.xyz;
}

vec3 getShadowOffset(in vec3 projection, in vec2 size, int x, int y)
{
	return vec3(projection.xy + vec2(x * size.x, y * size.y), projection.z);
}

float calculateSpotLightShadow(in vec3 position, in int i)
{
	vec2 shadowSize = textureSize(spotLightShadows[i], 0);
	shadowSize = vec2(1.0f / shadowSize.x,  1.0f / shadowSize.y);
	vec3 projection = getScreenSpaceProjection(spotLights[i].shadowMatrix, position); 
	
	if (shadowSamples < 0)
	{
		return texture(spotLightShadows[i], getShadowOffset(projection, shadowSize, 0, 0));
	}
	
	int maxSamples = 0;
	float visibility = 0.0f;
	
    for (int y = -shadowSamples; y <= shadowSamples; y++)
	{
        for (int x = -shadowSamples; x <= shadowSamples; x++) 
		{
			maxSamples++;
            visibility += texture(spotLightShadows[i], getShadowOffset(projection, shadowSize, x, y));
        }
    }

	return clamp((visibility / maxSamples), 0.0f, 1.0f);
}

float calculatePointLightShadow(in vec3 position, in int i)
{
	vec2 shadowSize = textureSize(pointLightShadows[i], 0);
	vec3 lightDirection = pointLights[i].position.xyz - position;
	lightDirection.y = -lightDirection.y;
	vec4 cubecoord = vec4(lightDirection, max(max(abs(lightDirection.x), abs(lightDirection.y)), abs(lightDirection.z)));
	
	float zFar = 10000.0f;
	float zNear = 0.1f;
	float first = zFar / ( zFar - zNear );
	float second = zFar * zNear / ( zNear - zFar );
	cubecoord.w = first + second / cubecoord.w;

	return texture(pointLightShadows[i], cubecoord);
}

float calculateDirectionalLightShadow(in vec3 position, in int i)
{
		vec2 shadowSize = textureSize(directionalLightShadows[i], 0);
	vec3 projection = getScreenSpaceProjection(directionalLights[i].shadowMatrix, position); 

	if (shadowSamples < 0)
	{
		return texture(directionalLightShadows[i], getShadowOffset(projection, shadowSize, 0, 0));
	}
	
	int maxSamples = 0;
	float visibility = 0.0f;
	
    for (int y = -shadowSamples; y <= shadowSamples; y++)
	{
        for (int x = -shadowSamples; x <= shadowSamples; x++) 
		{
			maxSamples++;
            visibility += texture(directionalLightShadows[i], getShadowOffset(projection, shadowSize, x, y));
        }
    }
	
	return clamp((visibility / maxSamples), 0.0f, 1.0f);
}

/* -------------------- Directional Light ----------------------- */

vec3 CalculateDirectionalLight(in vec3 worldPosition, in vec3 normal, in vec3 inDiffuse, in vec3 inSpecular, in int i)
{
	float shadow = calculateDirectionalLightShadow(worldPosition, i);
	
    vec3 ambient = directionalLights[i].ambient.xyz;
	vec3 diffuse = vec3(0.0f, 0.0f, 0.0f);
	vec3 specular = vec3(0.0f, 0.0f, 0.0f);
	
    const float diffuseFactor = dot(normal, -directionalLights[i].direction);
	
    if (diffuseFactor > 0) 
	{
        diffuse = directionalLights[i].diffuse.xyz * diffuseFactor * shadow;
		float specularFactor = calculateSpecularFactor(worldPosition, normal, -directionalLights[i].direction, directionalLights[i].specularIntensity);
		
        if (specularFactor > 0) 
		{
            specular = directionalLights[i].specular.xyz * inSpecular * specularFactor * shadow;
        }
    }

    return ambient + diffuse + specular;
}

/* -------------------- Point Light ----------------------- */

vec3 CalculatePointLight(in vec3 worldPosition, in vec3 normal, in vec3 inDiffuse, in vec3 inSpecular, in int i)
{	
	float lightDistance = 0.0f;
	vec3 lightDirection = calculateLightDirectionDistance(pointLights[i].position.xyz, worldPosition, lightDistance);
	
	vec3 ambient = pointLights[i].ambient.xyz;
	vec3 diffuse = vec3(0.0f, 0.0f, 0.0f);
	vec3 specular = vec3(0.0f, 0.0f, 0.0f);

	float shadow = 1.0f;

	const float diffuseFactor = dot(normal, lightDirection);
		
	if (diffuseFactor > 0) 
	{
		shadow = shadow = calculatePointLightShadow(worldPosition, i);
		diffuse = pointLights[i].diffuse.xyz * diffuseFactor;
		float specularFactor = calculateSpecularFactor(worldPosition, normal, lightDirection, pointLights[i].specularIntensity);
		
		if (specularFactor > 0) 
		{
			specular = inSpecular * pointLights[i].specular.xyz * specularFactor;
		}
	}
	
	return (ambient + ((diffuse + specular) * shadow)) / getAttenuation(lightDistance, pointLights[i].attenuation);
}

/* -------------------- Spot Light ----------------------- */

vec3 CalculateSpotLight(in vec3 worldPosition, in vec3 normal, in vec3 inDiffuse, in vec3 inSpecular, in int i)
{	
	float lightDistance = 0.0f;
	vec3 lightDirection = calculateLightDirectionDistance(spotLights[i].position.xyz, worldPosition, lightDistance);
	const float coneAngle = dot(spotLights[i].direction.xyz, lightDirection);
   	
	if(coneAngle > spotLights[i].cone.x)
	{		
		float shadow = calculateSpotLightShadow(worldPosition, i);
		
		vec3 ambient = spotLights[i].ambient.xyz;
		vec3 diffuse = vec3(0.0f, 0.0f, 0.0f);
		vec3 specular = vec3(0.0f, 0.0f, 0.0f);
		
		if (shadow < globalAmbient.x && shadow < globalAmbient.y && shadow < globalAmbient.z)
		{
			return globalAmbient;
		}
		
		const float diffuseFactor = dot(normal, lightDirection);
		
		if (diffuseFactor > 0.0f) 
		{						
			diffuse = spotLights[i].diffuse.xyz * diffuseFactor * shadow;
			
			float specularFactor = calculateSpecularFactor(worldPosition, normal, lightDirection, spotLights[i].specularIntensity);
			
			if (specularFactor > 0.0f) 
			{
				specular = inSpecular * spotLights[i].specular.xyz * specularFactor * shadow;
			}
		}
		
		const float falloff = clamp((coneAngle - spotLights[i].cone.x) / (spotLights[i].cone.y - spotLights[i].cone.x), 0.0f, 1.0f);
		return clamp((ambient + ((diffuse + specular) * falloff)) / getAttenuation(lightDistance, spotLights[i].attenuation.xyz), globalAmbient, vec3(1.0f));
	}
	
	return globalAmbient;
}

void main(void)
{
	int i; 
	vec4 positionBuffer = texture2D(positionTarget, textureCoords);
	vec4 diffuseBuffer = texture2D(diffuseTarget, textureCoords);
	vec4 normalBuffer = texture2D(normalsTarget, textureCoords);
	vec4 specularBuffer = texture2D(specularTarget, textureCoords);
	
	vec4 lightAccumulation = vec4(globalAmbient, 1.0f);
	
	for (i = 0; i < directionalLightCount; i++)
	{
		lightAccumulation.xyz += CalculateDirectionalLight(positionBuffer.xyz, normalBuffer.xyz, diffuseBuffer.xyz, specularBuffer.xyz, i);
	}
	
	for (i = 0; i < pointLightCount; i++)
	{
		lightAccumulation.xyz += CalculatePointLight(positionBuffer.xyz, normalBuffer.xyz, diffuseBuffer.xyz, specularBuffer.xyz, i);
	}
		
	for (i = 0; i < spotLightCount; i++)
	{
		lightAccumulation.xyz += CalculateSpotLight(positionBuffer.xyz, normalBuffer.xyz, diffuseBuffer.xyz, specularBuffer.xyz, i);
	}
		
	
	finalColor = diffuseBuffer * lightAccumulation;
}
