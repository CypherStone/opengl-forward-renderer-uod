#version 430

layout(location=0) in vec3 inPosition;
layout(location=1) in vec3 inNormal;
layout(location=2) in vec3 inTangent;
layout(location=3) in vec3 inBinormal;
layout(location=4) in vec2 inTexCoord;

out vec3 position;
out vec3 normals;
out vec3 tangent;
out vec3 binormal;
out vec2 texCoord;

out vec3 worldPosition;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void main(void)
{
	mat4 mvp = projectionMatrix * viewMatrix * modelMatrix;
	gl_Position = mvp * vec4(inPosition, 1.0f);
	
	worldPosition = (modelMatrix * vec4(inPosition, 1.0f)).xyz;
	
	position = gl_Position.xyz;
	texCoord = inTexCoord;
	normals = normalize(modelMatrix * vec4(inNormal, 0.0f)).xyz;	
    tangent = normalize(modelMatrix * vec4(inTangent, 0.0f)).xyz;
    binormal = normalize(modelMatrix * vec4(inBinormal, 0.0f)).xyz;
}
