#version 430

in vec3 worldPosition;
uniform vec4 lightPosition;       

layout(location = 0) out float shadow;

void main(void)
{
	vec3 lightToVertex = worldPosition - lightPosition.xyz;
    float lightToPixel = length(lightToVertex);

    shadow = lightToPixel;
}