#version 430

in vec3	position;
in vec3	normals;
in vec3 tangent;
in vec3 binormal;
in vec2	texCoord;

in vec3 worldPosition;

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform sampler2D specularTexture;

layout(location=0) out vec4 outPosition;
layout(location=1) out vec4 outDiffuse;
layout(location=2) out vec4 outNormals;
layout(location=3) out vec4 outSpecular;

void main(void)
{
	float near = 0.1f;
	float far = 1000.0f;
	float depthShift = (position.z - near) / (far - near);

	vec3 bumpMap = vec3(texture2D(normalTexture, texCoord.st)).xyz;
	bumpMap = (bumpMap * 2.0f) - 1.0f;
	bumpMap = normalize(normals + bumpMap.x * tangent + bumpMap.y * binormal);
	
	outPosition = vec4(worldPosition, 1.0f);
	outDiffuse = vec4(texture2D(diffuseTexture, texCoord.st));
	outNormals = vec4(bumpMap.xyz, depthShift);
	outSpecular = vec4(texture2D(specularTexture, texCoord.st));
}