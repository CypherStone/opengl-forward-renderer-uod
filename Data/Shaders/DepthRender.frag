#version 430

layout(location = 0) out float shadow;

void main(void)
{
	shadow = gl_FragCoord.z;
}