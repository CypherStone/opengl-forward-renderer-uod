#version 330

uniform sampler2D output;
in vec2 textureCoords;

out vec4 finalColor;

void main(void)
{
	finalColor = texture2D(output, textureCoords);
	//finalColor = vec4(1,1,1,1);
}
