#version 430

uniform float linearDepthConstant;

in vec3 position;

out float outDepthNormal;

void main()
{
    outDepthNormal = -position.z * linearDepthConstant;;
}

