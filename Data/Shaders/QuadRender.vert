#version 330

layout(location=0) in vec3 inPosition;
layout(location=1) in vec2 inTexCoord;

out vec2 textureCoords;
uniform mat4 project;

void main( void )
{
	gl_Position = project * vec4(inPosition, 1.0f);
	textureCoords = inTexCoord;
}
