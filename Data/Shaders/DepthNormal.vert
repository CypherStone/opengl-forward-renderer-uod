#version 430

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

layout(location=0) in vec3 inPosition;

out vec3 position;

void main()
{
	position = (modelViewMatrix * vec4(inPosition, 1.0f)).xyz;
    //normal = mat3(modelViewMatrix) * inNormal;
	
	gl_Position = projectionMatrix * vec4(position, 1.0f);
}
