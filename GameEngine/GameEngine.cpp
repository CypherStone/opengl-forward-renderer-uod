#include "stdafx.h"

int main(int argc, char * argv[])
{
	using namespace CypherEngine;
	
	GameEngine engine;
	
	if (engine.create(1280, 720, false, false))
	{	
		engine.run();
	}

    return 0;
}

namespace CypherEngine
{
	GameEngine::GameEngine(void)
	{
		m_created = false;
	}

	GameEngine::~GameEngine(void)
	{
		destroy();
	}

	bool GameEngine::create(unsigned short width, unsigned short height, bool vsync, bool fullscreen)
	{
		m_system = System::getInstance();
		m_system->writeDebugString("Game Engine starting up.");

		if (!m_system->create("Game Engine", glm::ivec2(width, height), vsync, fullscreen))
		{
			m_system->showErrorMessageBox("Error - System", "Failed to start.");
			return false;
		}
		
		if (!m_system->parseFileStructure("CypherEngine.xml"))
		{
			m_system->showErrorMessageBox("Error - Data File", "Unable to correctly parse data file.");
			return false;
		}
	
		m_renderEngine = RenderEngine::getInstance();
		if (!m_renderEngine->create(width, height))
		{
			m_system->showErrorMessageBox("Error - Render Engine", "Failed to start.");
			return false;
		}

		m_sceneManager = SceneManager::getInstance();
		if (!m_sceneManager->create(m_system->getFileStructure().scenes))
		{
			m_system->showErrorMessageBox("Error - Scene Manager", "Failed to start.");
			return false;
		}

		m_system->setRunning(true);

		m_created = true;

		System::writeDebugString("Game Engine setup completed.");

		return true;
	}

	void GameEngine::run(void)
	{
		if (!m_created)
		{
			/* The user hasn't initiated the create method. */
			return;
		}

		while (m_system->isRunning())
		{
			m_system->getFrameClock().beginFrame();
			update();
			draw();
			m_system->getFrameClock().endFrame();
		}
	}

	void GameEngine::update(void)
	{
		m_system->update();
		m_sceneManager->update(m_system->getFrameClock().getLastFrameTime().asSeconds());
	}

	void GameEngine::draw(void)
	{
		m_system->getWindow().clear();
		m_sceneManager->draw();
		m_system->getWindow().display();
	}

	void GameEngine::destroy(void)
	{
		if (m_renderEngine)
		{
			m_renderEngine->destroy();
			m_renderEngine = nullptr;
		}

		if (m_sceneManager)
		{
			m_sceneManager->destroy();
			m_sceneManager = nullptr;
		}

		if (m_system)
		{
			m_system->destroy();
			m_system = nullptr;
		}
	}

};