#include "stdafx.h"
#include "Object.h"

namespace CypherEngine
{
	/* Constructor Functions */

	Object::Object(string name)
	{
		m_name = name;
		m_active = false;
	}

	Object::~Object(void)
	{

	}

	/* Member Functions */

	/* Public Functions */

	/* Get Functions */

	bool Object::isActive(void)
	{
		return m_active;
	}

	string Object::getName(void)
	{
		return m_name;
	}

	/* Set Functions*/

	void Object::setActive(bool active)
	{
		m_active = active;
	}
}