#include "stdafx.h"
#include "Camera.h"

namespace CypherEngine
{
	/* Constructor Functions */

	Camera::Camera(vec3 position, vec3 rotation)
	{
		m_movement = vec3(0.0f);
		m_up = vec3(0.0f);
		m_position = position;
		m_rotation = rotation;
		m_active = true;
		m_updated = true;

		setDepth(0.1f, 5000.0f);
	}
	
	/* Public Functions */

	void Camera::update(void)
	{
		if (m_active)
		{		
			if (m_updated)
			{
				m_modelMatrix = glm::rotate(mat4(1.0f), m_rotation.x, vec3(1, 0, 0));
				m_modelMatrix = glm::rotate(m_modelMatrix, m_rotation.y, vec3(0, 1, 0));
				m_modelMatrix = glm::rotate(m_modelMatrix, m_rotation.z, vec3(0, 0, 1));
				m_modelMatrix = glm::translate(m_modelMatrix, m_position);

				vec3 target = m_position + m_movement;

				m_view = glm::lookAt(m_position, target, m_up);
				m_projection = glm::perspectiveFov(glm::radians(m_fov), m_screen.x, m_screen.y, m_minDepth, m_maxDepth);
				m_updated = false;
			}
		}
	}

	/* Get Functions */

	mat4 Camera::getProjection(void)
	{
		return m_projection;
	}

	mat4 Camera::getView(void)
	{
		return m_view;
	}

	mat4 Camera::getModelMatrix(void)
	{
		return m_modelMatrix;
	}


	vec3 Camera::getMovement(void)
	{
		return m_movement;
	}

	vec3 Camera::getPosition(void)
	{
		return m_position;
	}

	vec3 Camera::getRotation(void)
	{
		return m_rotation;
	}

	vec3 Camera::getUp(void)
	{
		return m_up;
	}

	vec2 Camera::getScreenSize(void)
	{
		return m_screen;
	}

	float Camera::getFOV(void)
	{
		return m_fov;
	}

	bool Camera::isActive(void)
	{
		return m_active;
	}

	/* Set Functions */

	void Camera::setMovement(vec3 movement)
	{
		m_movement = movement;
		m_updated = true;
	}

	void Camera::setPosition(vec3 position)
	{
		m_position = position;
		m_updated = true;
	}

	void Camera::setRotation(vec3 rotation)
	{
		m_rotation = rotation;
		m_updated = true;
	}

	void Camera::setUp(vec3 up)
	{
		m_up = up;
		m_updated = true;
	}

	void Camera::setScreenSize(vec2 screensize)
	{
		m_screen = screensize;
		m_updated = true;
	}

	void Camera::setFieldOfView(float fov)
	{
		m_fov = fov;
		m_updated = true;
	}

	void Camera::setActive(bool tmpActive)
	{
		m_active = tmpActive;
		m_updated = true;
	}

	void Camera::setDepth(float min, float max)
	{
		m_minDepth = min;
		m_maxDepth = max;
		
		m_linearDepth = 1.0f / (max - min);

		m_updated = true;
	}

	float Camera::getLinearDepth(void)
	{
		return m_linearDepth;
	}

};