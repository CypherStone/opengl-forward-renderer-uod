#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class Camera
	{
	private:
		mat4 m_modelMatrix;
		mat4 m_view;
		mat4 m_projection;
		vec3 m_position;
		vec3 m_movement;
		vec3 m_rotation;
		vec3 m_up;
		vec2 m_screen;
		float m_fov;
		float m_linearDepth;
		float m_minDepth;
		float m_maxDepth;
		bool m_active;
		bool m_updated;

	public:
		Camera(vec3 pos = glm::vec3(), glm::vec3 ang = glm::vec3());

		void update(void);

		mat4 getProjection(void);
		mat4 getView(void);
		mat4 getModelMatrix(void);
		vec3 getMovement(void);
		vec3 getPosition(void);
		vec3 getRotation(void);
		vec3 getUp(void); 
		vec2 getScreenSize(void);
		float getFOV(void);
		float getLinearDepth(void);
		bool isActive(void);

		void setMovement(vec3 movement);
		void setPosition(vec3 position);
		void setRotation(vec3 rotation);
		void setUp(vec3 up);
		void setScreenSize(vec2 screen);
		void setFieldOfView(float fov);
		void setActive(bool active);

		void setDepth(float min, float max);
		
	};
};