#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class Object
	{
	private:
		string m_name;
		bool m_active;

	public:
		Object(string name);
		~Object(void);

		virtual void update(float deltaTime = 0.0f) = 0;
		virtual void destroy() = 0;

		bool isActive(void);
		string getName(void);

		void setActive(bool active);
	};
}