#pragma once
#include "../../stdafx.h"
#include "Graphics/Shader.h"

namespace CypherEngine
{
	class GameObject
	{
	private:
		vector<Mesh> meshes;
		glm::mat4 m_modelMatrix;
		vec3 position;
		vec3 rotation;
		vec3 m_scale;
		string name;
		string tag;
		unsigned int polycount;
		unsigned int meshCount;
		int layer;
		bool modelLoaded;
		bool active;
		bool visible;
		bool m_updateMatrix;

		unsigned int m_shaderID;
		unsigned int m_textureLocations[4];
		unsigned int m_matrixLocation;

		void copy(const GameObject& model);
		void updateModelMatrix(void);

	public:

		/* Constructors */

		GameObject(string name, string tag);
		~GameObject(void);

		/* Functions */

		void create(void);
		void addMeshes(vector<Mesh> &mesh);
		void draw(shared_ptr<Shader> shader, bool material);
		void destroy(void);
		
		/* Get Functions */

		vector<string> getTextures(void);
		glm::mat4 getModelMatrix(void);
		vec3 getPosition(void);
		vec3 getRotation(void);
		vec3 getScale(void);
		string getName(void);
		string getTag(void);
		int getPolygonCount(void);
		int getLayer(void);
		bool isActive(void);
		bool isLoaded(void);
		bool isVisible(void);

		/* Set Functions */

		void setTextures(const map<string, GLuint> & textures);
		void setPosition(vec3 pos);
		void setRotation(vec3 rot);
		void setScale(vec3 scale);
		void setName(string name);
		void setTag(string tag);
		void setVisible(bool visible);
	};
};