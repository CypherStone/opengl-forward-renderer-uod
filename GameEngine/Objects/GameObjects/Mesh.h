#pragma once
#include "stdafx.h"


namespace CypherEngine
{
	class Mesh
	{
	private:
		MeshMaterial material;
		vector<Vertex> vertices;
		int	vertexCount;
		int	indexCount;
		GLuint	vertexArrayId;
		GLuint	vertexBufferId;
		GLuint	indexBufferId;

		GLuint diffuseHandle;
		GLuint normalHandle;
		GLuint specularHandle;
		GLuint opacityHandle;

		void generateTexture(GLuint &handle, sf::Image &image);

	public:
		
		Mesh(void);
		Mesh(MeshMaterial &material, std::vector<Vertex> & vertices);
		~Mesh(void);

		void create(void);
		void draw(void);
		void destroy(void);
		
		inline int getVertexCount(void)
		{
			return this->vertexCount;
		}

		MeshMaterial & getMaterial(void);

		GLuint getDiffuseTexture(void);
		GLuint getNormalTexture(void);
		GLuint getSpecularTexture(void);
		GLuint getOpacityTexture(void);

		void setDiffuseTexture(GLuint id);
		void setNormalTexture(GLuint id);
		void setSpecularTexture(GLuint id);
		void setOpacityTexture(GLuint id);
	};
};