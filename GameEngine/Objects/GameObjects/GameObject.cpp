#pragma once
#include "stdafx.h"
#include "GameObject.h"

namespace CypherEngine
{
	/* Constructor Functions */

	GameObject::GameObject(string name = "GameObject", string tag = "Default")
	{
		this->name = name;
		this->tag = tag;
		m_scale = vec3(1.0f);
		modelLoaded = false;
	}

	GameObject::~GameObject(void)
	{
		destroy();
	}

	/* Public Functions */

	void GameObject::create(void)
	{
		if (meshes.size() > 0)
		{
			meshCount = meshes.size();
			for (auto & mesh : meshes)
			{
				mesh.create();
			}
		}
	}

	void GameObject::addMeshes(vector<Mesh> & mesh)
	{
		modelLoaded = true;
		meshes = mesh;
	}

	void GameObject::draw(shared_ptr<Shader> shader, bool material)
	{	
		auto drawShader = dynamic_cast<DrawShader *>(shader.get());
		if (drawShader)
		{ 
			if (material)
			{
				if (drawShader->getProgram() != m_shaderID)
				{
					m_matrixLocation = shader->getUniformLocation("modelMatrix");
				}

				drawShader->bindUniformMatrix4(m_matrixLocation, getModelMatrix());
			}

			for (auto & mesh : meshes)
			{
				if (material)
				{
					int diffuse = MODEL_TEXTURE_START;
					int normal = MODEL_TEXTURE_START + 1;
					int specular = MODEL_TEXTURE_START + 2;
					int opacity = MODEL_TEXTURE_START + 3;

					if (drawShader->getProgram() != m_shaderID)
					{
						m_shaderID = drawShader->getProgram();

						m_textureLocations[0] = shader->getUniformLocation("diffuseTexture");
						m_textureLocations[1] = shader->getUniformLocation("normalTexture");
						m_textureLocations[2] = shader->getUniformLocation("specularTexture");
						m_textureLocations[3] = shader->getUniformLocation("opacityTexture");
					}

					drawShader->bindTexture(m_textureLocations[0], diffuse, mesh.getDiffuseTexture());
					drawShader->bindTexture(m_textureLocations[1], normal, mesh.getNormalTexture());
					drawShader->bindTexture(m_textureLocations[2], specular, mesh.getSpecularTexture());
					drawShader->bindTexture(m_textureLocations[3], opacity, mesh.getOpacityTexture());
				}

				mesh.draw();
			}
		}
	}

	void GameObject::destroy(void)
	{
		for (auto & mesh : meshes)
		{
			mesh.destroy();
		}
	}

	/* Get Functions */

	vector<string> GameObject::getTextures(void)
	{
		vector<string> textures;

		for (auto & mesh : meshes)
		{
			textures.push_back(mesh.getMaterial().diffuseTexture);
			textures.push_back(mesh.getMaterial().normalTexture);
			textures.push_back(mesh.getMaterial().specularTexture);
			textures.push_back(mesh.getMaterial().opacityTexture);
		}

		return textures;
	}

	glm::mat4 GameObject::getModelMatrix(void)
	{
		if (m_updateMatrix)
		{
			updateModelMatrix();
		}

		return m_modelMatrix;
	}

	string GameObject::getName(void)
	{
		return name;
	}

	string GameObject::getTag(void)
	{
		return tag;
	}

	glm::vec3 GameObject::getPosition(void)
	{
		return position;
	}

	glm::vec3 GameObject::getRotation(void)
	{
		return rotation;
	}

	vec3 GameObject::getScale(void)
	{
		return m_scale;
	}

	int GameObject::getPolygonCount(void)
	{
		return polycount;
	}

	int GameObject::getLayer(void)
	{
		return layer;
	}

	bool GameObject::isLoaded(void)
	{
		return modelLoaded;
	}

	bool GameObject::isActive(void)
	{
		return active;
	}

	bool GameObject::isVisible(void)
	{
		return visible;
	}

	/* Set Functions */

	void GameObject::setTextures(const map<string, GLuint> & textures)
	{
		map<string, GLuint>::const_iterator textureMap;
		for (auto & mesh : meshes)
		{
			textureMap = textures.find(mesh.getMaterial().diffuseTexture);
			if (textureMap != textures.end())
			{
				mesh.setDiffuseTexture((*textureMap).second);
			}

			textureMap = textures.find(mesh.getMaterial().normalTexture);
			if (textureMap != textures.end())
			{
				mesh.setNormalTexture((*textureMap).second);
			}

			textureMap = textures.find(mesh.getMaterial().specularTexture);
			if (textureMap != textures.end())
			{
				mesh.setSpecularTexture((*textureMap).second);
			}

			textureMap = textures.find(mesh.getMaterial().opacityTexture);
			if (textureMap != textures.end())
			{
				mesh.setOpacityTexture((*textureMap).second);
			}
		}
	}

	void GameObject::setPosition(glm::vec3 pos)
	{
		m_updateMatrix = true;
		position = pos;
	}

	void GameObject::setRotation(glm::vec3 rot)
	{
		m_updateMatrix = true;
		rotation = rot;
	}

	void GameObject::setScale(vec3 scale)
	{
		m_updateMatrix = true;
		m_scale = scale;
	}

	void GameObject::setTag(string tag)
	{
		this->tag = tag;
	}

	void GameObject::setName(string name)
	{
		this->name = name;
	}

	void GameObject::setVisible(bool visibility)
	{
		visible = visibility;
	}

	/* Member Functions */

	void GameObject::updateModelMatrix(void)
	{
		m_updateMatrix = false;

		m_modelMatrix = glm::translate(glm::mat4(1.0f), position);
		m_modelMatrix = glm::rotate(m_modelMatrix, rotation.x, glm::vec3(1, 0, 0));
		m_modelMatrix = glm::rotate(m_modelMatrix, rotation.y, glm::vec3(0, 1, 0));
		m_modelMatrix = glm::rotate(m_modelMatrix, rotation.z, glm::vec3(0, 0, 1));
		m_modelMatrix = glm::scale(m_modelMatrix, m_scale);
	}
}