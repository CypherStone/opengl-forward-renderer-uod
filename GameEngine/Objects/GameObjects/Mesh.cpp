#include "stdafx.h"

namespace CypherEngine
{
	Mesh::Mesh(void)
	{
		vertexArrayId = 0;
		vertexBufferId = 0;
		indexBufferId = 0;
	}

	Mesh::Mesh(MeshMaterial & material, std::vector<Vertex> & vertices)
	{
		this->material = material;
		this->vertexCount = vertices.size();
		this->vertices = vertices;
	}

	Mesh::~Mesh(void)
	{

	}

	void Mesh::create(void)
	{
		glGenVertexArrays(1, &vertexArrayId);
		glGenBuffers(1, &vertexBufferId);
		glGenBuffers(1, &indexBufferId);

		auto indicies = vector<int>();
		for (int i = 0; i < vertexCount; i++)
		{
			indicies.push_back(i);
		}
		indexCount = vertexCount;

		glBindVertexArray(vertexArrayId);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
		glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(0));
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(12));
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(24));
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(36));
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(48));
		//glDisableVertexAttribArray(4);
		//glDisableVertexAttribArray(3);
		//glDisableVertexAttribArray(2);
		//glDisableVertexAttribArray(1);
		//glDisableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ((unsigned int)indexCount) * sizeof(unsigned int), &indicies[0], GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		vertices.clear();
	}

	void Mesh::draw(void)
	{
		glBindVertexArray(vertexArrayId);
		glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, BUFFER_OFFSET(0));
		glBindVertexArray(0);
	}

	void Mesh::destroy(void)
	{
		glDeleteVertexArrays(1, &vertexArrayId);
		glDeleteBuffers(1, &vertexBufferId);
		glDeleteBuffers(1, &indexBufferId);
	}

	MeshMaterial & Mesh::getMaterial(void)
	{
		return material;
	}

	GLuint Mesh::getDiffuseTexture(void)
	{
		return diffuseHandle;
	}

	GLuint Mesh::getNormalTexture(void)
	{
		return normalHandle;
	}

	GLuint Mesh::getSpecularTexture(void)
	{
		return specularHandle;
	}

	GLuint Mesh::getOpacityTexture(void)
	{
		return opacityHandle;
	}

	void Mesh::setDiffuseTexture(GLuint id)
	{
		diffuseHandle = id;
	}

	void Mesh::setNormalTexture(GLuint id)
	{
		normalHandle = id;
	}

	void Mesh::setSpecularTexture(GLuint id)
	{
		specularHandle = id;
	}

	void Mesh::setOpacityTexture(GLuint id)
	{
		opacityHandle = id;
	}
};