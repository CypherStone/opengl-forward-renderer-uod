#pragma once
#include "stdafx.h"
#include "PointLight.h"

namespace CypherEngine
{
	/* Constructor Functions */

	PointLight::PointLight(void) : Light()
	{

	}

	/* Public Functions */

	bool PointLight::create(short width, short height)
	{
		if (!m_shadow.create(width, height))
		{
			return false;
		}

		m_projectionMatrix = glm::perspective<float>(90.0f, (float)width / (float)height, 0.1f, 10000.0f);

		return true;
	}

	/* Set Functions */
	
	void PointLight::setLightProperties(const PointLightShaderBuffer & properties)
	{
		m_properties = properties;
		m_updateMatrix = true;
	}

	/* Get Functions */

	PointLightShaderBuffer & PointLight::getLightProperties(void)
	{
		m_updateMatrix = true;
		return m_properties;
	}

	CubeDepthTexture & PointLight::getShadow(void)
	{
		return m_shadow;
	}

	glm::mat4 & PointLight::getViewMatrix(vec3 position, vec3 target, vec3 up)
	{
		if (m_updateMatrix)
		{
			updateMatrix(vec3(m_properties.position), target, up);
		}
		return m_viewMatrix;
	}

	/* Member Functions */


}