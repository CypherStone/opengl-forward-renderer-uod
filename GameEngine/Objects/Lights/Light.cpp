#include "stdafx.h"
#include "Light.h"


namespace CypherEngine
{
	/* Constructor Functions */
	Light::Light(void)
	{
		m_id = -1;
	}

	/* Public Functions */
	
	/* Get Functions */

	glm::mat4 & Light::getProjectionMatrix(void)
	{
		return m_projectionMatrix;
	}

	int Light::getID(void)
	{
		return m_id;
	}

	bool Light::isActive(void)
	{
		return m_active;
	}

	/* Set Functions */

	void Light::setActive(bool active)
	{
		m_active = active;
	}

	void Light::setID(int id)
	{
		m_id = id;
	}

	/* Private Functions */

	void Light::updateMatrix(vec3 position, vec3 target, vec3 up)
	{
		m_viewMatrix = glm::lookAt(position, position - target, up);
	}
}

