#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class DirectionalLight : public Light
	{
	private:
		DirectionalLightShaderBuffer m_properties;
		DepthTexture m_shadow;
		vec3 m_position;

	public:
		DirectionalLight(void);

		bool create(short width, short height);

		void setLightProperties(const DirectionalLightShaderBuffer & properties);
		void setPosition(vec3 & position);

		DirectionalLightShaderBuffer & getLightProperties(void);
		DepthTexture & getShadow(void);
		glm::mat4 & getViewMatrix(vec3 position = vec3(), vec3 target = vec3(), vec3 up = vec3(0.0f, 1.0f, 0.0f));
		vec3 & DirectionalLight::getPosition(void);
	};
}
