#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class SpotLight : public Light
	{
	private:
		SpotLightShaderBuffer m_properties;
		DepthTexture m_shadow;

	public:
		SpotLight(void);

		bool create(short width, short height);

		void setLightProperties(const SpotLightShaderBuffer & properties);

		SpotLightShaderBuffer & getLightProperties(void);
		DepthTexture & getShadow(void);
		glm::mat4 & getViewMatrix(vec3 position = vec3(), vec3 target = vec3(), vec3 up = vec3(0.0f, 1.0f, 0.0f));
	};
}


