#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class Light
	{
	protected:
		glm::mat4 m_projectionMatrix;
		glm::mat4 m_viewMatrix;
		bool m_active;
		bool m_updateMatrix;
		int m_id;

		void updateMatrix(vec3 position, vec3 target, vec3 up);
	public:

		Light(void);

		virtual bool create(short width, short height) = 0;

		glm::mat4 & getProjectionMatrix(void);
		virtual glm::mat4 & getViewMatrix(vec3 position = vec3(), vec3 target = vec3(), vec3 up = vec3(0.0f, 1.0f, 0.0f)) = 0;
		int getID(void);
		bool isActive(void);

		void setID(int id);
		void setActive(bool active);
	};

}