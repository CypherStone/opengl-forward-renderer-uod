#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class PointLight : public Light
	{
	private:
		PointLightShaderBuffer m_properties;
		CubeDepthTexture m_shadow;

	public:
		PointLight(void);

		bool create(short width, short height);

		void setLightProperties(const PointLightShaderBuffer & properties);

		PointLightShaderBuffer & getLightProperties(void);
		CubeDepthTexture & getShadow(void);
		glm::mat4 & getViewMatrix(vec3 position = vec3(), vec3 target = vec3(), vec3 up = vec3());
	};
}


