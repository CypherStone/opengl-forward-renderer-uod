#include "stdafx.h"
#include "DirectionalLight.h"

namespace CypherEngine
{
	/* Constructor Functions */

	DirectionalLight::DirectionalLight(void) : Light()
	{

	}

	/* Member Functions */

	bool DirectionalLight::create(short width, short height)
	{
		if (!m_shadow.create(width, height))
		{
			return false;
		}

		//m_projectionMatrix = glm::perspective(90.0f, 1024.0f / 1024.0f, 100.0f, 1000.0f);
		m_projectionMatrix = glm::ortho(-250.0f, 250.0f, -250.0f, 250.0f, 1.0f, 1000.0f);

		return true;
	}

	/* Set Functions */

	void DirectionalLight::setPosition(vec3 & position)
	{
		m_updateMatrix = true;
		m_position = position;
	}

	void DirectionalLight::setLightProperties(const DirectionalLightShaderBuffer & properties)
	{
		m_updateMatrix = true;
		m_properties = properties;
	}

	/* Get Functions */

	DirectionalLightShaderBuffer & DirectionalLight::getLightProperties(void)
	{
		return m_properties;
	}

	DepthTexture & DirectionalLight::getShadow(void)
	{
		return m_shadow;
	}

	vec3 & DirectionalLight::getPosition(void)
	{
		return m_position;
	}

	glm::mat4 & DirectionalLight::getViewMatrix(vec3 position, vec3 target, vec3 up)
	{
		if (m_updateMatrix)
		{
			updateMatrix(m_position, glm::vec3(m_properties.direction), up);
		}
		return m_viewMatrix;
	}

	/* Member Functions */

}