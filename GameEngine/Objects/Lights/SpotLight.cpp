#pragma once
#include "stdafx.h"
#include "SpotLight.h"

namespace CypherEngine
{
	/* Constructor Functions */

	SpotLight::SpotLight(void) : Light()
	{

	}

	/* Member Functions */

	bool SpotLight::create(short width, short height)
	{
		if (!m_shadow.create(width, height))
		{
			return false;
		}

		m_projectionMatrix = glm::perspective<float>(90.0f, (float)width / (float)height, 0.1f, 10000.0f);

		return true;
	}

	/* Set Functions */

	void SpotLight::setLightProperties(const SpotLightShaderBuffer & properties)
	{
		m_properties = properties;
		m_updateMatrix = true;
	}

	/* Get Functions */

	SpotLightShaderBuffer & SpotLight::getLightProperties(void)
	{
		m_updateMatrix = true;
		return m_properties;
	}

	DepthTexture & SpotLight::getShadow(void)
	{
		return m_shadow;
	}

	glm::mat4 & SpotLight::getViewMatrix(vec3 position, vec3 target, vec3 up)
	{
		if (m_updateMatrix)
		{
			updateMatrix(vec3(m_properties.position), vec3(m_properties.direction), up);
			m_properties.direction = glm::normalize(m_properties.direction);
		}
		return m_viewMatrix;
	}

	/* Member Functions */

}