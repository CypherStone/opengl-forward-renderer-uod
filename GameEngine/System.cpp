#include "stdafx.h"
#include "System.h"

namespace CypherEngine
{
	System * System::m_instance = nullptr;

	System::System(void)
	{
		m_vsync = false;
		fullscreen = false;
	}

	System::~System(void)
	{

	}

	System * System::getInstance(void)
	{
		if (!m_instance)
		{
			m_instance = new System;
		}

		return m_instance;
	}

	void System::destroy(void)
	{
		if (m_instance)
		{
			delete m_instance;
			m_instance = nullptr;
		}
	}

	bool System::create(string title, glm::ivec2 resolution, bool vsync, bool fs)
	{
		m_vsync = vsync;
		fullscreen = fs;

		m_currentVideoMode = sf::VideoMode(resolution.x, resolution.y);
		if (!m_currentVideoMode.isValid())
		{
			m_currentVideoMode = sf::VideoMode(1024, 768);
		}

		m_fullscreenVideoModes = sf::VideoMode::getFullscreenModes();

		sf::ContextSettings context(24);
		context.antialiasingLevel = 2;

		m_window.create(m_currentVideoMode, title, createWindowStyle(fullscreen ? WS_FULLSCREEN : WS_DEFAULT), context);
		m_window.setVerticalSyncEnabled(vsync);
		m_window.setFramerateLimit(300);

		m_statistics.create(1.0f);
		if (m_statistics.getOpenGLVersion() < 4)
		{
			return false;
		}

		srand((unsigned int)time(nullptr));

		return true;
	}

	sf::Uint32 System::createWindowStyle(WindowStyles style)
	{
		sf::Uint32 windowStyle;

		switch (style)
		{
		case WS_FULLSCREEN:
			windowStyle = sf::Style::Fullscreen;
			break;
		case WS_SPLASH:
			windowStyle = sf::Style::None;
			break;
		case WS_DEFAULT:
			windowStyle = sf::Style::Titlebar | sf::Style::Close;
			break;
		}

		return windowStyle;
	}

	bool System::parseFileStructure(const string & file)
	{
		tinyxml2::XMLDocument xmlFile;
		if (xmlFile.LoadFile(file.c_str()) != tinyxml2::XML_NO_ERROR)
		{
			return false;
		}

		auto data = xmlFile.FirstChild()->FirstChildElement("FileStructure");
		if (!data)
		{
			return false;
		}

		m_structure.path = data->Attribute("path");
		if (m_structure.path.empty())
		{
			return false;
		}

		if (!parseFilepath(data, "Scenes", m_structure.path, m_structure.scenes))
		{
			return false;
		}

		if (!parseFilepath(data, "Fonts", m_structure.path, m_structure.fonts))
		{
			return false;
		}

		if (!parseFilepath(data, "Shaders", m_structure.path, m_structure.shaders))
		{
			return false;
		}

		if (!parseFilepath(data, "Models", m_structure.path, m_structure.models))
		{
			return false;
		}

		if (!parseFilepath(data, "Textures", m_structure.path, m_structure.textures))
		{
			return false;
		}

		if (!parseFilepath(data, "Screenshots", m_structure.path, m_structure.screenshots))
		{
			return false;
		}

		if (!parseFilepath(data, "Statistics", m_structure.path, m_structure.statistics))
		{
			return false;
		}

		return true;
	}

	bool System::parseFilepath(const tinyxml2::XMLElement * data, const string & name, const string & path, string & storage)
	{
		string tmpStorage = data->FirstChildElement(name.c_str())->GetText();
		if (tmpStorage.empty())
		{
			return false;
		}

		storage = path + tmpStorage + "\\";

		return true;
	}

	void System::update(void)
	{
		m_statistics.update();

		sf::Event windowEvent;
		while (m_window.pollEvent(windowEvent))
		{
			switch (windowEvent.type)
			{
			case sf::Event::Closed:
				running = false;
				break;
			}

			for (auto & gui : m_guis)
			{
				gui->handleEvent(windowEvent);
			}
		}
	}

	void System::showConsoleWindow(void)
	{
		if (allowUpdate)
		{
			ShowWindow(GetConsoleWindow(), consoleVisible ? SW_SHOW : SW_HIDE);
			consoleVisible = !consoleVisible;
		}
	}

	const System::FileStructure & System::getFileStructure(void)
	{
		return m_structure;
	}

	void System::attachGUI(shared_ptr<tgui::Gui> gui)
	{
		m_guis.push_back(gui);
	}

	sf::RenderWindow & System::getWindow(void)
	{
		return m_window;
	}

	bool System::isRunning(void)
	{
		return running;
	}

	bool System::isVSyncActive(void)
	{
		return m_vsync;
	}

	bool System::hasFocus(void)
	{
		return m_window.hasFocus();
	}

	SystemStatistics & System::getSystemStatistics(void)
	{
		return m_statistics;
	}

	sfx::FrameClock & System::getFrameClock(void)
	{
		return m_statistics.getFrameClock();
	}

	void System::dettachGUIs(void)
	{
		m_guis.clear();
	}

	void System::setRunning(bool run)
	{
		running = run;
	}

	System::MessageBoxValue System::showMessageBox(const string &title, const string &message, MessageBoxType type)
	{
		UINT value = NULL;

		switch (type)
		{
		case MBT_OK:
			value = MB_OK;
			break;
		case MBT_OKCANCEL:
			value = MB_OKCANCEL;
			break;
		case MBT_RETRYCANCEL:
			value = MB_RETRYCANCEL;
			break;
		case MBT_YESNO:
			value = MB_YESNO;
			break;
		case MBT_YESNOCANCEL:
			value = MB_YESNOCANCEL;
			break;
		case MBT_ABORTRETRYIGNORE:
			value = MB_ABORTRETRYIGNORE;
			break;
		case MBT_CANCELTRYCONTINUE:
			value = MB_CANCELTRYCONTINUE;
			break;
		}

		return static_cast<MessageBoxValue>(MessageBox(m_window.getSystemHandle(), message.c_str(), title.c_str(), value));
	}

	void System::showErrorMessageBox(const string &title, const string &message)
	{
		MessageBox(m_window.getSystemHandle(), message.c_str(), title.c_str(), MB_ICONERROR);
	}

	void System::writeDebugString(string text)
	{
//#ifdef _DEBUG
		text += "\n\n";
		std::cout << text;
//#endif
	}

	void System::setVSync(bool vsync)
	{
		m_vsync = vsync;
		m_window.setVerticalSyncEnabled(m_vsync);
	}

	void System::close(void)
	{
		if (m_instance)
		{
			m_instance->running = false;
		}
	}

	void System::takeScreenshot(void)
	{
		time_t a = time(0);
		tm b;
		localtime_s(&b, &a);
		char buffer[80];
		std::strftime(buffer, sizeof(buffer), "%d-%m-%Y_%H-%M-%S", &b);
		string datetimeFile = buffer + string(".png");

		m_instance->m_window.capture().saveToFile(m_instance->m_structure.screenshots + datetimeFile);

		writeDebugString("Screenshot: " + datetimeFile + " saved to " + m_instance->m_structure.screenshots);
	}

};