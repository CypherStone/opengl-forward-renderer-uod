#include "stdafx.h"
#include "SystemStatisticsImpl.h"

namespace CypherEngine
{
	unordered_map<string, PerformanceMeasurement> SystemStatisticsImpl::m_performanceMeasure;
	sfx::FrameClock SystemStatisticsImpl::m_frameClock;
	int SystemStatisticsImpl::m_process;
	string SystemStatisticsImpl::m_cpu;
	string SystemStatisticsImpl::m_gpu;
	string SystemStatisticsImpl::m_gpuManufacturer;
	string SystemStatisticsImpl::m_os;
	string SystemStatisticsImpl::m_information;
	string SystemStatisticsImpl::m_frameInformation;
	vec2 SystemStatisticsImpl::m_currentResolution;
	vector<vec2> SystemStatisticsImpl::m_fullscreenResolutions;
	unsigned int SystemStatisticsImpl::m_cpuCores;
	unsigned int SystemStatisticsImpl::m_totalSystemMemory;
	unsigned int SystemStatisticsImpl::m_usedSystemMemory;
	unsigned int SystemStatisticsImpl::m_videoMemory;
	unsigned int SystemStatisticsImpl::m_videoMemoryOffset;
	unsigned int SystemStatisticsImpl::m_usedVideoMemory;
	unsigned int SystemStatisticsImpl::m_osArchitecture;
	unsigned int SystemStatisticsImpl::m_cpuUsage;
	float SystemStatisticsImpl::m_openglVersion;

	SystemStatisticsImpl::SystemStatisticsImpl()
	{

	}

	SystemStatisticsImpl::~SystemStatisticsImpl()
	{

	}

	sfx::FrameClock & SystemStatisticsImpl::getFrameClock(void)
	{
		return m_frameClock;
	}

	const string & SystemStatisticsImpl::getCPUName(void)
	{
		return m_cpu;
	}

	const string & SystemStatisticsImpl::getGPUName(void)
	{
		return m_gpu;
	}

	const string & SystemStatisticsImpl::getOSName(void)
	{
		return m_os;
	}

	void SystemStatisticsImpl::updateFrameInformation(void)
	{
		std::ostringstream tmp;

		tmp << "\nFrame Information\n";
		tmp << "\nFPS: " << m_frameClock.getFramesPerSecond();
		tmp << "\n\nMinimum FPS: " << m_frameClock.getMinFramesPerSecond();
		tmp << "\nAverage FPS: " << m_frameClock.getAverageFramesPerSecond();
		tmp << "\nMaximum FPS: " << m_frameClock.getMaxFramesPerSecond();
		tmp << "\n\nDelta: " << m_frameClock.getLastFrameTime().asSeconds();
		tmp << "\n\nMinimum FrameTime: " << m_frameClock.getMinFrameTime().asMicroseconds() / 1000.0f << "ms";
		tmp << "\nAverage FrameTime: " << m_frameClock.getAverageFrameTime().asMicroseconds() / 1000.0f << "ms";
		tmp << "\nMaximum FrameTime: " << m_frameClock.getMaxtFrameTime().asMicroseconds() / 1000.0f << "ms";
		tmp << "\n\nFrame Measurements";
		for (auto & time : m_performanceMeasure)
		{
			tmp << "\n" << time.first << ": " << time.second.getTimeDuration() << "ms";
		}

		m_frameInformation = tmp.str();
	}

	void SystemStatisticsImpl::updateSystemInfomration(void)
	{
		std::ostringstream tmp;

		tmp << "\nSystem Information\n";
#ifdef _DEBUG
		tmp << "\nDebug Build";
#else
		tmp << "\nRelease Build";
#endif
		tmp << "\n\nOS: " << getOSName() << " " << getOSArchitecture() << "bit";
		tmp << "\n\nCPU: " << getCPUName();
		tmp << "\nCPU Cores: " << getCPUCores();
		tmp << "\nCPU Usage: " << getCPUUsage() << "%";
		tmp << "\n\nGPU: " << getGPUName();
		tmp << "\nTotal Video Memory: " << getVideoMemory() << "MB";
		tmp << "\nVideo Memory Used: " << getUsedVideoMemory() << "MB";
		tmp << "\nOpenGL Version: " << getOpenGLVersion();
		tmp << "\n\nTotal System Memory: " << getTotalSystemMemory() << "MB";
		tmp << "\nEngine Memory Used: " << getUsedSystemMemory() << "MB";

		m_information = tmp.str();
	}

	vec2 SystemStatisticsImpl::getCurrentResolution(void)
	{
		return m_currentResolution;
	}

	vector<vec2> & SystemStatisticsImpl::getFullscreenResolutions(void)
	{
		return m_fullscreenResolutions;
	}

	unsigned int SystemStatisticsImpl::getTotalSystemMemory(void)
	{
		return m_totalSystemMemory;
	}

	unsigned int SystemStatisticsImpl::getUsedSystemMemory(void)
	{
		return m_usedSystemMemory;
	}

	unsigned int SystemStatisticsImpl::getVideoMemory(void)
	{
		return m_videoMemory;
	}

	unsigned int SystemStatisticsImpl::getOSArchitecture(void)
	{
		return m_osArchitecture;
	}

	unsigned int SystemStatisticsImpl::getCPUUsage(void)
	{
		return m_cpuUsage;
	}

	unsigned int SystemStatisticsImpl::getUsedVideoMemory(void)
	{
		return m_usedVideoMemory;
	}

	unsigned int SystemStatisticsImpl::getCPUCores(void)
	{
		return m_cpuCores;
	}

	float SystemStatisticsImpl::getOpenGLVersion(void)
	{
		return m_openglVersion;
	}

	void SystemStatisticsImpl::createMeasurement(const string & name)
	{
		m_performanceMeasure.insert(pair<string, PerformanceMeasurement>(name, PerformanceMeasurement()));
	}

	void SystemStatisticsImpl::startMeasurement(const string & name)
	{
		auto perfomance = m_performanceMeasure.find(name);
		if (perfomance != m_performanceMeasure.end())
		{
			perfomance->second.start();
		}
		else
		{
			createMeasurement(name);
		}
	}

	void SystemStatisticsImpl::endMeasurement(const string & name)
	{
		auto perfomance = m_performanceMeasure.find(name);
		if (perfomance != m_performanceMeasure.end())
		{
			perfomance->second.end();
		}
	}

	float SystemStatisticsImpl::getMeasurement(const string & name)
	{
		auto perfomance = m_performanceMeasure.find(name);
		if (perfomance != m_performanceMeasure.end())
		{
			return perfomance->second.getTimeDuration();
		}

		return 0.0f;
	}

	const string & SystemStatisticsImpl::getSystemInformation(void)
	{
		return m_information;
	}

	const string & SystemStatisticsImpl::getFrameInformation(void)
	{
		return m_frameInformation;
	}
}