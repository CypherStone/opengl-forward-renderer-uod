#pragma once
#include "stdafx.h"
#ifdef CE_WIN
#define WGL_GPU_RAM_AMD 0x21A3
#define GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX 0x9048
#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX 0x9049

#include "SystemStatisticsImpl.h"

namespace CypherEngine
{
	class SystemStatisticsWin : SystemStatisticsImpl
	{
	private:

		typedef UINT(WINAPI * PFNWGLGETGPUIDSAMDPROC) (UINT maxCount, UINT *ids);
		typedef INT(WINAPI * PFNWGLGETGPUINFOAMDPROC) (UINT id, int property, GLenum dataType, UINT size, void *data);
		PFNWGLGETGPUIDSAMDPROC wglGetGPUIDsAMD;
		PFNWGLGETGPUINFOAMDPROC wglGetGPUInfoAMD;
		GLuint noOfGPUs;
		GLuint * gpuIDs;

		ULARGE_INTEGER m_lastCPU;
		ULARGE_INTEGER m_lastSystemCPU;
		ULARGE_INTEGER m_lastUserCPU;
		
		void setCPUCores(void);
		void setCPUName(void);
		void setGPUName(void);
		void setOSName(void);
		void setOSArchitecture(void);
		void setTotalSystemMemory(void);
		void setVideoMemory(void);
		void setOpenGLVersion(void);

	public:
		SystemStatisticsWin(void);
		~SystemStatisticsWin(void);

		virtual void create(float updateDuration);
		virtual void update(void);
		virtual void updateUsedSystemMemory(void);
		virtual void updateCPUUsage(void);
		virtual void updateGPUUsage(void);

		virtual void setProcessHandle(int process);
		virtual void setCurrentResolution(vec2 & currentResolution);
		virtual void setFullscreenResolutions(vector<vec2> & resolutions);
	};
}

#endif