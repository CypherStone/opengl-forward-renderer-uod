#pragma once
#include "stdafx.h"
#include "SystemStatisticsWin.h"

namespace CypherEngine
{
	class SystemStatistics
	{
	private:

#ifdef CE_WIN
		SystemStatisticsWin m_impl;
#endif

	public:
		SystemStatistics(void);
		~SystemStatistics(void);
		
		void create(float updateDuration);
		void update(void);

		sfx::FrameClock & getFrameClock(void);
		const string & getCPUName(void);
		const string & getGPUName(void);
		const string & getOSName(void);
		vec2 getCurrentResolution(void);
		vector<vec2> & getFullscreenResolutions(void);
		unsigned int getTotalSystemMemory(void);
		unsigned int getUsedSystemMemory(void);
		unsigned int getVideoMemory(void);
		unsigned int getOSArchitecture(void);
		unsigned int getCPUUsage(void);
		unsigned int getUsedVideoMemory(void);
		unsigned int getCPUCores(void);
		float getOpenGLVersion(void);

		void setProcessHandle(int process);

		void createMeasurement(const string & name);
		void startMeasurement(const string & name);
		void endMeasurement(const string & name);
		float getMeasurement(const string & name);

		const string & getSystemInformation(void);
		const string & getFrameInformation(void);
	};

}