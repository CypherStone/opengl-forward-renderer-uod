#include "stdafx.h"

namespace CypherEngine
{
	namespace Common
	{
		string String::BuildFilePath(const string * fileString, const string * filename)
		{
			auto newFileString = *fileString;
			auto endFileName = newFileString.substr(newFileString.find_last_of('\\') + 1);
			newFileString.replace(newFileString.find(endFileName), endFileName.size(), *filename);
			return newFileString;
		}

		bool String::Match(const string * a, const string * b)
		{
			return (*a) == (*b);
		}

		bool String::Match(const string * a, const string b)
		{
			return Match(a, &b);
		}

		bool String::Match(const string a, const string * b)
		{
			return Match(&a, b);
		}

		bool String::Match(const string a, const string b)
		{
			return Match(&a, &b);
		}

		float randomFloat(void)
		{
			return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		}

		float randomFloat(float low, float high)
		{
			return low + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high - low)));
		}

		vec4 randomVec4(float min, float max)
		{
			return vec4(Common::randomFloat(min, max), Common::randomFloat(min, max), Common::randomFloat(min, max), Common::randomFloat(min, max));
		}

		vec3 randomVec3(float min, float max)
		{
			return vec3(Common::randomFloat(min, max), Common::randomFloat(min, max), Common::randomFloat(min, max));
		}

		vec2 randomVec2(float min, float max)
		{
			return vec2(Common::randomFloat(min, max), Common::randomFloat(min, max));
		}
	}
}