#include "stdafx.h"
#ifdef CE_WIN

#include "SystemStatisticsWin.h"

namespace CypherEngine
{
	SystemStatisticsWin::SystemStatisticsWin()
	{

	}

	SystemStatisticsWin::~SystemStatisticsWin()
	{

	}

	void SystemStatisticsWin::create(float updateDuration)
	{
		m_updateDuration = updateDuration;

		m_frameClock.setSampleDepth(1000);

		setProcessHandle(reinterpret_cast<int>(GetCurrentProcess()));

		setCPUName();
		setGPUName();
		setOSName();
		setOSArchitecture();
		setTotalSystemMemory();
		setCPUCores();
		setVideoMemory();
		setOpenGLVersion();

		FILETIME filetime, systemFiletime, userFiletime;
		GetSystemTimeAsFileTime(&filetime);
		memcpy(&m_lastCPU, &filetime, sizeof(FILETIME));

		GetProcessTimes(reinterpret_cast<HANDLE>(m_process), &filetime, &filetime, &systemFiletime, &userFiletime);
		memcpy(&m_lastSystemCPU, &systemFiletime, sizeof(FILETIME));
		memcpy(&m_lastUserCPU, &userFiletime, sizeof(FILETIME));
		
		m_videoMemoryOffset = 0;
		updateGPUUsage();
		m_videoMemoryOffset = m_usedVideoMemory;
	}

	void SystemStatisticsWin::update(void)
	{
		if (m_updateClock.getElapsedTime().asSeconds() > m_updateDuration)
		{
			updateFrameInformation();
			updateSystemInfomration();
			updateUsedSystemMemory();
			updateCPUUsage();
			updateGPUUsage();
			m_updateClock.restart();
		}
	}

	void SystemStatisticsWin::updateUsedSystemMemory(void)
	{
		PROCESS_MEMORY_COUNTERS pmc;
		GetProcessMemoryInfo(reinterpret_cast<HANDLE>(m_process), &pmc, sizeof(pmc));
		m_usedSystemMemory = static_cast<unsigned int>(pmc.PagefileUsage / 1024 / 1024);
	}

	void SystemStatisticsWin::updateCPUUsage(void)
	{
		FILETIME filetime, systemFiletime, userFiletime;
		ULARGE_INTEGER now, system, user;
		float percent;

		GetSystemTimeAsFileTime(&filetime);
		memcpy(&now, &filetime, sizeof(FILETIME));

		GetProcessTimes(reinterpret_cast<HANDLE>(m_process), &filetime, &filetime, &systemFiletime, &userFiletime);
		memcpy(&system, &systemFiletime, sizeof(FILETIME));
		memcpy(&user, &userFiletime, sizeof(FILETIME));

		percent = (float)(system.QuadPart - m_lastSystemCPU.QuadPart) + (user.QuadPart - m_lastUserCPU.QuadPart);
		percent /= (now.QuadPart - m_lastCPU.QuadPart);
		percent /= m_cpuCores;

		m_lastCPU = now;
		m_lastUserCPU = user;
		m_lastSystemCPU = system;

		m_cpuUsage = static_cast<unsigned int>(percent * 100);
	}

	void SystemStatisticsWin::updateGPUUsage(void)
	{
		int availableKB[4];

		if (m_gpuManufacturer.find("AMD") != string::npos || m_gpuManufacturer.find("ATI") != string::npos)
		{
			if (GL_ATI_meminfo)
			{
				glGetIntegerv(GL_VBO_FREE_MEMORY_ATI, availableKB);
			}
		}
		else if (m_gpuManufacturer.find("NVIDIA") != string::npos)
		{
			if (GL_NVX_gpu_memory_info)
			{
				glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, availableKB);
			}
		}

		m_usedVideoMemory = (m_videoMemory - m_videoMemoryOffset) - (availableKB[0] / 1024);
	}

	void SystemStatisticsWin::setCPUCores(void)
	{
		if (m_cpuCores < 1)
		{
			SYSTEM_INFO sysInfo;
			GetSystemInfo(&sysInfo);
			m_cpuCores = sysInfo.dwNumberOfProcessors;
		}
	}

	void SystemStatisticsWin::setCPUName(void)
	{
		if (m_cpu.empty())
		{
			int CPUInfo[4] = { -1 };
			__cpuid(CPUInfo, 0x80000000);
			unsigned int nExIds = CPUInfo[0];

			for (unsigned int i = 0x80000000; i <= nExIds; ++i)
			{
				__cpuid(CPUInfo, i);
				if (i >= 0x80000002 && i <= 0x80000004)
				{
					m_cpu += string(((char *)CPUInfo), sizeof(CPUInfo));
				}
			}
		}
	}

	void SystemStatisticsWin::setGPUName(void)
	{
		m_gpu = string(reinterpret_cast<char const *>(glGetString(GL_RENDERER)));
		m_gpu = m_gpu.substr(0, m_gpu.find_first_of("//"));

		m_gpuManufacturer = string(reinterpret_cast<char const *>(glGetString(GL_VENDOR)));
	}

	void SystemStatisticsWin::setVideoMemory(void)
	{
		if (m_gpuManufacturer.find("AMD") != string::npos || m_gpuManufacturer.find("ATI") != string::npos)
		{
			wglGetGPUInfoAMD = (PFNWGLGETGPUINFOAMDPROC)wglGetProcAddress("wglGetGPUInfoAMD");
			wglGetGPUIDsAMD = (PFNWGLGETGPUIDSAMDPROC)wglGetProcAddress("wglGetGPUIDsAMD");

			noOfGPUs = wglGetGPUIDsAMD(0, 0);
			gpuIDs = new GLuint[noOfGPUs];
			wglGetGPUIDsAMD(noOfGPUs, gpuIDs);
			m_videoMemory = 0;
			wglGetGPUInfoAMD(gpuIDs[0], WGL_GPU_RAM_AMD, GL_UNSIGNED_INT, sizeof(GLuint), &m_videoMemory);

			delete gpuIDs;
		}
		else if (m_gpuManufacturer.find("NVIDIA") != string::npos)
		{
			GLint totalMemory = 0;
			glGetIntegerv(GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX, &totalMemory);
			m_videoMemory = totalMemory / 1024;
		}
	}

	void SystemStatisticsWin::setOpenGLVersion(void)
	{
		m_openglVersion = std::stof(string(reinterpret_cast<char const *>(glGetString(GL_VERSION))));
	}

	void SystemStatisticsWin::setOSName(void)
	{
		if (m_os.empty())
		{
			m_os = "Windows ";
			if (IsWindows7OrGreater())
			{
				if (IsWindows7SP1OrGreater())
				{
					if (IsWindows8OrGreater())
					{
						if (IsWindows8Point1OrGreater())
						{
							m_os += "8.1";
						}
						else
						{
							m_os += "8";
						}
					}
					else
					{
						m_os += "7 SP1";
					}
				}
				else
				{
					m_os += "7";
				}
			}
		}
	}


	void SystemStatisticsWin::setTotalSystemMemory(void)
	{
		if (m_totalSystemMemory < 1)
		{
			MEMORYSTATUSEX statex;
			statex.dwLength = sizeof(MEMORYSTATUSEX);
			GlobalMemoryStatusEx(&statex);
			m_totalSystemMemory = (int)(statex.ullTotalPhys / 1024 / 1024);
		}
	}

	void SystemStatisticsWin::setOSArchitecture(void)
	{
		if (m_osArchitecture < 1)
		{
			BOOL is64bit = FALSE;
			IsWow64Process(reinterpret_cast<HANDLE>(m_process), &is64bit);
			m_osArchitecture = is64bit ? 64 : 32;
		}
	}

	void SystemStatisticsWin::setProcessHandle(int process)
	{
		m_process = process;
	}

	void SystemStatisticsWin::setCurrentResolution(vec2 & currentResolution)
	{

	}

	void SystemStatisticsWin::setFullscreenResolutions(vector<vec2> & resolutions)
	{

	}
}

#endif