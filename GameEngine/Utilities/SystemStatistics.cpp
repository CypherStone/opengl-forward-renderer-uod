#include "stdafx.h"
#include "SystemStatistics.h"

namespace CypherEngine
{
	SystemStatistics::SystemStatistics(void)
	{

	}

	SystemStatistics::~SystemStatistics(void)
	{

	}

	void SystemStatistics::create(float updateDuration)
	{
		m_impl.create(updateDuration);
	}

	void SystemStatistics::update(void)
	{
		m_impl.update();
	}

	sfx::FrameClock & SystemStatistics::getFrameClock(void)
	{
		return SystemStatisticsImpl::getFrameClock();
	}

	const string & SystemStatistics::getCPUName(void)
	{
		return SystemStatisticsImpl::getCPUName();
	}

	const string & SystemStatistics::getGPUName(void)
	{
		return SystemStatisticsImpl::getGPUName();
	}

	const string & SystemStatistics::getOSName(void)
	{
		return SystemStatisticsImpl::getOSName();
	}

	vec2 SystemStatistics::getCurrentResolution(void)
	{
		return SystemStatisticsImpl::getCurrentResolution();
	}

	vector<vec2> & SystemStatistics::getFullscreenResolutions(void)
	{
		return SystemStatisticsImpl::getFullscreenResolutions();
	}

	unsigned int SystemStatistics::getTotalSystemMemory(void)
	{
		return SystemStatisticsImpl::getTotalSystemMemory();
	}

	unsigned int SystemStatistics::getUsedSystemMemory(void)
	{
		return SystemStatisticsImpl::getUsedSystemMemory();
	}

	unsigned int SystemStatistics::getVideoMemory(void)
	{
		return SystemStatisticsImpl::getVideoMemory();
	}

	unsigned int SystemStatistics::getOSArchitecture(void)
	{
		return SystemStatisticsImpl::getOSArchitecture();
	}

	unsigned int SystemStatistics::getCPUUsage(void)
	{
		return SystemStatisticsImpl::getCPUUsage();
	}

	unsigned int SystemStatistics::getUsedVideoMemory(void)
	{
		return SystemStatisticsImpl::getUsedVideoMemory();
	}

	unsigned int SystemStatistics::getCPUCores(void)
	{
		return SystemStatisticsImpl::getCPUCores();
	}

	float SystemStatistics::getOpenGLVersion(void)
	{
		return SystemStatisticsImpl::getOpenGLVersion();
	}

	void SystemStatistics::setProcessHandle(int process)
	{
		m_impl.setProcessHandle(process);
	}

	void SystemStatistics::createMeasurement(const string & name)
	{
		SystemStatisticsImpl::createMeasurement(name);
	}

	void SystemStatistics::startMeasurement(const string & name)
	{
		SystemStatisticsImpl::startMeasurement(name);
	}

	void SystemStatistics::endMeasurement(const string & name)
	{
		SystemStatisticsImpl::endMeasurement(name);
	}

	float SystemStatistics::getMeasurement(const string & name)
	{
		return SystemStatisticsImpl::getMeasurement(name);
	}

	const string & SystemStatistics::getFrameInformation(void)
	{
		return SystemStatisticsImpl::getFrameInformation();
	}

	const string & SystemStatistics::getSystemInformation(void)
	{
		return SystemStatisticsImpl::getSystemInformation();
	}
}