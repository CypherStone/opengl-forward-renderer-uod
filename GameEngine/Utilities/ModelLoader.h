#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class ModelLoader
	{
	public:
		static GameObject readOBJ(const string &filepath);
		static vector<MeshMaterial> getOBJMaterial(const string &filepath);
		static void CalculateTangentBinormal(Vertex a, Vertex b, Vertex c, glm::vec3 &tangent, glm::vec3 &binormal);
	};

}

