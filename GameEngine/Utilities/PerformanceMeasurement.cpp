#include "stdafx.h"
#include "PerformanceMeasurement.h"

namespace CypherEngine
{
	PerformanceMeasurement::PerformanceMeasurement(void)
	{
		m_time = sf::Time();
	}

	PerformanceMeasurement::~PerformanceMeasurement(void)
	{

	}

	void PerformanceMeasurement::start(void)
	{
		m_clock.restart();
	}

	void PerformanceMeasurement::end(void)
	{
		m_time = m_clock.getElapsedTime();
	}

	float PerformanceMeasurement::getTimeDuration(void)
	{
		return static_cast<float>(m_time.asMicroseconds() / 1000.0f);
	}
}