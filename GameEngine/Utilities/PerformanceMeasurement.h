#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class PerformanceMeasurement
	{
	private:
		sf::Clock m_clock;
		sf::Time m_time;
		
	public:
		PerformanceMeasurement(void);
		~PerformanceMeasurement(void);

		void start(void);
		void end(void);

		float getTimeDuration(void);
	};
}
