#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	namespace Common
	{	
		namespace String
		{
			string BuildFilePath(const string * fileString, const string * filename);

			bool Match(const string * a, const string * b);
			bool Match(const string * a, const string b);
			bool Match(const string a, const string * b);
			bool Match(const string a, const string b);
		};

		float randomFloat(void);
		float randomFloat(float low, float high);

		vec4 randomVec4(float min, float max);
		vec3 randomVec3(float min, float max);
		vec2 randomVec2(float min, float max);
	};
};