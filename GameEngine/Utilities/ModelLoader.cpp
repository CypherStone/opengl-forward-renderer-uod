#include "stdafx.h"
#include "ModelLoader.h"

namespace CypherEngine
{
	GameObject ModelLoader::readOBJ(const string &filepath)
	{
		std::ifstream objFile(filepath, std::ifstream::in);

		if (objFile)
		{
			GameObject obj = GameObject("", "");
			const int					size = 1024;
			string					line;
			vector<Mesh>			tmpMeshes;
			vector<Vertex>			tmpVertexes;
			vector<glm::vec2>		tmpTexels;
			vector<glm::vec3>		tmpNormals;
			vector<Vertex>			vertexes;
			vector<MeshMaterial>	materials;
			string					mtlLocation;
			int	materialNo = 0;
			bool normals = false;
			bool textures = false;
			bool activateMeshes = false;

			while (getline(objFile, line))
			{
				std::istringstream istring(line, std::istringstream::in);
				string		type;
				Vertex		vertex;
				glm::vec2 tmpTexel;
				glm::vec3 tmpNormal;
				int			v1, v2, v3;
				int			vt1, vt2, vt3;
				int			vn1, vn2, vn3;
				char		disposable;

				istring >> type;

				if (line.empty() || Common::String::Match(type, "#"))
				{
					continue;
				}

				if (Common::String::Match(type, "mtllib"))
				{
					istring >> mtlLocation;
					mtlLocation = Common::String::BuildFilePath(&filepath, &mtlLocation);
					materials = getOBJMaterial(mtlLocation);
				}
				else if (Common::String::Match(type, "usemtl"))
				{
					if (activateMeshes)
					{
						tmpMeshes.push_back(Mesh(materials[materialNo], vertexes));
						vertexes.clear();
					}
					else
					{
						activateMeshes = true;
					}

					istring >> type;
					for (int i = 0; i < (int)materials.size(); i++)
					{
						if (Common::String::Match(type, materials[i].materialName.c_str()))
						{
							materialNo = i;
							break;
						}
					}
				}
				else if (Common::String::Match(type, "v"))
				{
					istring >> vertex.position.x >> vertex.position.y >> vertex.position.z;
					tmpVertexes.push_back(vertex);
				}
				else if (Common::String::Match(type, "vt"))
				{
					textures = true;
					istring >> tmpTexel.x >> tmpTexel.y;
					tmpTexels.push_back(tmpTexel);
				}
				else if (Common::String::Match(type, "vn"))
				{
					normals = true;
					istring >> tmpNormal.x >> tmpNormal.y >> tmpNormal.z;
					tmpNormals.push_back(tmpNormal);
				}
				else if (type[0] == 'f')
				{
					if (textures && normals)
					{
						istring >> v1 >> disposable >> vt1 >> disposable >> vn1
							>> v2 >> disposable >> vt2 >> disposable >> vn2
							>> v3 >> disposable >> vt3 >> disposable >> vn3;

						unsigned int vert1 = v1 - 1;
						unsigned int vert2 = v2 - 1;
						unsigned int vert3 = v3 - 1;

						tmpVertexes[vert1].normal = tmpNormals[vn1 - 1];
						tmpVertexes[vert2].normal = tmpNormals[vn2 - 1];
						tmpVertexes[vert3].normal = tmpNormals[vn3 - 1];

						tmpVertexes[vert1].texel = tmpTexels[vt1 - 1];
						tmpVertexes[vert2].texel = tmpTexels[vt2 - 1];
						tmpVertexes[vert3].texel = tmpTexels[vt3 - 1];

						glm::vec3 tangent, binormal;
						CalculateTangentBinormal(tmpVertexes[vert1], tmpVertexes[vert2], tmpVertexes[vert3], tangent, binormal);

						tmpVertexes[vert1].binormal = binormal;
						tmpVertexes[vert1].tangent = tangent;

						tmpVertexes[vert2].binormal = binormal;
						tmpVertexes[vert2].tangent = tangent;

						tmpVertexes[vert3].binormal = binormal;
						tmpVertexes[vert3].tangent = tangent;

						vertexes.push_back(tmpVertexes[vert1]);
						vertexes.push_back(tmpVertexes[vert2]);
						vertexes.push_back(tmpVertexes[vert3]);
					}
				}
			}

			objFile.close();

			tmpMeshes.push_back(Mesh(materials[materialNo], vertexes));
			obj.addMeshes(tmpMeshes);

			return obj;
		}
		else
		{
			throw new std::exception("INVALID OBJ FILEPATH");
		}
	}

	std::vector<CypherEngine::MeshMaterial> CypherEngine::ModelLoader::getOBJMaterial(const string &filepath)
	{
		std::ifstream meshFile(filepath);
		if (meshFile)
		{
			vector<MeshMaterial> materials;
			const int		size = 1024;
			string			line;
			MeshMaterial	material;
			bool			inMaterial = false;
			bool			opacity = false;

			while (std::getline(meshFile, line))
			{
				std::istringstream istring(line, std::istringstream::in);
				string type;
				istring >> type;

				if (type.empty() || Common::String::Match(type, "#"))
				{
					continue;
				}

				if (Common::String::Match(type, "newmtl"))
				{
					/*
					Finds a new material.
					If not in a material, it setups the proceedure of storing it - this is usually the beginning.
					Else, it has completed a material strucTexture and adds it the to the vector.
					*/
					if (!inMaterial)
					{
						inMaterial = true;
					}
					else if (inMaterial)
					{
						if (material.opacityTexture.empty())
						{
							material.opacityTexture = "..\\Textures\\white.png";
							material.opacityTexture = Common::String::BuildFilePath(&filepath, &material.opacityTexture);
						}
						materials.push_back(material);
					}

					material = MeshMaterial();
					istring >> material.materialName;
				}
				else if (Common::String::Match(type, "Ka"))
				{
					istring >> material.ambient.r >> material.ambient.g >> material.ambient.b;
				}
				else if (Common::String::Match(type, "Kd"))
				{
					istring >> material.diffuse.r >> material.diffuse.g >> material.diffuse.b;
				}
				else if (Common::String::Match(type, "Ks"))
				{
					istring >> material.specular.r >> material.specular.g >> material.specular.b;
				}
				else if (Common::String::Match(type, "Ns"))
				{
					istring >> material.specularItensity;
				}
				else if (Common::String::Match(type, "d"))
				{
					istring >> material.alpha;
				}
				else if (Common::String::Match(type, "illum"))
				{
					istring >> material.illumination;
				}
				else if (Common::String::Match(type, "map_Kd"))
				{
					istring >> material.diffuseTexture;
					material.diffuseTexture = Common::String::BuildFilePath(&filepath, &material.diffuseTexture);
				}
				else if (Common::String::Match(type, "map_Kn"))
				{
					istring >> material.normalTexture;
					material.normalTexture = Common::String::BuildFilePath(&filepath, &material.normalTexture);
				}
				else if (Common::String::Match(type, "map_Ks"))
				{
					istring >> material.specularTexture;
					material.specularTexture = Common::String::BuildFilePath(&filepath, &material.specularTexture);
				}
				else if (Common::String::Match(type, "map_Ko"))
				{
					istring >> material.opacityTexture;
					material.opacityTexture = Common::String::BuildFilePath(&filepath, &material.opacityTexture);
				}
			}

			if (material.opacityTexture.empty())
			{
				material.opacityTexture = "..\\Textures\\white.png";
				material.opacityTexture = Common::String::BuildFilePath(&filepath, &material.opacityTexture);
			}

			materials.push_back(material);
			meshFile.close();

			return materials;
		}
		else
		{
			throw new std::exception("INVALID OBJ MATERIAL FILEPATH");
		}
	}

	void ModelLoader::CalculateTangentBinormal(Vertex a, Vertex b, Vertex c, glm::vec3 &tangent, glm::vec3 &binormal)
	{
		float vector1[3], vector2[3];
		float tuVector[2], tvVector[2];

		vector1[0] = b.position.x - a.position.x;
		vector1[1] = b.position.y - a.position.y;
		vector1[2] = b.position.z - a.position.z;

		vector2[0] = c.position.x - a.position.x;
		vector2[1] = c.position.y - a.position.y;
		vector2[2] = c.position.z - a.position.z;

		// Calculate the tu and tv texture space vectors. 
		tuVector[0] = b.texel.x - a.texel.x;
		tvVector[0] = b.texel.y - a.texel.y;

		tuVector[1] = c.texel.x - a.texel.x;
		tvVector[1] = c.texel.y - a.texel.y;

		// Calculate the denominator of the tangent/binormal equation. 
		float den = 1.0f / (tuVector[0] * tvVector[1] - tuVector[1] * tvVector[0]);

		// Calculate the cross products and multiply by the coefficient to get the tangent and binormal. 
		tangent.x = (tvVector[1] * vector1[0] - tvVector[0] * vector2[0]) * den;
		tangent.y = (tvVector[1] * vector1[1] - tvVector[0] * vector2[1]) * den;
		tangent.z = (tvVector[1] * vector1[2] - tvVector[0] * vector2[2]) * den;

		binormal.x = (tuVector[0] * vector2[0] - tuVector[1] * vector1[0]) * den;
		binormal.y = (tuVector[0] * vector2[1] - tuVector[1] * vector1[1]) * den;
		binormal.z = (tuVector[0] * vector2[2] - tuVector[1] * vector1[2]) * den;

		// Calculate the length of this normal. 
		float length = sqrt((tangent.x * tangent.x) + (tangent.y * tangent.y) + (tangent.z * tangent.z));

		// Normalize the normal and then store it 
		tangent.x = tangent.x / length;
		tangent.y = tangent.y / length;
		tangent.z = tangent.z / length;

		// Calculate the length of this normal. 
		length = sqrt((binormal.x * binormal.x) + (binormal.y * binormal.y) + (binormal.z * binormal.z));

		// Normalize the normal and then store it 
		binormal.x = binormal.x / length;
		binormal.y = binormal.y / length;
		binormal.z = binormal.z / length;
	}
}