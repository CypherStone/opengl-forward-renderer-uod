#pragma once
#include "stdafx.h"
#include "FrameClock.h"

namespace CypherEngine
{
	class SystemStatisticsImpl
	{
	protected:

		sf::Clock m_updateClock;
		float m_updateDuration;

		static unordered_map<string, PerformanceMeasurement> m_performanceMeasure;
		static sfx::FrameClock m_frameClock;
		static int m_process;
		static string m_cpu;
		static string m_gpu;
		static string m_gpuManufacturer;
		static string m_os;
		static vec2 m_currentResolution;
		static vector<vec2> m_fullscreenResolutions;
		static unsigned int m_cpuCores;
		static unsigned int m_totalSystemMemory;
		static unsigned int m_usedSystemMemory;
		static unsigned int m_videoMemory;
		static unsigned int m_videoMemoryOffset;
		static unsigned int m_usedVideoMemory;
		static unsigned int m_osArchitecture;
		static unsigned int m_cpuUsage;
		static float m_openglVersion;
		
		static string m_information;
		static string m_frameInformation;

		void setCPUCores(void);
		void setCPUName(void);
		void setGPUName(void);
		void setOSName(void);
		void setOSArchitecture(void);
		void setTotalSystemMemory(void);
		void setVideoMemory(void);
		void setOpenGLVersion(void);

		void updateFrameInformation(void);
		void updateSystemInfomration(void);
		void updateUsedSystemMemory(void);
		void updateCPUUsage(void);
		void updateUsedVideoMemory(void);

	public:
		SystemStatisticsImpl(void);
		~SystemStatisticsImpl(void);

		virtual void create(float updateDuration) = 0;
		virtual void update(void) = 0;

		static sfx::FrameClock & getFrameClock(void);
		static const string & getCPUName(void);
		static const string & getGPUName(void);
		static const string & getOSName(void);
		static vec2 getCurrentResolution(void);
		static vector<vec2> & getFullscreenResolutions(void);
		static unsigned int getTotalSystemMemory(void);
		static unsigned int getUsedSystemMemory(void);
		static unsigned int getVideoMemory(void);
		static unsigned int getOSArchitecture(void);
		static unsigned int getCPUUsage(void);
		static unsigned int getUsedVideoMemory(void);
		static unsigned int getCPUCores(void);
		static float getOpenGLVersion(void);

		static void createMeasurement(const string & name);
		static void startMeasurement(const string & name);
		static void endMeasurement(const string & name);
		static float getMeasurement(const string & name);

		static const string & getSystemInformation(void);
		static const string & getFrameInformation(void);

		virtual void setProcessHandle(int process) = 0;

		virtual void setCurrentResolution(vec2 & currentResolution) = 0;
		virtual void setFullscreenResolutions(vector<vec2> & resolutions) = 0;
	};
}