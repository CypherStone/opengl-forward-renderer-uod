#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class GameEngine
	{
	private:
		System * m_system;
		RenderEngine * m_renderEngine;
		SceneManager * m_sceneManager;

		bool m_created;

		void update(void);
		void draw(void);

	public:

		GameEngine(void);
		~GameEngine(void);

		bool	create(unsigned short width, unsigned short height, bool fullscreen, bool vsync);
		void	run(void);
		void	destroy(void);		
	};
};