#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class LightManager
	{
	private:
		vector<shared_ptr<Light>> m_lights;

		GLuint m_directionallightBuffer;
		GLuint m_pointlightBuffer;
		GLuint m_spotlightBuffer;

		int m_directionallightBinding;
		int m_pointlightBinding;
		int m_spotlightBinding;
		int m_pointLightCount;
		int m_spotLightCount;	
		int m_directionalLightCount;

		short m_width;
		short m_height;

		bool m_created;

		bool m_rebind;

	public:
		/* Constructor Functions */
		LightManager(void);
		~LightManager(void);

		/* Public Functions */
		bool create(short width, short height);
		void bindLights(Shader & shader);
		void bindPointLights(void);
		void attachLight(shared_ptr<Light> & light);
		void dettachLight(shared_ptr<Light> & light);

		/* Get Functions */

		vector<shared_ptr<Light>> & getLights(void);
	};
};