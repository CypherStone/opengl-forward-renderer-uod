#include "stdafx.h"
#include "LightManager.h"

namespace CypherEngine
{
	LightManager::LightManager(void)
	{
		m_created = false;
	}

	LightManager::~LightManager(void)
	{
		glDeleteBuffers(1, &m_pointlightBuffer);
		glDeleteBuffers(1, &m_spotlightBuffer);
		glDeleteBuffers(1, &m_directionallightBuffer);
	}

	/* Public Functions */
	bool LightManager::create(short width, short height)
	{
		m_width = width;
		m_height = height;

		glGenBuffers(1, &m_pointlightBuffer);
		if (m_pointlightBuffer == -1)
		{
			return false;
		}

		glGenBuffers(1, &m_spotlightBuffer);
		if (m_spotlightBuffer == -1)
		{
			return false;
		}

		glGenBuffers(1, &m_directionallightBuffer);
		if (m_spotlightBuffer == -1)
		{
			return false;
		}

		m_pointlightBinding = 3;
		m_spotlightBinding = 4;
		m_directionallightBinding = 5;

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_pointlightBuffer);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, m_pointlightBinding, m_pointlightBuffer);
		
		//glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_spotlightBuffer);
		//glBindBufferBase(GL_SHADER_STORAGE_BUFFER, m_spotlightBinding, m_spotlightBuffer);

		//glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_directionallightBuffer);
		//glBindBufferBase(GL_SHADER_STORAGE_BUFFER, m_directionallightBinding, m_directionallightBuffer);

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

		//for (auto & light : m_lights)
		//{
		//	if (!light->create(width, height))
		//	{
		//		return false;
		//	}
		//}

		m_rebind = true;
		m_created = true;

		return true;
	}
	
	void LightManager::bindLights(Shader & shader)
	{
		vector<DirectionalLightShaderBuffer> directionalLights;
		vector<PointLightShaderBuffer> pointLights;
		vector<SpotLightShaderBuffer> spotLights;
		glm::mat4 biasMatrix(
			0.5, 0.0, 0.0, 0.0,
			0.0, 0.5, 0.0, 0.0,
			0.0, 0.0, 0.5, 0.0,
			0.5, 0.5, 0.5, 1.0);

		int texture = SHADOW_TEXTURE_START;

		for (auto & light : m_lights)
		{
			if (light->isActive())
			{
				std::ostringstream shadow;
				auto directionalLight = dynamic_cast<DirectionalLight *>(light.get());
				auto pointlight = dynamic_cast<PointLight *>(light.get());
				auto spotlight = dynamic_cast<SpotLight *>(light.get());

				if (directionalLight)
				{
					if (directionalLight->getShadow().getTextureID() == -1)
					{
						shadow << "directionalLightShadows[" << directionalLights.size() << "]";
						directionalLight->getShadow().setTextureID(shader.getUniformLocation(shadow.str()));
					}

					shader.bindTexture(directionalLight->getShadow().getTextureID(), texture, directionalLight->getShadow().getTexture());
					directionalLight->getLightProperties().shadowMatrix = biasMatrix * directionalLight->getLightProperties().shadowMatrix;
					directionalLights.push_back(directionalLight->getLightProperties());
				}		
				else if (pointlight)
				{
					if (pointlight->getShadow().getTextureID() == -1)
					{
						shadow << "pointLightShadows[" << pointLights.size() << "]";
						pointlight->getShadow().setTextureID(shader.getUniformLocation(shadow.str()));
					}
					shader.bindTexture(pointlight->getShadow().getTextureID(), texture,	pointlight->getShadow().getTexture(), GL_TEXTURE_CUBE_MAP);

					pointLights.push_back(pointlight->getLightProperties());
				}
				else if (spotlight)
				{
					if (spotlight->getShadow().getTextureID() == -1)
					{
						shadow << "spotLightShadows[" << spotLights.size() << "]";
						spotlight->getShadow().setTextureID(shader.getUniformLocation(shadow.str()));
					}	
					shader.bindTexture(spotlight->getShadow().getTextureID(), texture,
						spotlight->getShadow().getTexture());

					spotlight->getLightProperties().shadowMatrix = biasMatrix * spotlight->getLightProperties().shadowMatrix;
					spotLights.push_back(spotlight->getLightProperties());
				}

				texture++;
			}
		}

		shader.bindUniformInteger("directionalLightCount", directionalLights.size());
		shader.bindUniformInteger("pointLightCount", pointLights.size());
		shader.bindUniformInteger("spotLightCount", spotLights.size());

		if (directionalLights.size() > 0)
		{
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_directionallightBuffer);
			glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(DirectionalLightShaderBuffer) * directionalLights.size(), &directionalLights[0], GL_STATIC_DRAW);
		}

		if (pointLights.size() > 0)
		{
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_pointlightBuffer);
			glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(PointLightShaderBuffer) * pointLights.size(), &pointLights[0], GL_STATIC_DRAW);
		}

		if (spotLights.size() > 0)
		{
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_spotlightBuffer);
			glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(SpotLightShaderBuffer) * spotLights.size(), &spotLights[0], GL_STATIC_DRAW);
		}

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}

	void LightManager::bindPointLights(void)
	{
		if (m_rebind)
		{
			vector<PointLightShaderBuffer> pointLights;

			for (auto & light : m_lights)
			{
				auto pointlight = dynamic_cast<PointLight *>(light.get());
				if (pointlight)
				{
					pointLights.push_back(pointlight->getLightProperties());
				}
			}

			if (pointLights.size() > 0)
			{
				glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_pointlightBuffer);
				glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(PointLightShaderBuffer) * pointLights.size(), pointLights.data(), GL_STATIC_DRAW);
			}

			m_rebind = false;
		}
	}
	void LightManager::attachLight(shared_ptr<Light> & light)
	{
		if (light)
		{
			if (light->getID() == -1)
			{
				while (true)
				{
					int id = rand() % INT_MAX;
					light->setID(id);

					for (auto iLight : m_lights)
					{
						if (light->getID() == iLight->getID())
						{
							continue;
						}
					}

					break;
				}
			}
			else
			{
				for (auto & iLight : m_lights)
				{
					if (light->getID() == iLight->getID())
					{
						return;
					}
				}
			}

			if (m_created)
			{
				//light->create(m_width, m_height);
			}

			m_lights.push_back(light);

			m_rebind = false;
		}
	}

	void LightManager::dettachLight(shared_ptr<Light> & light)
	{
		if (light)
		{
			for (auto & iLight = m_lights.begin(); iLight != m_lights.end(); ++iLight)
			{
				if (light->getID() == (*iLight)->getID())
				{
					m_lights.erase(iLight);
					m_rebind = true;
					break;
				}
			}
		}
	}

	/* Get Functions */

	vector<shared_ptr<Light>> & LightManager::getLights(void)
	{
		return m_lights;
	}

}
