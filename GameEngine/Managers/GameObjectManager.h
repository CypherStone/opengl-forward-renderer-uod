#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class GameObjectManager : public Manager
	{
	private:
		map<string, GameObject> gameObjects;
		map<string, GameObject>::iterator iter;
		void resourceLoad(void);

	public:
		GameObjectManager(void);
		~GameObjectManager(void);

		void destroy(void);
		void load(void);

		bool hasGameObject(const string &id);
		bool hasGameObject(const int &id);
		int getGameObjectCount(void);
		GameObject getGameObject(const string &id);
		GameObject getGameObject(const int &id);
		vector<string> getGameObjectTextures(void);

		void setGameObjectTextures(const map<string, GLuint> & textures);

		void destroyGameObject(const string &id);
	};

}