#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class ShaderManager
	{
	private:
		static ShaderManager * m_instance;
		vector<shared_ptr<Shader>> m_shaders;
		shared_ptr<Shader> m_currentShader;
		string m_filepath;

		ShaderManager(void);

		bool parseShaderFiles(const string & filepath);
		void setShaderVariables(shared_ptr<Shader> shader, tinyxml2::XMLElement * element, bool uniform);

	public:

		/* Constructor Functions*/
		static ShaderManager * getInstance(void);
		bool create(const string & filepath);
		void destroy(void);

		/* Public Functions */
		shared_ptr<Shader> activateShader(const string & name);
		void deactivateShader(void);

		/* Get Functions */
		shared_ptr<Shader> getShader(const string & name);
		shared_ptr<Shader> getActiveShader(void);

		static void reloadShaders(void);
	};

}