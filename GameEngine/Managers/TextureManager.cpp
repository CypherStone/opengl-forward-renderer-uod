﻿#include "stdafx.h"
#include "TextureManager.h"

namespace CypherEngine
{
	TextureManager::TextureManager(void)
	{

	}

	TextureManager::~TextureManager(void)
	{

	}

	void TextureManager::destroy(void)
	{
		if (!loading)
		{
			loadingMutex.lock();
			textures.clear();
			degenerateTextures();
			textureIDs.clear();
			loadingMutex.unlock();
		}
	}

	void TextureManager::resourceLoad(void)
	{
		loadingMutex.lock();
		resourceIter = resourceQueue.begin();

		sf::Context context;
		while (resourceIter != resourceQueue.end())
		{
			string textureName = (*resourceIter).first;
			auto cutname = textureName.substr(textureName.find_last_of("\\") + 1);

			System::writeDebugString("Texture: " + cutname + " is loading.");

			sf::Image tmpTexture;
			if (!tmpTexture.loadFromFile(textureName))
			{
				System::getInstance()->showErrorMessageBox("Error", "Texture missing - " + textureName);
			}

			textures.insert(pair<string, sf::Image>(textureName, tmpTexture));
			generateTexture(getTextureID(textureName), getTexture((*resourceIter).first), true);

			System::writeDebugString("Texture: " + cutname + " has loaded.");

			resourceIter = resourceQueue.erase(resourceIter);
		}

		resourceQueue.clear();
		loading = false;
		resourceCount = textures.size();
		//textures.clear();

		loadingMutex.unlock();
	}

	void TextureManager::generateTexture(const GLuint &handle, const sf::Image & image, bool mipmap)
	{
		glBindTexture(GL_TEXTURE_2D, handle);
		glTexStorage2D(GL_TEXTURE_2D, mipmap ? 4 : 1, GL_RGBA8, image.getSize().x, image.getSize().y);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image.getSize().x, image.getSize().y, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());		

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		if (mipmap)
		{
			glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void TextureManager::addTextures(const vector<string> & texturePaths)
	{
		for (unsigned int i = 0; i < texturePaths.size(); i++)
		{
			addTexture(texturePaths.at(i));
		}
		created = true;
	}

	void TextureManager::addTexture(const string& texturePath)
	{
		resourceQueue.insert(pair<string, string>(texturePath, string()));
		GLuint newTextureID;
		glGenTextures(1, &newTextureID);
		textureIDs.insert(pair<string, GLuint>(texturePath, newTextureID));
	}

	void TextureManager::degenerateTextures(void)
	{
		textureIDIterator = textureIDs.begin();
		while (textureIDIterator != textureIDs.end())
		{
			glDeleteTextures(1, &(*textureIDIterator).second);
			textureIDIterator++;
		}
	}

	const sf::Image & TextureManager::getTexture(const string &id)
	{
		textureIterator = textures.find(id);
		if (textureIterator != textures.end())
		{
			return (*textureIterator).second;
		}
		else
		{
			return textures.at(0);
		}
	}

	const GLuint & TextureManager::getTextureID(const string& id)
	{
		textureIDIterator = textureIDs.find(id);
		if (textureIDIterator != textureIDs.end())
		{
			return (*textureIDIterator).second;
		}
		else
		{
			return textureIDs.at(0);
		}
	}

	const map<string, GLuint> & TextureManager::getTextureIDs(void)
	{
		return textureIDs;
	}
}
