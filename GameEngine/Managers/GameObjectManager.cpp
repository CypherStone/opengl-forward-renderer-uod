#include "stdafx.h"
#include "GameObjectManager.h"

namespace CypherEngine
{
	GameObjectManager::GameObjectManager() : Manager()
	{
		finished = false;
		loading = false;
	}

	GameObjectManager::~GameObjectManager()
	{

	}

	void GameObjectManager::destroy(void)
	{
		if (!loading)
		{
			loadingMutex.lock();
			for (auto & gameobject : gameObjects)
			{
				gameobject.second.destroy();
			}
			gameObjects.clear();
			loadingMutex.unlock();
		}
	}

	void GameObjectManager::load(void)
	{
		if (!loading)
		{
			loading = true;
			workerThread = thread(&GameObjectManager::resourceLoad, this);
		}
	}

	void GameObjectManager::resourceLoad(void)
	{
		auto system = System::getInstance();

		loadingMutex.lock();
		for (resourceIter = resourceQueue.begin(); resourceIter != resourceQueue.end(); )
		{
			System::writeDebugString("GameObject: " + (*resourceIter).first + " is loading.");

			auto filepath = system->getFileStructure().models + "\\" + (*resourceIter).second;
			gameObjects.insert(pair<string, GameObject>((*resourceIter).first, ModelLoader::readOBJ(filepath)));
			gameObjects.find((*resourceIter).first)->second.setName((*resourceIter).first);

			System::writeDebugString("GameObject: " + (*resourceIter).first + " has loaded.");

			resourceIter = resourceQueue.erase(resourceIter);
		}

		resourceCount = gameObjects.size();
		loadingMutex.unlock();

		finished = true;
		loading = false;
	}


	bool GameObjectManager::hasGameObject(const string &id)
	{
		loadingMutex.lock();
		iter = gameObjects.find(id);
		bool result = (iter != gameObjects.end());
		loadingMutex.unlock();
		return result;
	}

	bool GameObjectManager::hasGameObject(const int &id)
	{
		int count = 0;
		bool result = false;
		loadingMutex.lock();
		for (auto gameobject : gameObjects)
		{
			if (count == id)
			{
				result = true;
				break;
			}
			count++;
		}
		loadingMutex.unlock();
		return result;
	}

	GameObject GameObjectManager::getGameObject(const string &id)
	{
		iter = gameObjects.find(id);
		if (iter != gameObjects.end())
		{
			return (*iter).second;
		}
		else
		{
			return gameObjects.at(0);
		}
	}

	GameObject GameObjectManager::getGameObject(const int &id)
	{
		bool found = false;
		int count = 0;

		loadingMutex.lock();
		for (auto gameobject : gameObjects)
		{
			if (count == id)
			{
				found = true;
				break;
			}
			count++;
		}
		GameObject * obj = &(*iter).second;
		loadingMutex.unlock();

		if (found)
		{
			return *obj;
		}
		else
		{
			return gameObjects.at(0);
		}
	}

	vector<string> GameObjectManager::getGameObjectTextures(void)
	{
		vector<string> textures;

		for (auto & gameobject : gameObjects)
		{
			auto strings = gameobject.second.getTextures();
			textures.insert(textures.end(), strings.begin(), strings.end());
		}

		return textures;
	}

	void GameObjectManager::setGameObjectTextures(const map<string, GLuint> & textures)
	{
		for (auto & gameobject : gameObjects)
		{
			gameobject.second.setTextures(textures);
		}
	}

	void GameObjectManager::destroyGameObject(const string &id)
	{
		iter = gameObjects.find(id);
		if (iter != gameObjects.end())
		{
			gameObjects.erase(iter);
		}
	}
}