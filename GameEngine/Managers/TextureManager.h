#pragma once
namespace CypherEngine
{
	class TextureManager : public Manager
	{
	private:
		map<string, sf::Image> textures;
		map<string, sf::Image>::iterator textureIterator;
		map<string, GLuint> textureIDs;
		map<string, GLuint>::iterator textureIDIterator;

		void resourceLoad(void);
		void generateTexture(const GLuint &handle, const sf::Image &image, bool mipmap);
		void degenerateTextures(void);

	public:
		TextureManager(void);
		~TextureManager(void);

		void destroy(void);

		void addTextures(const vector<string> & texturePaths);
		void addTexture(const string& texturePath);
		const sf::Image & getTexture(const string& texturePath);
		const GLuint & getTextureID(const string& texturePath);
		const map<string, GLuint> & getTextureIDs(void);
	};

}