#include "stdafx.h"
#include "ResourceManager.h"

namespace CypherEngine
{
	/* Constructor Functions */

	ResourceManager::ResourceManager()
	{

	}

	ResourceManager::~ResourceManager()
	{

	}

	/* Public Functions */

	bool ResourceManager::create(const string &resourceFile)
	{
		if (!parseResourceFile(resourceFile))
		{
			return false;
		}

		m_gameObjectManager.create(m_gameobjects);

		m_fontManager.create(m_fonts);

		return true;
	}

	void ResourceManager::load(void)
	{
		m_gameObjectManager.load();
		m_fontManager.load();
		m_loadingGameObjectTextures = true;
	}

	void ResourceManager::update(void)
	{
		m_fontManager.update();
		m_gameObjectManager.update();

		if (m_loadingGameObjectTextures)
		{
			if (m_gameObjectManager.isFinished())
			{
				if (!m_textureManager.isCreated())
				{
					m_textureManager.addTextures(m_gameObjectManager.getGameObjectTextures());
					m_textureManager.load();
				}
				else
				{
					m_textureManager.update();
					if (m_textureManager.isFinished())
					{
						m_gameObjectManager.setGameObjectTextures(m_textureManager.getTextureIDs());
						m_loadingGameObjectTextures = false;
					}
				}
			}
		}

	}

	void ResourceManager::destroyFonts(void)
	{
		m_fontManager.destroy();
	}

	void ResourceManager::destroyGameObjects(void)
	{
		m_gameObjectManager.destroy();
	}

	void ResourceManager::destroyImages(void)
	{
		m_textureManager.destroy();
	}

	void ResourceManager::destroy(void)
	{
		destroyFonts();
		destroyGameObjects();
		destroyImages();
	}

	/* Get Functions */

	FontManager& ResourceManager::getFontManager(void)
	{
		return m_fontManager;
	}

	GameObjectManager& ResourceManager::getGameObjectManager(void)
	{
		return m_gameObjectManager;
	}

	GameObject ResourceManager::getGameObject(const string& id, bool destroy)
	{
		GameObject tmp = GameObject(m_gameObjectManager.getGameObject(id));

		if (destroy)
		{
			m_gameObjectManager.destroyGameObject(id);
		}

		return tmp;
	}

	bool ResourceManager::isLoading(void)
	{
		return isFontsLoading() || isGameObjectsLoading() || isTexturesLoading();
	}

	bool ResourceManager::isFontsLoading(void)
	{
		return m_fontManager.isLoading() || !m_fontManager.isFinished();
	}

	bool ResourceManager::isGameObjectsLoading(void)
	{
		return m_gameObjectManager.isLoading() || m_loadingGameObjectTextures;
	}

	bool ResourceManager::isTexturesLoading(void)
	{
		return m_textureManager.isLoading() || !m_textureManager.isFinished();
	}

	/* Set Functions */

	/* Private Functions */

	bool ResourceManager::parseResourceFile(const string & filepath)
	{
		using namespace tinyxml2;
		auto system = System::getInstance();

		tinyxml2::XMLDocument xmlFile;
		if (xmlFile.LoadFile(filepath.c_str()) != tinyxml2::XML_NO_ERROR)
		{
			return false;
		}

		auto resource = xmlFile.FirstChild()->FirstChildElement("Scene");
		if (!resource)
		{
			return false;
		}

		if (!parseResourceType("GameObject", resource->FirstChildElement("GameObjects"), m_gameobjects))
		{
			return false;
		}

		if (!parseResourceType("Font", resource->FirstChildElement("Fonts"), m_fonts))
		{
			return false;
		}

		return true;
	}

	bool ResourceManager::parseResourceType(const string & type, tinyxml2::XMLElement * elements, map<string, string> & storage)
	{
		using namespace tinyxml2;
		auto system = System::getInstance();

		if (!elements)
		{
			return false;
		}

		string name, path;
		for (auto element = elements->FirstChildElement(type.c_str());
			element != nullptr; element = element->NextSiblingElement(type.c_str()))
		{
			
			name = element->GetText();
			if (name.empty())
			{
				system->showErrorMessageBox("Error - " + type, "Found " + type + " with no unique name.");
				return false;
			}

			path = element->Attribute("path");
			if (path.empty())
			{
				system->showErrorMessageBox("Error - " + type, "Found " + type + " with no path.");
				return false;
			}

			storage.insert(std::pair<string, string>(name, path));
		}

		return true;
	}
}