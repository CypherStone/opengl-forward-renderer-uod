#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class ResourceManager
	{
	private:
		FontManager m_fontManager;
		GameObjectManager m_gameObjectManager;
		TextureManager m_textureManager;
		bool m_loadingGameObjectTextures;

		map<string, string> m_fonts;
		map<string, string> m_gameobjects;

		bool parseResourceFile(const string & filepath);
		bool parseResourceType(const string & type, tinyxml2::XMLElement * element, map<string, string> & storage);

	public:
		/* Constructor Functions */
		ResourceManager(void);
		~ResourceManager(void);

		/* Public Functions */
		bool create(const string &resourceFile);
		void load(void);
		void update(void);
		void destroyGameObjects(void);
		void destroyImages(void);
		void destroyFonts(void);
		void destroyTextures(void);
		void destroy(void);

		/* Get Functions */
		FontManager &getFontManager(void);
		GameObjectManager &getGameObjectManager(void);
		GameObject getGameObject(const string &id, bool destroy);
		bool isLoading(void);
		bool isFontsLoading(void);
		bool isGameObjectsLoading(void);
		bool isTexturesLoading(void);

		/* Set Functions */

	};
}


