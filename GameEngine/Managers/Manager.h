#pragma once
#include "stdafx.h"
#include "Time.h"

namespace CypherEngine
{
	class Manager
	{
	protected:
		map<string, string> resourceQueue;
		map<string, string>::iterator resourceIter;
		thread workerThread;
		mutex loadingMutex;
		bool created;
		bool loading;
		bool finished;
		unsigned int resourceCount;

		virtual void resourceLoad(void) = 0;

	public:
		Manager(void);
		virtual ~Manager(void);

		virtual void create(map<string, string> &dataQueue);
		virtual void load(void);
		virtual void update(void);
		virtual void destroy(void) = 0;
		
		void setCreated(bool available);
		
		bool isCreated(void);
		bool isFinished(void);
		bool isLoading(void);
		unsigned int getResourceCount(void);
	};
};

