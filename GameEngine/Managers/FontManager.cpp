#include "stdafx.h"
#include "FontManager.h"

namespace CypherEngine
{
	FontManager::FontManager(void) : Manager()
	{

	}

	FontManager::~FontManager(void)
	{

	}

	void FontManager::destroy(void)
	{
		if (!loading)
		{
			loadingMutex.lock();
			fonts.clear();
			loadingMutex.unlock();
		}
	}

	void FontManager::resourceLoad(void)
	{
		auto system = System::getInstance();

		loadingMutex.lock();

		for (resourceIter = resourceQueue.begin(); resourceIter != resourceQueue.end(); )
		{
			System::writeDebugString("Font: " + (*resourceIter).first + " is loading.");

			auto filepath = system->getFileStructure().fonts + "\\" + (*resourceIter).second;
			sf::Font tmpFont;
			tmpFont.loadFromFile(filepath);
			fonts.insert(pair<string, sf::Font>((*resourceIter).first, tmpFont));

			System::writeDebugString("Font: " + (*resourceIter).first + " has loaded.");

			resourceIter = resourceQueue.erase(resourceIter);
		}

		loading = false;
		resourceCount = fonts.size();

		loadingMutex.unlock();
	}

	bool FontManager::hasFont(const string &id)
	{
		loadingMutex.lock();

		iter = fonts.find(id);
		bool result = (iter != fonts.end());

		loadingMutex.unlock();
		return result;
	}

	bool FontManager::hasFont(const int &id)
	{
		int count = 0;
		bool result = false;
		loadingMutex.lock();

		for (auto font : fonts)
		{
			if (count == id)
			{
				result = true;
				break;
			}
		}

		loadingMutex.unlock();
		return result;
	}

	sf::Font FontManager::getFont(const string &id)
	{
		iter = fonts.find(id);
		if (iter != fonts.end())
		{
			return (*iter).second;
		}
		else
		{
			return fonts.at(0);
		}
	}

	sf::Font FontManager::getFont(const int &id)
	{
		bool found = false;
		int count = 0;

		loadingMutex.lock();

		for (auto font : fonts)
		{
			if (count == id)
			{
				found = true;
				break;
			}
		}

		auto foundFont = &(*iter).second;

		loadingMutex.unlock();

		if (found)
		{
			return *foundFont;
		}
		else
		{
			return fonts.at(0);
		}
	}
}