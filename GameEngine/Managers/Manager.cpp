#include "stdafx.h"
#include "Manager.h"

namespace CypherEngine
{
	Manager::Manager(void)
	{
		created = false;
		loading = false;
		finished = false;
	}

	Manager::~Manager(void)
	{
		if (workerThread.joinable())
		{
			workerThread.join();
		}
	}

	void Manager::create(map<string, string> &dataQueue)
	{
		resourceQueue = dataQueue;
		created = true;
	}

	void Manager::load(void)
	{
		if (!loading)
		{
			loading = true;
			finished = false;
			workerThread = thread(&Manager::resourceLoad, this);
		}
	}

	void Manager::update(void)
	{
		if (!loading)
		{
			if (workerThread.joinable())
			{
				workerThread.join();
				finished = true;
			}
		}
	}

	void Manager::setCreated(bool available)
	{
		created = available;
	}

	bool Manager::isCreated(void)
	{
		return created;
	}

	bool Manager::isFinished(void)
	{
		return finished;
	}

	bool Manager::isLoading(void)
	{
		return loading;
	}

	unsigned int Manager::getResourceCount(void)
	{
		return resourceCount;
	}
}