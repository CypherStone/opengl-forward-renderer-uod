#pragma once
#include "Manager.h"

namespace CypherEngine
{

	class FontManager : public Manager
	{
	private:
		map<string, sf::Font> fonts;
		map<string, sf::Font>::iterator iter;
		void resourceLoad(void);

	public:
		FontManager(void);
		~FontManager(void);

		void destroy(void);
	
		bool hasFont(const string &id);
		bool hasFont(const int &id);

		int getFontCount(void);

		sf::Font getFont(const string &id);
		sf::Font getFont(const int &id);
	};

};

