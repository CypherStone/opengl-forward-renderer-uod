#include "stdafx.h"
#include "ShaderManager.h"

namespace CypherEngine
{
	/* Constructor Functions*/
	ShaderManager * ShaderManager::m_instance = nullptr;

	ShaderManager::ShaderManager(void)
	{

	}

	void ShaderManager::destroy(void)
	{
		if (m_instance)
		{
			m_instance->m_shaders.clear();

			delete m_instance;
			m_instance = nullptr;
		}

		Shader::deactivate();
	}

	ShaderManager * ShaderManager::getInstance(void)
	{
		if (!m_instance)
		{
			m_instance = new ShaderManager;
		}

		return m_instance;
	}

	/* Public Functions */

	bool ShaderManager::create(const string & filepath)
	{
		m_filepath = filepath;

		if (!parseShaderFiles(filepath))
		{
			return false;
		}

		return true;
	}

	shared_ptr<Shader> ShaderManager::activateShader(const string & name)
	{
		for (auto & shader : m_shaders)
		{
			if (shader->getName() == name)
			{
				m_currentShader = shader;
				m_currentShader->setActive();
				return m_currentShader;
			}
		}

		return shared_ptr<Shader>(nullptr);
	}

	void ShaderManager::deactivateShader(void)
	{
		Shader::deactivate();
	}

	/* Get Functions */

	shared_ptr<Shader> ShaderManager::getShader(const string & name)
	{
		for (auto & shader : m_shaders)
		{
			if (shader->getName() == name)
			{
				return shader;
			}
		}

		return shared_ptr<Shader>(nullptr);
	}

	shared_ptr<Shader> ShaderManager::getActiveShader(void)
	{
		return m_currentShader;
	}

	/* Set Functions */

	void ShaderManager::setShaderVariables(shared_ptr<Shader> shader, tinyxml2::XMLElement * elements, bool uniform)
	{
		if (elements)
		{
			string variableName;
			for (auto element = elements->FirstChildElement(); element != nullptr; element = element->NextSiblingElement())
			{
				variableName = element->GetText();
				if (!variableName.empty())
				{
					if (uniform)
					{
						shader->setUniformLocation(variableName);
					}
					else
					{
						shader->setAttributeLocation(variableName);
					}
				}
			}
		}
	}

	/* Private Functions */

	bool ShaderManager::parseShaderFiles(const string & filepath)
	{
		using namespace tinyxml2;
		auto system = System::getInstance();
		tinyxml2::XMLDocument xmlFile;

		if (xmlFile.LoadFile(filepath.c_str()) != tinyxml2::XML_NO_ERROR)
		{
			return false;
		}

		auto shaders = xmlFile.FirstChild()->FirstChildElement("Shaders");
		if (!shaders)
		{
			return false;
		}

		for (auto shader = shaders->FirstChildElement(); shader != nullptr; shader = shader->NextSiblingElement())
		{
			string name, vertexpath, fragmentpath, computepath;
			shared_ptr<Shader> newShader;

			name = shader->Attribute("name");
			if (name.empty())
			{
				return false;
			}

			if (shader->FirstChildElement("ComputeShader"))
			{
				computepath = shader->FirstChildElement("ComputeShader")->GetText();
				if (computepath.empty())
				{
					return false;
				}

				computepath = system->getFileStructure().shaders + "\\" + computepath;

				newShader = make_shared<ComputeShader>(name, computepath);
			}
			else
			{
				vertexpath = shader->FirstChildElement("VertexShader")->GetText();
				if (vertexpath.empty())
				{
					return false;
				}

				fragmentpath = shader->FirstChildElement("FragmentShader")->GetText();
				if (fragmentpath.empty())
				{
					return false;
				}

				vertexpath = system->getFileStructure().shaders + "\\" + vertexpath;
				fragmentpath = system->getFileStructure().shaders + "\\" + fragmentpath;

				newShader = make_shared<DrawShader>(name, vertexpath, fragmentpath);
			}

			if (!newShader->isCreated())
			{
				system->showErrorMessageBox("Error - Shader", "Failed to create the shader: " + name);
				return false;
			}

			setShaderVariables(newShader, shader->FirstChildElement("Uniforms"), true);
			setShaderVariables(newShader, shader->FirstChildElement("Attributes"), false);

			m_shaders.push_back(newShader);
		}

		return true;
	}

	void ShaderManager::reloadShaders(void)
	{
		m_instance->m_shaders.clear();

		m_instance->m_currentShader.reset();

		while (!m_instance->create(m_instance->m_filepath));
	}

}