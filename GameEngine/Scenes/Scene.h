#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class Scene
	{
	public:
		enum SceneState
		{
			Unintialised,
			Created,
			Loading,
			Game,
			LoadNextScene,
			LoadPreloadScene,
			Destroyed
		};

	private:
		RenderEngine * m_renderEngine;
		ResourceManager m_resources;
		vector<Script*> m_scripts;
		vector<GameObject *> m_visibleObjects;
		vector<GameObject *> m_attachedObjects;
		vector<std::pair<string, sf::Text *>> m_attachedTexts;
		vector<std::pair<string, sf::Font *>> m_attachedFonts;
		LightManager m_lights;
		string m_name;
		string m_resourceFile;
		SceneState m_state;
		Camera m_camera;
		int m_order;
		bool m_active;
		bool m_loading;
		bool m_sceneLoaded;
		bool m_fontsLoaded;

	public:
		
		/* Constructor Functions */
		Scene(string name, string resourceFile, int order);
		~Scene(void);

		/* Public Functions */

		bool create(vector<Script *> & scripts);
		void loadScene(void);

		void updateGame(float delta);
		void updateLoading(float delta);

		void drawGame(void);
		void drawGUI(void);
		void drawLoading(void);
		void reset(void);
		void destroy(void);

		/* Set Functions */

		void setActive(bool active);
		void setCamera(Camera & camera);
		void attachLight(shared_ptr<Light> & light);
		void attachModel(GameObject * object);
		void attachText(const string & name, sf::Text * text);
		void attachFont(const string & name, sf::Font * font);
		void dettachLight(shared_ptr<Light> & light);
		void dettachModel(GameObject * object);

		/* Get Functions */

		Camera getCamera(void);
		string getName(void);
		SceneState getState(void);
		int getOrder(void);
		bool isLoading(void);
		bool isActive(void);
	};
};