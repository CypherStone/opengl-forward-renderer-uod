#include "stdafx.h"
#include "Scene.h"

namespace CypherEngine
{
	/* Constructor Functions*/

	Scene::Scene(string name, string resourceFile, int order)
	{
		m_name = name;
		m_resourceFile = resourceFile;
		m_state = SceneState::Unintialised;
		m_order = order;
		m_active = false;
		m_loading = false;
		m_sceneLoaded = false;
		m_fontsLoaded = false;
	}

	Scene::~Scene(void)
	{
		destroy();
	}

	/* Public Functions*/
	bool Scene::create(vector<Script *> & scripts)
	{
		System::writeDebugString("Scene: " + m_name + " has been created.");

		m_renderEngine = RenderEngine::getInstance();
		if (!m_resources.create(m_resourceFile))
		{
			return false;
		}

		m_scripts = scripts;

		for (auto & script : m_scripts)
		{
			script->create();
		}

		m_lights.create(0, 0);

		m_state = SceneState::Created;

		return true;
	}

	void Scene::loadScene(void)
	{
		if (!m_sceneLoaded)
		{
			System::writeDebugString("Scene: " + m_name + " is loading.");
			m_loading = true;
			m_resources.load();
			m_state = SceneState::Loading;
		}
	}

	void Scene::updateGame(float delta)
	{
		for (auto & script : m_scripts)
		{
			script->update(delta);
			if (System::getInstance()->hasFocus())
			{
				script->updateInput(delta);
			}
			script->updateGUI(delta);
		}
	}

	void Scene::updateLoading(float delta)
	{
		if (!m_sceneLoaded)
		{
			m_resources.update();

			if (!m_resources.isGameObjectsLoading())
			{
				for (auto & object : m_attachedObjects)
				{
					auto name = object->getName();
					auto tag = object->getTag();
					*object = m_resources.getGameObject(name, false);
					object->create();
					object->setTag(tag);
					m_visibleObjects.push_back(object);
				}
			}

			if (!m_fontsLoaded && !m_resources.isFontsLoading())
			{
				for (auto text : m_attachedTexts)
				{
					text.second->setFont(m_resources.getFontManager().getFont(text.first));
				}

				for (auto & font : m_attachedFonts)
				{
					*font.second = m_resources.getFontManager().getFont(font.first);
				}

				for (auto & script : m_scripts)
				{
					script->loadedGUI();
				}

				m_fontsLoaded = true;
			}
			else if (m_fontsLoaded)
			{
				for (auto & script : m_scripts)
				{
					script->updateGUI(delta);
				}
			}

			if (!m_resources.isLoading())
			{
				for (auto & script : m_scripts)
				{
					script->loaded();
				}

				System::writeDebugString("Scene: " + m_name + " has loaded.");
				m_sceneLoaded = true;
				m_loading = false;
				m_state = Game;
			}
		}
	}

	void Scene::drawGame(void)
	{
		m_renderEngine->draw(m_camera, m_lights, m_visibleObjects);
	}

	void Scene::drawGUI(void)
	{
		if (!m_resources.isFontsLoading())
		{
			for (auto & script : m_scripts)
			{
				script->drawGUI();
			}
		}
	}

	void Scene::drawLoading(void)
	{
		for (auto & script : m_scripts)
		{
			script->drawGUI();
		}
	}

	void Scene::reset(void)
	{
		for (auto & script : m_scripts)
		{
			script->reset();
		}
	}

	void Scene::destroy(void)
	{
		m_resources.destroy();

		for (auto & script : m_scripts)
		{
			script->destroy();
		}

		m_scripts.clear();
		
		System::getInstance()->dettachGUIs();
	}

	/* Set Functions*/

	void Scene::setActive(bool active)
	{
		m_active = active;
	}

	void Scene::setCamera(Camera & camera)
	{
		m_camera = camera;
	}

	void Scene::attachLight(shared_ptr<Light> & light)
	{
		m_lights.attachLight(light);
	}

	void Scene::attachModel(GameObject * object)
	{
		m_attachedObjects.push_back(object);
	}

	void Scene::attachText(const string & name, sf::Text * text)
	{
		m_attachedTexts.push_back(std::pair<string, sf::Text *>(name, text));
	}

	void Scene::attachFont(const string & name, sf::Font * font)
	{
		m_attachedFonts.push_back(std::pair<string, sf::Font *>(name, font));
	}

	void Scene::dettachLight(shared_ptr<Light> & light)
	{
		m_lights.dettachLight(light);
	}

	void Scene::dettachModel(GameObject * object)
	{

	}

	/* Get Functions*/

	Camera Scene::getCamera(void)
	{
		return m_camera;
	}

	string Scene::getName(void)
	{
		return m_name;
	}

	Scene::SceneState Scene::getState(void)
	{
		return m_state;
	}

	int Scene::getOrder(void)
	{
		return m_order;
	}

	bool Scene::isLoading(void)
	{
		return m_loading;
	}

	bool Scene::isActive(void)
	{
		return m_active;
	}

	/* Private Functions*/
}