#include "stdafx.h"
#include "SceneManager.h"

namespace CypherEngine
{
	SceneManager * SceneManager::m_instance = nullptr;

	/* Constructor Functions */

	SceneManager::SceneManager(void)
	{

	}

	SceneManager::~SceneManager(void)
	{

	}

	SceneManager * SceneManager::getInstance(void)
	{
		if (m_instance == nullptr)
		{
			m_instance = new SceneManager();
		}

		return m_instance;
	}

	void SceneManager::destroy(void)
	{
		for (auto & scene : m_instance->m_scenes)
		{
			if (scene)
			{
				scene->destroy();
			}
		}

		if (m_instance->m_scriptManager)
		{
			m_instance->m_scriptManager->destroy();
			m_instance->m_scriptManager = nullptr;
		}

		if (m_instance != nullptr)
		{
			delete m_instance;
			m_instance = nullptr;
		}
	}

	/* Public Functions */

	bool SceneManager::create(string filepath)
	{
		m_scriptManager = ScriptManager::getInstance();
		ScriptStorage();

		if (!parseSceneFile(filepath + "Scenes.xml"))
		{
			return false;
		}

		int current = INT_MAX;
		for (auto & scene : m_scenes)
		{
			if (current > scene->getOrder())
			{
				current = scene->getOrder();
				m_currentScene = scene;
			}
		}

		if (!m_currentScene->create(m_scriptManager->getScriptsForScene(m_currentScene->getName())))
		{
			System::getInstance()->showErrorMessageBox("Error - Scene Manager", "First scene failed to create.");
			return false;
		}

		return true;
	}

	void SceneManager::update(float delta)
	{
		if (m_currentScene->getState() == Scene::SceneState::Created)
		{
			m_currentScene->loadScene();
		}

		if (m_currentScene->getState() == Scene::SceneState::Game ||
			m_currentScene->getState() == Scene::SceneState::Loading)
		{
			if (m_currentScene->isLoading())
			{
				m_currentScene->updateLoading(delta);
			}
			else
			{
				m_currentScene->updateGame(delta);
			}
		}

		if (m_currentScene->getState() == Scene::SceneState::Destroyed ||
			m_currentScene->getState() == Scene::SceneState::LoadNextScene ||
			m_currentScene->getState() == Scene::SceneState::LoadPreloadScene)
		{
			if (m_currentScene->getState() == Scene::SceneState::Unintialised)
			{
				// Work out next scene

				m_currentScene->create(m_scriptManager->getScriptsForScene(m_currentScene->getName()));
				m_currentScene->loadScene();
			}

			if (m_currentScene->getState() == Scene::SceneState::Destroyed &&
				!m_currentScene->isLoading())
			{
				m_currentScene->destroy();
				m_currentScene = m_preloadScene;
			}
		}
	}

	void SceneManager::draw(void)
	{
		if (m_currentScene->getState() == Scene::SceneState::Game ||
			m_currentScene->getState() == Scene::SceneState::Loading)
		{
			if (m_currentScene->isLoading())
			{
				m_currentScene->drawLoading();
			}
			else
			{
				m_currentScene->drawGame();
				m_currentScene->drawGUI();
			}
		}
	}

	/* Get Functions */

	Camera SceneManager::getCameraFromScene(void)
	{
		auto currentScnene = m_instance->m_currentScene;
		if (m_instance && currentScnene)
		{
			return currentScnene->getCamera();
		}

		return Camera();
	}

	bool SceneManager::exists(void)
	{
		return (m_instance != nullptr);
	}

	/* Set Functions */

	void SceneManager::setCameraToScene(Camera & camera)
	{
		auto currentScnene = m_instance->m_currentScene;
		if (m_instance && currentScnene)
		{
			currentScnene->setCamera(camera);
		}
	}

	shared_ptr<GameObject> SceneManager::attachModelToScene(const string & name, const string & tag)
	{
		auto currentScnene = m_instance->m_currentScene;
		if (m_instance && currentScnene)
		{
			auto obj = make_shared<GameObject>(name, tag);
			currentScnene->attachModel(obj.get());
			return obj;
		}

		return shared_ptr<GameObject>(nullptr);
	}

	shared_ptr<Light> SceneManager::attachLightToScene(LightType type)
	{
		auto currentScnene = m_instance->m_currentScene;
		if (m_instance && currentScnene)
		{
			shared_ptr<Light> light;

			switch (type)
			{
			case Directional:
				light = make_shared<DirectionalLight>();
				break;
			case Spot:
				light = make_shared<SpotLight>();
				break;
			case Point:
			default:
				light = make_shared<PointLight>();
				break;
			}

			currentScnene->attachLight(light);

			return light;
		}

		return shared_ptr<Light>(nullptr);
	}

	shared_ptr<sf::Text> SceneManager::attachTextToScene(const string & fontName)
	{
		auto currentScnene = m_instance->m_currentScene;
		if (m_instance && currentScnene)
		{
			auto text = new sf::Text();
			currentScnene->attachText(fontName, text);
			return shared_ptr<sf::Text>(text);
		}

		return shared_ptr<sf::Text>(nullptr);
	}


	shared_ptr<sf::Font> SceneManager::attachFontToScene(const string & fontName)
	{
		auto currentScnene = m_instance->m_currentScene;
		if (m_instance && currentScnene)
		{
			auto font = new sf::Font();
			currentScnene->attachFont(fontName, font);
			return shared_ptr<sf::Font>(font);
		}

		return shared_ptr<sf::Font>(nullptr);
	}

	shared_ptr<tgui::Gui> SceneManager::attachGUI(void)
	{
		auto currentScnene = m_instance->m_currentScene;
		if (m_instance && currentScnene)
		{
			auto gui = make_shared<tgui::Gui>(System::getInstance()->getWindow());
			System::getInstance()->attachGUI(gui);
			return shared_ptr<tgui::Gui>(gui);
		}

		return shared_ptr<tgui::Gui>(nullptr);
	}

	/* Private Functions */

	bool SceneManager::parseSceneFile(string filepath)
	{
		auto system = System::getInstance();

		tinyxml2::XMLDocument xmlFile;
		if (xmlFile.LoadFile(filepath.c_str()) != tinyxml2::XML_NO_ERROR)
		{
			return false;
		}

		auto scenes = xmlFile.FirstChild()->FirstChildElement("Scenes");
		if (!scenes)
		{
			return false;
		}

		string name;
		string resource;
		string preload;
		int order = -1;
		int sceneCount = 0;

		for (auto scene = scenes->FirstChildElement("Scene");; scene = scene->NextSiblingElement("Scene"))
		{
			if (!scene)
			{
				if (sceneCount == 0)
				{
					system->showErrorMessageBox("Error - Scene Manager", "No scenes found.");
					return false;
				}
				else
				{
					return true;
				}
			}
			

			name = scene->GetText();
			if (name.empty())
			{
				system->showErrorMessageBox("Error - Scene Manager", "Scene found with no name.");
				return false;
			}
			else
			{
				for (auto sceneName : m_scenes)
				{
					if (sceneName->getName() == name)
					{
						system->showErrorMessageBox("Error - Scene Manager", "Scene found with an existing scene name.\nName: " + name);
						return false;
					}
				}
			}

			resource = scene->Attribute("resource");
			if (resource.empty())
			{
				return false;
			}
			resource = system->getFileStructure().scenes + "\\" + resource;

			order = scene->IntAttribute("order");
			if (order < 1)
			{
				return false;
			}

			preload = scene->Attribute("preloadScene");

			m_scenes.push_back(make_shared<Scene>(name, resource, order));
			sceneCount++;
		}

		return true;
	}
}