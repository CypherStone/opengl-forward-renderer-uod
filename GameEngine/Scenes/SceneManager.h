#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class SceneManager
	{
	public:

		enum LightType
		{
			Directional,
			Point,
			Spot	
		};

	private:
		static SceneManager * m_instance;
		ScriptManager * m_scriptManager;
		vector<shared_ptr<Scene>> m_scenes;
		vector<string> m_sceneFiles;
		shared_ptr<Scene> m_currentScene;
		shared_ptr<Scene> m_preloadScene;
		shared_ptr<Scene> m_loadScene;

		SceneManager(void);
		~SceneManager(void);

		bool parseSceneFile(string filepath);

	public:

		/* Constructor Functions */
		static SceneManager * getInstance(void);
		static void destroy(void);

		/* Public Functions */
		bool create(string filepath);
		void update(float delta);
		void draw(void);
		
		/* Get Functions */

		static Camera getCameraFromScene(void);
		bool exists(void);

		/* Set Functions */

		static void setCameraToScene(Camera & camera);
		static shared_ptr<Light> attachLightToScene(LightType type);
		static shared_ptr<GameObject> attachModelToScene(const string & name, const string & tag);
		static shared_ptr<sf::Text> attachTextToScene(const string & fontName);
		static shared_ptr<sf::Font> attachFontToScene(const string & fontName);
		static shared_ptr<tgui::Gui> attachGUI(void);
	};
}
