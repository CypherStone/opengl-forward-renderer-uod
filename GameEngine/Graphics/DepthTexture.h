#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class DepthTexture
	{
	private:
		GLuint m_id;
		GLuint m_fbo;
		GLuint m_rbo;
		vec2 m_size;

	public:
		DepthTexture(void);

		bool create(int width, int height);
		void begin(void);
		void end(void);
		void destroy(void);

		void setTextureID(GLint id);

		GLuint getTexture(void);
		GLuint getTextureID(void);
	};
};