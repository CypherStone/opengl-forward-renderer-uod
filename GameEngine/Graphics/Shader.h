#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class Shader
	{
	protected:
		unordered_map<string, GLint>::iterator m_row;
		unordered_map<string, GLint> m_attributeLocations, m_uniformLocations;
		string m_name;
		GLuint m_program;
		bool m_created;
		
		void initialise(void);
		bool linkShader(void); 
		bool compileShader(GLuint& shader, const string& data, GLenum type);
		string compileFromFile(const string &shaderFile);
		bool setGenericLocations(unordered_map<string, GLint> &locations, const string &parameter, bool attribute);
		GLint getGenericLocations(unordered_map<string, GLint> &locations, const string &parameter, bool attribute);

	public:
		Shader(const string & name);
		~Shader(void);

		virtual void destroy(void);

		void bindAttributeLocation(int location, const string & attribute);\
		void bindUniformMatrix4(const unsigned int id, glm::mat4 & matrix, bool transpose = false, int count = 1);
		void bindUniformMatrix4(const string &uniform, glm::mat4 & matrix, bool transpose = false, int count = 1);
		void bindUniformMatrix3(const string &uniform, glm::mat3 & matrix, bool transpose = false, int count = 1);
		void bindUniformVector4(const string &uniform, glm::vec4 & vector, int count = 1);
		void bindUniformVector3(const string &uniform, glm::vec3 & vector, int count = 1);
		void bindUniformVector2(const string &uniform, glm::vec2 & vector, int count = 1);
		void bindUniformFloat(const string &uniform, float data);
		void bindUniformInteger(const string &uniform, int data);
		void bindUniformUInteger(const string &uniform, unsigned int data);
		void bindUniformBlock(const string &uniform, GLuint binding);
		void bindTexture(const string & uniform, int position, GLuint texture, GLenum type = GL_TEXTURE_2D);
		void bindTexture(GLint uniform, int position, GLuint texture, GLenum type = GL_TEXTURE_2D);

		string getName(void);
		GLint getUniformLocation(const string &uniform);
		GLint getAttributeLocation(const string &attribute);
		const GLuint &getProgram(void) const;
		bool isCreated(void);

		void setActive(void);

		bool setUniformLocation(const string &uniform);
		bool setAttributeLocation(const string &attrbute);

		static void deactivate(void);

	};
};