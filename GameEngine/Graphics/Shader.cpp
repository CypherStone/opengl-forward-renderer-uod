#include "stdafx.h"

namespace CypherEngine
{
	Shader::Shader(const string & name)
	{
		m_name = name;
		m_program = 0;
	}

	Shader::~Shader(void)
	{
		destroy();
	}

	void Shader::initialise(void)
	{
		destroy();

		m_created = true;
		m_program = glCreateProgram();
	}

	void Shader::destroy(void)
	{
		glDeleteProgram(m_program);
		m_program = 0;
		m_uniformLocations.clear();
		m_attributeLocations.clear();
		m_created = false;
	}

	bool Shader::setUniformLocation(const string &uniform)
	{
		return setGenericLocations(m_uniformLocations, uniform, false);
	}

	void Shader::bindAttributeLocation(int location, const string &attribute)
	{
		glBindAttribLocation(m_program, location, attribute.c_str());
	}

	void Shader::bindUniformMatrix4(const string &uniform, glm::mat4 &matrix, bool transpose, int count)
	{
		glProgramUniformMatrix4fv(m_program, getUniformLocation(uniform), count, transpose, &matrix[0][0]);
	}

	void Shader::bindUniformMatrix4(const unsigned int id, glm::mat4 & matrix, bool transpose /*= false*/, int count /*= 1*/)
	{
		glProgramUniformMatrix4fv(m_program, id, count, transpose, &matrix[0][0]);
	}

	void Shader::bindUniformMatrix3(const string &uniform, glm::mat3 & matrix, bool transpose, int count)
	{
		glProgramUniformMatrix3fv(m_program, getUniformLocation(uniform), count, transpose, &matrix[0][0]);
	}

	void Shader::bindUniformVector4(const string &uniform, glm::vec4 & vector, int count)
	{
		glProgramUniform4fv(m_program, getUniformLocation(uniform), count, &vector[0]);
	}

	void Shader::bindUniformVector3(const string &uniform, glm::vec3 & vector, int count)
	{
		glProgramUniform3fv(m_program, getUniformLocation(uniform), count, &vector[0]);
	}

	void Shader::bindUniformVector2(const string &uniform, glm::vec2 & vector, int count)
	{
		glProgramUniform2fv(m_program, getUniformLocation(uniform), count, &vector[0]);
	}

	void Shader::bindUniformFloat(const string &uniform, float data)
	{
		glProgramUniform1f(m_program, getUniformLocation(uniform), data);
	}

	void Shader::bindUniformInteger(const string &uniform, int data)
	{
		glProgramUniform1i(m_program, getUniformLocation(uniform), data);
	}

	void Shader::bindUniformUInteger(const string &uniform, unsigned int data)
	{
		glProgramUniform1ui(m_program, getUniformLocation(uniform), data);
	}

	void Shader::bindUniformBlock(const string &uniform, GLuint data)
	{
		GLint blockIndex = glGetUniformBlockIndex(m_program, uniform.c_str());
		glUniformBlockBinding(m_program, blockIndex, data);
	}

	void Shader::bindTexture(const string & uniform, int position, GLuint texture, GLenum type)
	{
		glActiveTexture(GL_TEXTURE0 + position);
		bindUniformInteger(uniform, position);
		glBindTexture(type, texture);
	}

	void Shader::bindTexture(GLint uniform, int position, GLuint texture, GLenum type)
	{
		glActiveTexture(GL_TEXTURE0 + position);
		glProgramUniform1i(m_program, uniform, position);
		glBindTexture(type, texture);
	}

	/* Get Functions */

	string Shader::getName(void)
	{
		return m_name;
	}

	GLint Shader::getAttributeLocation(const string &attribute)
	{
		return getGenericLocations(m_attributeLocations, attribute, true);
	}

	GLint Shader::getUniformLocation(const string &uniform)
	{
		return getGenericLocations(m_uniformLocations, uniform, false);
	}

	const GLuint &Shader::getProgram(void) const
	{
		return m_program;
	}

	bool Shader::isCreated(void)
	{
		return m_created;
	}

	GLint Shader::getGenericLocations(unordered_map<string, GLint> &locations, const string &parameter, bool attribute)
	{
		m_row = locations.find(parameter);
		if (m_row == locations.end())
		{
			setGenericLocations(locations, parameter, attribute);
			m_row = locations.find(parameter);
			if (m_row != locations.end())
			{
				return m_row->second;
			}
		}
		else
		{
			return m_row->second;
		}

		return -1;
	}

	bool Shader::setGenericLocations(unordered_map<string, GLint> &locations, const string &parameter, bool attribute)
	{
		m_row = locations.find(parameter);
		if (m_row == locations.end())
		{
			GLint location = -1;

			if (attribute)
			{
				location = glGetAttribLocation(m_program, parameter.c_str());
			}
			else
			{
				location = glGetUniformLocation(m_program, parameter.c_str());
			}

			if (location >= 0)
			{
				locations.insert(std::pair<string, GLint>(parameter, location));
				return true;
			}
			else
			{
#ifdef _DEBUG
				//System::getInstance()->showErrorMessageBox("Error - Shader Variable Location", "File: " + m_name + "\n\nCannot find: " + parameter);
#endif
			}
		}

		return false;
	}

	/* Set Functions */

	bool Shader::setAttributeLocation(const string &attribute)
	{
		return setGenericLocations(m_attributeLocations, attribute, true);
	}

	void Shader::setActive(void)
	{
		glUseProgram(m_program);
	}

	/* Private Functions */


	bool Shader::linkShader(void)
	{
		int status = 0;
		glLinkProgram(m_program);
		glGetProgramiv(m_program, GL_LINK_STATUS, &status);
		if (!status)
		{
			int length;
			char infoLog[1024];
			glGetProgramInfoLog(m_program, sizeof(infoLog), &length, infoLog);
			string title = "Error - Shader Program";
			System::getInstance()->showErrorMessageBox(title, string("File: " + m_name + "\n\n" + string(infoLog)));
			return false;
		}

		return true;
	}

	string Shader::compileFromFile(const string &shader)
	{
		std::ifstream shaderFile(shader);
		if (shaderFile)
		{
			string shaderData;
			string tmpLine;
			while (getline(shaderFile, tmpLine))
			{
				shaderData += tmpLine;
				shaderData += "\n";
			}
			shaderFile.close();

			return shaderData;
		}
		else
		{
			return "NO_FILE";
		}
	}

	bool Shader::compileShader(GLuint& shader, const string& data, GLenum type)
	{
		int status = 0;
		const char * tmpShaderData = data.c_str();
		shader = glCreateShader(type);

		glShaderSource(shader, 1, &tmpShaderData, nullptr);
		glCompileShader(shader);

		glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
		if (!status)
		{
			int length;
			char infoLog[1024];
			glGetShaderInfoLog(shader, sizeof(infoLog), &length, infoLog);

			System::getInstance()->showErrorMessageBox("Error - Shader Compile", "File: " + m_name + "\n\n" + string(infoLog));
			return false;
		}

		glAttachShader(m_program, shader);

		return true;
	}

	void Shader::deactivate(void)
	{
		glUseProgram(0);
	}

}