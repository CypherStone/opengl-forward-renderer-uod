#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class ComputeShader : public Shader
	{
	private:
		GLuint m_computeShader;

		bool loadFromMemory(const string& computeShader);
		bool compileFromMemory(const string &computeShader);

	public:
		ComputeShader(const string & name, const string & computeShader);
		~ComputeShader(void);
		void destroy(void);

		bool loadFromFile(const string& computeShader);

		void dispatch(int numX, int numY, int numZ);
	};
};