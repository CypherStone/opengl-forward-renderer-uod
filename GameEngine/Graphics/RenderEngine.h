#pragma once
#include "stdafx.h"
#include "Managers/ShaderManager.h"
#include "Utilities/PerformanceMeasurement.h"
#include "System.h"

namespace CypherEngine
{
	class RenderEngine
	{
	public:
		enum DrawMethod
		{
			Deferred,
			FPlus
		};

	private:
		static RenderEngine * m_instance;
		ShaderManager * m_shaderManager;
		System * m_system;
		GBuffer m_gbuffer;
		DepthTexture m_depthTexture;
		ForwardPlus m_forwardPlus;
		DrawMethod m_method;

		RenderEngine(void);
		~RenderEngine(void);

		/* Deferred Rendering */
		void drawShadows(LightManager & lights, vector<GameObject *> & objects);
		void drawObjects(Camera & camera, vector<GameObject *> & objects);
		void drawLights(Camera & camera, LightManager & lights);
		void drawToBuffer(void);

		/* Forward+ Rendering */
		void drawSceneDepth(Camera & camera, vector<GameObject *> & objects);
		void drawForwardPlus(Camera & camera, LightManager & lights, vector<GameObject *> & objects);

	public:
		/* Constructor Functions */
		static RenderEngine * getInstance(void);
		static void destroy(void);

		bool create(const unsigned short &width, const unsigned short &height);

		void draw(Camera & camera, LightManager & lights, vector<GameObject *> & objects);
	};
};