﻿#pragma once
#include "stdafx.h"
#include "GBuffer.h"

namespace CypherEngine
{

	GBuffer::GBuffer(void)
	{

	}

	void GBuffer::destroy(void)
	{
		for (unsigned i = 0; i < m_textures.size(); i++)
		{
			glDeleteTextures(1, &m_textures[i]);
		}

		glDeleteFramebuffers(1, &m_fbo);

		glDeleteRenderbuffers(1, &m_depthBuffer);
		glDeleteVertexArrays(1, &m_vertexArrayId);
		glDeleteBuffers(1, &m_vertexArrayId);
		glDeleteBuffers(1, &m_indexBufferId);
	}

	bool GBuffer::create(const unsigned short width, const unsigned short height, shared_ptr<Shader> gbufferShader, shared_ptr<Shader> quadShader)
	{
		m_gbufferShader = dynamic_cast<DrawShader*>(gbufferShader.get());
		if (!m_gbufferShader)
		{
			return false;
		}

		m_quadShader = dynamic_cast<DrawShader*>(quadShader.get());
		if (!m_quadShader)
		{
			return false;
		}

		m_width = width;
		m_height = height;
		m_projection = glm::ortho(0.0f, (float)width, 0.0f, (float)height);
		

		m_textures.resize(GBufferTextures::TotalTargets);
		m_bufferID.resize(GBufferTextures::DrawTargetCount);

		glGenFramebuffers(1, &m_fbo);
		glGenTextures(GBufferTextures::TotalTargets, &m_textures[0]);
		glGenTextures(1, &m_depthBuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

		GLint formats[GBufferTextures::TotalTargets] = { GL_RGBA16F, GL_RGBA8, GL_RGBA16F, GL_RGBA8, GL_RGBA8 };

		for (int i = 0; i < GBufferTextures::TotalTargets; i++)
		{

			m_bufferTypes[i] = GL_COLOR_ATTACHMENT0 + i;
			glBindTexture(GL_TEXTURE_2D, m_textures[i]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexImage2D(GL_TEXTURE_2D, 0, formats[i], width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
			glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, m_bufferTypes[i], GL_TEXTURE_2D, m_textures[i], 0);
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		glBindTexture(GL_TEXTURE_2D, m_depthBuffer);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthBuffer, 0);

		GLenum status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE)
		{
			System::getInstance()->showErrorMessageBox("Error - GBuffer", "Can't created an FBO.");
			return false;
		}

		m_bufferID[GBufferTextures::PositionTarget] = m_gbufferShader->getUniformLocation("positionTarget");
		m_bufferID[GBufferTextures::DiffuseTarget] = m_gbufferShader->getUniformLocation("diffuseTarget");
		m_bufferID[GBufferTextures::NormalsTarget] = m_gbufferShader->getUniformLocation("normalsTarget");
		m_bufferID[GBufferTextures::SpecularTarget] = m_gbufferShader->getUniformLocation("specularTarget");

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glClearDepth(1.0f);
		glCullFace(GL_BACK);

		glViewport(0, 0, width, height);

		createVertexBuffer();

		return true;
	}

	void GBuffer::createVertexBuffer(void)
	{
		Vertex2D tmp;

		tmp.position = glm::vec3(0.0f, 0.0f, 0.0f);
		tmp.texel = glm::vec2(0.0f, 0.0f);
		m_vertices.push_back(tmp);

		tmp.position = glm::vec3((float)m_width, 0.0f, 0.0f);
		tmp.texel = glm::vec2(1.0f, 0.0f);
		m_vertices.push_back(tmp);

		tmp.position = glm::vec3((float)m_width, (float)m_height, 0.0f);
		tmp.texel = glm::vec2(1.0f, 1.0f);
		m_vertices.push_back(tmp);

		tmp.position = glm::vec3(0.0f, (float)m_height, 0.0f);
		tmp.texel = glm::vec2(0.0f, 1.0f);
		m_vertices.push_back(tmp);
		m_vertexCount = m_vertices.size();

		vector<int> indicies;
		for (int i = 0; i < m_vertexCount; i++)
		{
			indicies.push_back(i);
		}

		glGenVertexArrays(1, &m_vertexArrayId);
		glGenBuffers(1, &m_vertexBufferId);
		glGenBuffers(1, &m_indexBufferId);

		glBindVertexArray(m_vertexArrayId);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex2D) * m_vertexCount, &m_vertices[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex2D), BUFFER_OFFSET(0));
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2D), BUFFER_OFFSET(12));
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_vertexCount, &indicies[0], GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);	
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		m_vertices.clear();
	}

	void GBuffer::beginFrame(const sf::Color &color)
	{
		//Bind the GBuffer FBO and clear the colour.
		//Also prepare the colour of the clear.
		glClearColor(color.r, color.g, color.b, color.a);
		glDisable(GL_BLEND);
	}

	void GBuffer::drawGeometry(void)
	{
		//Prepare to draw the geometry by bind the necessary buffers.
		//Clear the binded buffers.		
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fbo);
		glDrawBuffers(GBufferTextures::DrawTargetCount, &m_bufferTypes[0]);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void GBuffer::drawWindowQuad(void)
	{
		glBindVertexArray(m_vertexArrayId);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glDrawElements(GL_QUADS, m_vertexCount, GL_UNSIGNED_INT, BUFFER_OFFSET(0));
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindVertexArray(0);
	}

	void GBuffer::drawLight(void)
	{		
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		unsigned int count = 0;

		//Bind the final render target for the lighting to draw onto.
		//glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fBO);
		glDrawBuffer(GL_COLOR_ATTACHMENT0 + GBufferTextures::FinalTarget);
		glClear(GL_COLOR_BUFFER_BIT);

		//Bind the previous render targets to the fragment shader.
		m_gbufferShader->bindUniformMatrix4("project", m_projection);
		for (count = 0; count < GBufferTextures::DrawTargetCount; count++)
		{
			glActiveTexture(GL_TEXTURE0 + count);
			glUniform1i(m_bufferID[count], count);
			glBindTexture(GL_TEXTURE_2D, m_textures[count]);
		}

		//Render the full window quad.
		//Possible optimisation for point lights is to use a stencil buffer and only 
		//render to the radius of the point light, thus saving calculations on unnecessary pixels.
		drawWindowQuad();

		for (count = 0; count < GBufferTextures::DrawTargetCount; count++)
		{
			glActiveTexture((GL_TEXTURE0 + GBufferTextures::DrawTargetCount - 1) - count);
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		//Unbind the textures by disabling textures, and set glActiveTexture(GL_TEXTURE0) to fix SFML from throwing errors.
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	}

	void GBuffer::drawToMainBuffer(GBufferTextures bufferTexture)
	{
		//Unbind the draw buffer and set the FBO to read-only.
		//Then blit the current buffer to the main buffer (SFML).
		//-- Drawing to a quad is faster than blitting.

		m_quadShader->bindUniformMatrix4("project", m_projection);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_textures[bufferTexture]);

		drawWindowQuad();

		//glBindFramebuffer(GL_READ_FRAMEBUFFER, fBO);
		//glReadBuffer(GL_TEXTURE0 + bufferTexture);
		//glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	}

	void GBuffer::endFrame(void)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glEnable(GL_BLEND);
	}
}
