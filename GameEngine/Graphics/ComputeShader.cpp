#include "stdafx.h"

namespace CypherEngine
{
	ComputeShader::ComputeShader(const string & name, const string & computerShader) : Shader(name)
	{
		m_computeShader = 0;
		m_created = loadFromFile(computerShader);
	}

	ComputeShader::~ComputeShader(void)
	{
		destroy();
	}

	void ComputeShader::destroy(void)
	{
		if (m_computeShader > 0)
		{
			glDetachShader(m_program, m_computeShader);
			glDeleteShader(m_computeShader);
		}

		Shader::destroy();
		m_computeShader = 0;
	}

	bool ComputeShader::loadFromFile(const string &computeShader)
	{
		initialise();
		string shader = compileFromFile(computeShader);

		if (shader == "NO_FILE")
		{
			return false;
		}

		if (!compileFromMemory(shader))
		{
			return false;
		}

		return true;
	}


	/* Private Functions */

	bool ComputeShader::compileFromMemory(const string &computeShader)
	{
		if (!computeShader.empty())
		{
			if (!compileShader(m_computeShader, computeShader, GL_COMPUTE_SHADER))
			{
				return false;
			}
		}

		bool linked = linkShader();

		if (m_computeShader > 0)
		{
			glDetachShader(m_program, m_computeShader);
			glDeleteShader(m_computeShader);
			m_computeShader = 0;
		}

		return linked;
	}

	void ComputeShader::dispatch(int numX, int numY, int numZ)
	{
		glDispatchCompute(numX, numY, numZ);
	}
}