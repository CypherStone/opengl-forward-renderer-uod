#pragma once
#include "..\stdafx.h"

namespace CypherEngine
{
	class GBuffer
	{

	private:
		DrawShader * m_gbufferShader;
		DrawShader * m_quadShader;
		vector<Vertex2D> m_vertices;
		vector<GLuint> m_bufferID;
		vector<GLuint> m_textures;
		glm::mat4 m_projection;
		GLenum m_bufferTypes[GBufferTextures::TotalTargets];
		GLuint m_fbo;
		GLuint	m_depthBuffer;
		GLuint	m_vertexArrayId;
		GLuint	m_vertexBufferId;
		GLuint	m_indexBufferId;
		int m_vertexCount;
		unsigned short	m_width;
		unsigned short	m_height;

		void createVertexBuffer(void);
		void copy(const GBuffer &buffer);
		void drawWindowQuad(void);

	public:
		GBuffer(void);

		bool create(const unsigned short width, const unsigned short height, shared_ptr<Shader> gbufferShader, shared_ptr<Shader> quadShader);

		void beginFrame(const sf::Color &color);
		void drawGeometry(void);
		void drawLight(void);
		void drawToMainBuffer(GBufferTextures bufferTexture = GBufferTextures::FinalTarget);
		void endFrame(void);

		void destroy(void);

		const GLuint &getTexture(GBufferTextures bufferTexture) const
		{
			return m_textures[bufferTexture];
		}
	};
};
