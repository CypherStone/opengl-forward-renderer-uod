#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class CubeDepthTexture
	{
	public:
		enum CubeFace
		{
			Left,
			Right,
			Up,
			Down,
			Forward,
			Backward,
			CubeFaceCount
		};

	private:
		GLint m_id;
		GLuint m_fbo;
		GLuint m_depthBuffer;
		GLuint m_depthTexture;
		vec2 m_size;

	public:
		CubeDepthTexture(void);

		bool create(int width, int height);
		void begin(CubeFace face);
		void end(void);
		void destroy(void);

		void setTextureID(GLint id);

		GLuint & getTexture(void);
		GLint getTextureID(void);
	};
}
