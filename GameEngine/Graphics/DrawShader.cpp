#include "stdafx.h"

namespace CypherEngine
{
	DrawShader::DrawShader(const string & name, const string & vertexShader, const string & fragmentShader) : Shader(name)
	{
		m_vertexShader = 0;
		m_fragmentShader = 0;
		m_vertexFile = vertexShader;
		m_fragmentFile = fragmentShader;
		m_created = loadFromFile(vertexShader, fragmentShader);
	}

	DrawShader::~DrawShader(void)
	{
		destroy();
	}

	void DrawShader::destroy(void)
	{
		if (m_fragmentShader > 0)
		{
			glDetachShader(m_program, m_fragmentShader);
			glDeleteShader(m_fragmentShader);
			m_fragmentShader = 0;
		}

		if (m_vertexShader > 0)
		{
			glDetachShader(m_program, m_vertexShader);
			glDeleteShader(m_vertexShader);
			m_vertexShader = 0;
		}

		Shader::destroy();
		m_vertexShader = 0;
		m_fragmentShader = 0;
	}

	bool DrawShader::loadFromFile(const string &vertexShader, const string &fragmentShader)
	{
		initialise();

		string vertex = compileFromFile(vertexShader);
		string fragment = compileFromFile(fragmentShader);

		if (vertex == "NO_FILE" || fragment == "NO_FILE")
		{
			return false;
		}

		if (!compileFromMemory(vertex, fragment))
		{
			return false;
		}

		return true;
	}

	/* Private Functions */

	bool DrawShader::compileFromMemory(const string &vertexData, const string &fragmentData)
	{
		if (!vertexData.empty())
		{
			if (!compileShader(m_vertexShader, vertexData, GL_VERTEX_SHADER))
			{
				return false;
			}
		}

		if (!fragmentData.empty())
		{
			if (!compileShader(m_fragmentShader, fragmentData, GL_FRAGMENT_SHADER))
			{
				return false;
			}
		}

		bool linked =  linkShader();

		if (m_fragmentShader > 0)
		{
			glDetachShader(m_program, m_fragmentShader);
			glDeleteShader(m_fragmentShader);
			m_fragmentShader = 0;
		}

		if (m_vertexShader > 0)
		{
			glDetachShader(m_program, m_vertexShader);
			glDeleteShader(m_vertexShader);
			m_vertexShader = 0;
		}

		return linked;
	}
}