#include "stdafx.h"
#include "RenderEngine.h"

namespace CypherEngine
{
	RenderEngine * RenderEngine::m_instance = nullptr;

	/* Constructor Functions */

	RenderEngine::RenderEngine(void)
	{

	}

	RenderEngine::~RenderEngine(void)
	{

	}

	RenderEngine * RenderEngine::getInstance(void)
	{
		if (!m_instance)
		{
			m_instance = new RenderEngine();
		}

		return m_instance;
	}

	/* Public Functions */

	bool RenderEngine::create(const unsigned short &width, const unsigned short &height)
	{
		if (glewInit() != GLEW_OK)
		{
			return false;
		}

		m_system = System::getInstance();
		m_shaderManager = ShaderManager::getInstance();

		if (!m_shaderManager->create(m_system->getFileStructure().shaders + "Shaders.xml"))
		{
			return false;
		}

		if (!m_gbuffer.create(width, height, m_shaderManager->getShader("GBufferFinal"), m_shaderManager->getShader("QuadRender")))
		{
			return false;
		}

		if (!m_depthTexture.create(width, height))
		{
			return false;
		}

		if (!m_forwardPlus.create(glm::vec2(width, height)))
		{
			return false;
		}

		m_method = DrawMethod::FPlus;

		return true;
	}

	void RenderEngine::destroy(void)
	{
		if (m_instance)
		{
			m_instance->m_gbuffer.destroy();
			m_instance->m_depthTexture.destroy();
			m_instance->m_forwardPlus.destroy();

			if (m_instance->m_shaderManager)
			{
				m_instance->m_shaderManager->destroy();
				m_instance->m_shaderManager = nullptr;
			}

			delete m_instance;
			m_instance = nullptr;
		}
	}

	void RenderEngine::draw(Camera & camera, LightManager & lights, vector<GameObject *> & objects)
	{
		m_system->getSystemStatistics().startMeasurement("Full Render");

		lights.bindPointLights();

		if (m_method == DrawMethod::Deferred)
		{
			drawShadows(lights, objects);
			drawObjects(camera, objects);
			drawLights(camera, lights);

			drawToBuffer();
		}
		else if (m_method == DrawMethod::FPlus)
		{
			m_system->getSystemStatistics().startMeasurement("Depth Render");
			drawSceneDepth(camera, objects);
			m_system->getSystemStatistics().endMeasurement("Depth Render");

			drawForwardPlus(camera, lights, objects);
		}

		m_system->getSystemStatistics().endMeasurement("Full Render");
	}

	/* Private Functions */

	void RenderEngine::drawShadows(LightManager & lights, vector<GameObject *> & objects)
	{
		auto shader = m_shaderManager->activateShader("DepthRender");
		auto depthVP = mat4();
		auto depthMVP = mat4();

		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		vec3 target[CubeDepthTexture::CubeFace::CubeFaceCount] =
		{
			vec3(1.0f, 0.0f, 0.0f),
			vec3(-1.0f, 0.0f, 0.0f),
			vec3(0.0f, -1.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f),
			vec3(0.0f, 0.0f, 1.0f),
			vec3(0.0f, 0.0f, -1.0f)
		};

		vec3 up[CubeDepthTexture::CubeFace::CubeFaceCount] =
		{
			vec3(0.0f, -1.0f, 0.0f),
			vec3(0.0f, -1.0f, 0.0f),
			vec3(0.0f, 0.0f, -1.0f),
			vec3(0.0f, 0.0f, 1.0f),
			vec3(0.0f, -1.0f, 0.0f),
			vec3(0.0f, -1.0f, 0.0f)
		};

		for (auto & light : (lights.getLights()))
		{
			depthVP = light->getProjectionMatrix() * light->getViewMatrix();

			auto directionalLight = dynamic_cast<DirectionalLight *>(light.get());
			auto pointlight = dynamic_cast<PointLight *>(light.get());
			auto spotlight = dynamic_cast<SpotLight *>(light.get());
			
			if (!pointlight)
			{
				/* Activate the correct shadow map to draw to. */
				if (directionalLight)
				{
					directionalLight->getShadow().begin();
				}
				else if (spotlight)
				{
					spotlight->getShadow().begin();
				}

				/* Draw all objects with no materials. */
				for (auto & obj : objects)
				{
					depthMVP = depthVP * obj->getModelMatrix();
					shader->bindUniformMatrix4("depthMVP", depthMVP);
					obj->draw(shader, false);
				}

				/* End the shadow map drawing and take a copy of the matrix. */
				if (directionalLight)
				{
					directionalLight->getShadow().end();
					directionalLight->getLightProperties().shadowMatrix = depthVP;
				}
				else if (spotlight)
				{
					spotlight->getShadow().end();
					spotlight->getLightProperties().shadowMatrix = depthVP;
				}
			}
			else
			{
				/* Point Lights have a cube shadow map so the rendering technique is different. */

				for (int j = 0; j < CubeDepthTexture::CubeFace::CubeFaceCount; j++)
				{
					depthVP = (pointlight->getProjectionMatrix() * pointlight->getViewMatrix(vec3(), target[j], up[j]));

					pointlight->getShadow().begin((CubeDepthTexture::CubeFace)j);
					for (auto & obj : objects)
					{
						depthMVP = depthVP * obj->getModelMatrix();
						shader->bindUniformMatrix4("depthMVP", depthMVP);
						obj->draw(shader, false);
					}
					pointlight->getShadow().end();
				}
			}
		}

		m_shaderManager->deactivateShader();
		glCullFace(GL_BACK);
		glDisable(GL_CULL_FACE);
	}

	void RenderEngine::drawObjects(Camera & camera, vector<GameObject *> & objects)
	{
		auto shader = m_shaderManager->activateShader("GBuffer");
		m_gbuffer.beginFrame(sf::Color::Black);
		m_gbuffer.drawGeometry();

		shader->bindUniformMatrix4("viewMatrix", camera.getView());
		shader->bindUniformMatrix4("projectionMatrix", camera.getProjection());

		for (auto & obj : objects)
		{
			obj->draw(shader, true);
		}

		m_shaderManager->deactivateShader();
	}

	void RenderEngine::drawLights(Camera &camera, LightManager & lights)
	{
		auto shader = m_shaderManager->activateShader("GBufferFinal");
		shader->bindUniformVector3("cameraPosition", camera.getPosition());
		shader->bindUniformMatrix4("cameraViewMatrix", camera.getView());

		lights.bindLights(*shader);
		m_gbuffer.drawLight();

		m_shaderManager->deactivateShader();
	}

	void RenderEngine::drawToBuffer(void)
	{
		m_shaderManager->activateShader("QuadRender");
		m_gbuffer.drawToMainBuffer();
		m_shaderManager->deactivateShader();
		m_gbuffer.endFrame();
	}

	void RenderEngine::drawSceneDepth(Camera & camera, vector<GameObject *> & objects)
	{
		auto shader = m_shaderManager->activateShader("DepthNormal");
		m_depthTexture.begin();

		shader->bindUniformMatrix4("projectionMatrix", camera.getProjection());
		shader->bindUniformFloat("linearDepthConstant", camera.getLinearDepth());

		unsigned int modelViewID = shader->getUniformLocation("modelViewMatrix");
		for (auto & obj : objects)
		{
			shader->bindUniformMatrix4(modelViewID, camera.getView() * obj->getModelMatrix());
			obj->draw(shader, false);
		}

		m_depthTexture.end();
	}

	void RenderEngine::drawForwardPlus(Camera & camera, LightManager & lights, vector<GameObject *> & objects)
	{
		auto shader = m_shaderManager->activateShader("CullLights");
		m_forwardPlus.cullLights(shader, m_depthTexture, camera, lights);

		shader = m_shaderManager->activateShader("ForwardPlus");
		m_forwardPlus.begin();

		shader->bindUniformVector3("cameraPosition", camera.getPosition());
		shader->bindUniformMatrix4("projectionViewMatrix", camera.getProjection() * camera.getView());

		m_forwardPlus.bindForShading(shader);

		for (auto & obj : objects)
		{
			obj->draw(shader, true);
		}

		m_forwardPlus.end();
		m_shaderManager->deactivateShader();
	}
};