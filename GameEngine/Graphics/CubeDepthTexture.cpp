#include "stdafx.h"
#include "CubeDepthTexture.h"

namespace CypherEngine
{
	/* Constructor Functions*/

	CubeDepthTexture::CubeDepthTexture(void)
	{
		m_id = -1;
	}

	/* Member Functions*/

	bool CubeDepthTexture::create(int width, int height)
	{
		m_size = vec2(width, height);

		glGenFramebuffers(1, &m_fbo);
		if (m_fbo < 0)
		{
			return false;
		}

		glGenTextures(1, &m_depthTexture);
		if (m_depthTexture < 0)
		{
			return false;
		}

		float color[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

		glBindTexture(GL_TEXTURE_CUBE_MAP, m_depthTexture);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

		for (unsigned int i = 0; i < 6; i++)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT16, (GLsizei)m_size.x, (GLsizei)m_size.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
		}

		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

		glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X, m_depthTexture, 0);

		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE)
		{
			std::ostringstream tmp;
			tmp << "Unable to create the cube depth texture.\nError: " << status;
			System::getInstance()->showErrorMessageBox("Error", tmp.str());
			return false;
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		return true;
	}

	void CubeDepthTexture::begin(CubeFace face)
	{
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glPushAttrib(GL_VIEWPORT_BIT);
		glViewport(0, 0, (GLsizei)m_size.x, (GLsizei)m_size.y);
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fbo);
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, m_depthTexture, 0);
		glClear(GL_DEPTH_BUFFER_BIT);  
	}

	void CubeDepthTexture::end(void)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glPopAttrib();
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	}

	void CubeDepthTexture::destroy(void)
	{
		if (m_depthTexture)
		{
			glDeleteTextures(1, &m_depthTexture);
		}

		if (m_depthBuffer)
		{
			glDeleteBuffers(1, &m_depthBuffer);
		}

		if (m_fbo)
		{
			glDeleteFramebuffers(1, &m_fbo);
		}
	}

	/* Set Functions*/

	/* Get Functions*/

	GLuint & CubeDepthTexture::getTexture(void)
	{
		return m_depthTexture;
	}

	GLint CubeDepthTexture::getTextureID(void)
	{
		return m_id;
	}

	/* Set Functions */

	void CubeDepthTexture::setTextureID(GLint id)
	{
		m_id = id;
	}
};