#include "stdafx.h"
#include "ForwardPlus.h"

namespace CypherEngine
{
	ForwardPlus::ForwardPlus()
	{

	}

	ForwardPlus::~ForwardPlus()
	{
		destroy();
	}

	bool ForwardPlus::create(vec2 screenSize)
	{
		destroy();

		m_screenSize = screenSize;

		const unsigned numTiles = getTilesXCount() * getTilesYCount();
		const unsigned maxNumLightsPerTile = calculateMaxLightsPerTile();

		glGenBuffers(1, &m_lightIndexTextureBufferId);
		glBindBuffer(GL_TEXTURE_BUFFER, m_lightIndexTextureBufferId);
		glBufferData(GL_TEXTURE_BUFFER, 4 * maxNumLightsPerTile * numTiles, nullptr, GL_STATIC_DRAW);

		glGenTextures(1, &m_lightIndexTextureId);
		glBindTexture(GL_TEXTURE_BUFFER, m_lightIndexTextureId);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_R32UI, m_lightIndexTextureBufferId);

		glBindImageTexture(1, m_lightIndexTextureId, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);

		return true;
	}

	void ForwardPlus::destroy(void)
	{
		glDeleteBuffers(1, &m_lightIndexTextureBufferId);
		glDeleteTextures(1, &m_lightIndexTextureId);
	}

	unsigned int ForwardPlus::calculateMaxLightsPerTile(void)
	{
		const unsigned int adjustmentMultipier = 32;
		const unsigned int height = (m_screenSize.y > 1080) ? 1080 : (int) m_screenSize.y;

		return (m_maxLights - (adjustmentMultipier * (height / 120)));
	}

	unsigned int ForwardPlus::getTilesXCount() const
	{
		return (unsigned int)((m_screenSize.x + tileResolution - 1) / (float) tileResolution);
	}

	unsigned int ForwardPlus::getTilesYCount() const
	{
		return (unsigned int)((m_screenSize.y + tileResolution - 1) / (float) tileResolution);
	}

	void ForwardPlus::cullLights(shared_ptr<Shader> shader, DepthTexture & texture, Camera & camera, LightManager & lights)
	{
		auto cullShader = dynamic_cast<ComputeShader *>(shader.get());
		if (cullShader)
		{ 
			cullShader->bindUniformMatrix4("invProjection", glm::inverse(camera.getProjection()));
			cullShader->bindUniformMatrix4("viewMatrix", camera.getView());
			cullShader->bindUniformVector2("windowSize", m_screenSize);
			cullShader->bindUniformUInteger("lightCount", lights.getLights().size() & 0xFFFFu);
			cullShader->bindUniformInteger("maxNumLightsPerTile", calculateMaxLightsPerTile());
			cullShader->bindUniformFloat("linearDepthConstant", camera.getLinearDepth());

			glBindImageTexture(0, texture.getTexture(), 0, GL_FALSE, 0, GL_READ_ONLY, GL_R16F);

			cullShader->dispatch(getTilesXCount(), getTilesYCount(), 1);
			glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

			glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R16F);
		}
	}

	void ForwardPlus::begin(void)
	{
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	void ForwardPlus::bindForShading(shared_ptr<Shader> shader)
	{
		shader->bindUniformVector2("windowSize", m_screenSize);
		shader->bindUniformUInteger("maxNumLightsPerTile", calculateMaxLightsPerTile());
	}

	void ForwardPlus::end(void)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
	}

}