#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class ForwardPlus
	{
	private:
		const int tileResolution = 16;
		unsigned int m_lightsPerTile;
		unsigned int m_maxLights = 2048;
		unsigned int m_activeLights = 0;
		vec2 m_screenSize;

		GLuint m_lightIndexTextureBufferId = 0;
		GLuint m_lightIndexTextureId = 0;

	public:
		ForwardPlus(void);
		~ForwardPlus(void);

		bool create(vec2 screenSize);
		void destroy(void);

		unsigned int calculateMaxLightsPerTile(void);
		unsigned int getTilesXCount() const;
		unsigned int getTilesYCount() const;
		void begin(void);
		void cullLights(shared_ptr<Shader> cullShader, DepthTexture & texture, Camera & camera, LightManager & lights);
		void bindForShading(shared_ptr<Shader> shader);
		void end(void);
		
	};
}
