#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class DrawShader : public Shader
	{
	protected:
		string m_vertexFile;
		string m_fragmentFile;
		GLuint m_vertexShader;
		GLuint m_fragmentShader;

		bool loadFromMemory(const string& vertexShader, const string& fragmentShader);
		bool compileFromMemory(const string &vertexData, const string &fragmentData);

	public:
		DrawShader(const string & name, const string & vertexShader, const string & fragmentShader);
		~DrawShader(void);

		bool loadFromFile(const string& vertexShader, const string& fragmentShader);

		void destroy();
	};
};