#pragma once
#include "stdafx.h"
#include "Utilities/SystemStatistics.h"

namespace CypherEngine
{
	class System
	{
	public:
		struct FileStructure
		{
			string path;
			string fonts;
			string models;
			string scenes;
			string shaders;
			string textures;
			string screenshots;
			string statistics;
		};

		enum MessageBoxType
		{
			MBT_OK,
			MBT_OKCANCEL,
			MBT_RETRYCANCEL,
			MBT_YESNO,
			MBT_YESNOCANCEL,
			MBT_ABORTRETRYIGNORE,
			MBT_CANCELTRYCONTINUE
		};

		enum MessageBoxValue
		{
#ifdef CE_WIN
			MBV_ABORT = 3,
			MBV_CANCEL = 2,
			MBV_CONTINE = 11,
			MBV_IGNORE = 5,
			MBV_NO = 7,
			MBV_OK = 1,
			MBV_RETRY = 4,
			MBV_TRYAGAIN = 10,
			MBV_YES = 6
#else
			MBV_ABORT = 3,
			MBV_CANCEL = 2,
			MBV_CONTINE = 11,
			MBV_IGNORE = 5,
			MBV_NO = 7,
			MBV_OK = 1,
			MBV_RETRY = 4,
			MBV_TRYAGAIN = 10,
			MBV_YES = 6
#endif
		};

	private:

		enum WindowStyles
		{
			WS_DEFAULT,
			WS_FULLSCREEN,
			WS_SPLASH
		};

		static System * m_instance;

		sf::RenderWindow m_window;
		SystemStatistics m_statistics;
		vector<sf::VideoMode> m_fullscreenVideoModes;
		sf::VideoMode m_currentVideoMode;		

		vector<shared_ptr<tgui::Gui>> m_guis;

		FileStructure m_structure;

		bool consoleVisible;
		bool running;
		bool created;
		bool m_vsync;
		bool fullscreen;
		bool allowUpdate;

		float timeScale;

		System(void);
		~System(void);

		sf::Uint32 createWindowStyle(WindowStyles style);

		bool parseFilepath(const tinyxml2::XMLElement * data, const string & name, const string & path, string & storage);

	public:
		static System * getInstance(void);
		static void destroy(void);

		static void writeDebugString(string text);

		bool create(string title, glm::ivec2 resolution, bool vsync, bool fullscreen);
		bool parseFileStructure(const string & file);
		void update(void);

		MessageBoxValue showMessageBox(const string &title, const string &message, MessageBoxType type = MBT_OK);
		void showErrorMessageBox(const string &title, const string &message);
		void showConsoleWindow(void);

		bool isVSyncActive(void);
		bool isFullscreen(void);
		bool isRunning(void);
		bool hasFocus(void);

		SystemStatistics & getSystemStatistics(void);
		sfx::FrameClock & getFrameClock(void);
		vector<sf::VideoMode> & getFullscreenModes(void);
		sf::VideoMode & getCurrentVideoMode(void);
		sf::RenderWindow & getWindow(void);
		const FileStructure & getFileStructure(void);

		void setVSync(bool tmpVSync);
		void setRunning(bool run);
		void setFullscreen(void);

		void attachGUI(shared_ptr<tgui::Gui> gui);
		void dettachGUIs(void);

		static void close(void);
		static void takeScreenshot(void);
	};
};