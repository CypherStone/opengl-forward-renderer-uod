#include "stdafx.h"
#include "SponzaMapScript.h"


SponzaMapScript::SponzaMapScript() : Script("SponzaMapScript", "Scene2")
{

}

void SponzaMapScript::create(void)
{
	pointLight = ScriptManager::createLightVector();
	map = SceneManager::attachModelToScene("sponza_map", "sponza");

	int lights = 2048;
	for (int i = 0; i < lights; i++)
	{
		pointLight->push_back(SceneManager::attachLightToScene(SceneManager::Point));
	}
}

void SponzaMapScript::loaded(void)
{	
	for (auto & point : (*pointLight))
	{
		auto pLight = dynamic_cast<PointLight *>(point.get());
		auto prop = pLight->getLightProperties();
		prop.position = vec4(Common::randomFloat(-1300.0f, 1300.0f), Common::randomFloat(10.0f, 1100.0f), Common::randomFloat(-550.0f, 550.0f), Common::randomFloat(100, 250));
		prop.colour = Common::randomVec4(0.2f, 0.8f);
		pLight->setLightProperties(prop);
	}

	auto camera = SceneManager::getCameraFromScene();
	camera.setPosition(vec3(0.0f, 500.0f, 0.0f));
	camera.setRotation(vec3(0.0f, 90.0f, 0.0f));
	SceneManager::setCameraToScene(camera);
}

void SponzaMapScript::update(float delta)
{

}

void SponzaMapScript::drawGUI(void)
{

}