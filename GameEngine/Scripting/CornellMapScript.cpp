#include "stdafx.h"
#include "CornellMapScript.h"


CornellMapScript::CornellMapScript() : Script("CornellMapScript", "Scene1")
{

}

void CornellMapScript::create(void)
{
	pointLight = ScriptManager::createLightVector();
	map = SceneManager::attachModelToScene("cornell_map", "cornell");
	torus = SceneManager::attachModelToScene("torus", "cornell");
	cube = SceneManager::attachModelToScene("cube", "cornell");

	int lights = 2;
	for (int i = 0; i < lights; i++)
	{
		pointLight->push_back(SceneManager::attachLightToScene(SceneManager::Point));
	}
}

void CornellMapScript::loaded(void)
{
	torus->setPosition(vec3(100, 0, 0));
	torus->setRotation(vec3(90, 0.0f, 0.0f));

	cube->setPosition(vec3(-150.0f, -200.0f, -150.0f));
	cube->setRotation(vec3(0, 45.0f, 0.0f));
	
	auto light = (PointLight *)pointLight->at(0).get();
	auto lightProp = light->getLightProperties();
	lightProp.position = vec4(0.0f, 200.0f, 0.0f, 1000.0f);
	lightProp.colour = vec4(1.0f, 0.8f, 0.3f, 1.0f);
	light->setLightProperties(lightProp);

	light = (PointLight *)pointLight->at(1).get();
	lightProp = light->getLightProperties();
	lightProp.position = vec4(-100.0f, -50.0f, 100.0f, 500.0f);
	lightProp.colour = vec4(0.2f, 0.2f, 1.0f, 1.0f);
	light->setLightProperties(lightProp);
}

void CornellMapScript::update(float delta)
{
	auto rotation = torus->getRotation();
	rotation.x += 1.0f * delta;
	rotation.y += 1.0f * delta;
	torus->setRotation(rotation);
}

void CornellMapScript::drawGUI(void)
{

}