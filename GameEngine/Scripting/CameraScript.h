#pragma once
#include "stdafx.h"
#include "System.h"

using namespace CypherEngine;

class CameraScript : Script
{
private:
	System * m_system;
	shared_ptr<Camera> camera;
	bool lockMouse;
	bool usedMouse;

public:
	CameraScript();

	virtual void create(void);
	virtual void loaded(void);
	virtual void update(float delta);
	virtual void updateInput(float delta);
};

