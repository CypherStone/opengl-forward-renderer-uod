#pragma once
#include "stdafx.h"

using namespace CypherEngine;

class CornellMapScript : CypherEngine::Script
{
private:
	shared_ptr<GameObject> map;
	shared_ptr<GameObject> torus;
	shared_ptr<GameObject> cube;
	shared_ptr<vector<shared_ptr<Light>>> pointLight;

public:

	CornellMapScript();

	virtual void create(void);
	virtual void loaded(void);
	virtual void update(float delta);
	virtual void drawGUI(void);
};

