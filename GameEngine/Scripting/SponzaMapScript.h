#pragma once
#include "stdafx.h"

using namespace CypherEngine;

class SponzaMapScript : CypherEngine::Script
{
private:
	shared_ptr<GameObject> map;
	shared_ptr<vector<shared_ptr<Light>>> pointLight;

public:

	SponzaMapScript();

	virtual void create(void);
	virtual void loaded(void);
	virtual void update(float delta);
	virtual void drawGUI(void);
};

