#include "stdafx.h"
#include "ScriptStorage.h"

namespace CypherEngine
{
	ScriptStorage::ScriptStorage(void)
	{
		/* Place all scripts here. */
		new DebugScript();
		new CameraScript();
		new CornellMapScript();
		new SponzaMapScript();
	}
}