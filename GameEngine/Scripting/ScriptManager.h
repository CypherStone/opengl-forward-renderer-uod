#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class ScriptManager
	{
	private:
		static ScriptManager * m_instance;
		vector<Script *> m_scripts;

		ScriptManager(void);
		~ScriptManager(void);

	public:
		/* Public Functions */
		void destroy(void);
		void resetScripts(void);

		static shared_ptr<vector<shared_ptr<Light>>> createLightVector(void);
		static shared_ptr<Camera> createCamera(void);

		/* Get Functions */
		static ScriptManager * getInstance(void);
		static bool exists(void);
		vector<Script *> getScriptsForScene(string scene);

		/* Set Functions */
		void attachScript(Script * script);
	};
}

