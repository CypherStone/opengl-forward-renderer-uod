#include "stdafx.h"
#include "ScriptStorage.h"

DebugScript::DebugScript(void) : Script("DebugScript", "Scene_ALL")
{

}

void DebugScript::create(void)
{
	screenshot = true;
	debugReleased = true;
	reloading = false;
	debugVisible = true;

	system = System::getInstance();
	gui = SceneManager::attachGUI();
	
	guiFont = SceneManager::attachFontToScene("debugfont");
	debugdisplay = make_shared<tgui::TextBox>();
	framedisplay = make_shared<tgui::TextBox>();
}

void DebugScript::loaded(void)
{
	system->getSystemStatistics().getFrameClock().clear();
}

void DebugScript::loadedGUI(void)
{
	gui->setGlobalFont(guiFont);

	debugdisplay = tgui::TextBox::create(GUI_THEME);
	debugdisplay->setPosition(10, 30);
	debugdisplay->setSize(350, 300);
	debugdisplay->setTextSize(12);
	debugdisplay->setReadOnly(true);

	framedisplay = tgui::TextBox::create(GUI_THEME);
	framedisplay->setPosition(10, 370);
	framedisplay->setSize(350, 300);
	framedisplay->setTextSize(12);
	framedisplay->setReadOnly(true);

	gui->add(debugdisplay);
	gui->add(framedisplay);
}

void DebugScript::update(float delta)
{

}

void DebugScript::updateInput(float delta)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		System::close();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::F1) && debugReleased)
	{
		debugVisible = !debugVisible;
		debugReleased = false;
	}
	else if (!sf::Keyboard::isKeyPressed(sf::Keyboard::F1))
	{
		debugReleased = true;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::R) && !reloading)
	{
		ShaderManager::reloadShaders();
		reloading = true;
	}
	else if (!sf::Keyboard::isKeyPressed(sf::Keyboard::R))
	{
		reloading = false;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::F2) && !screenshot)
	{
		System::takeScreenshot();
		screenshot = true;
	}
	else if (!sf::Keyboard::isKeyPressed(sf::Keyboard::F2))
	{
		screenshot = false;
	}
}

void DebugScript::updateGUI(float delta)
{
	if (debugVisible)
	{
		auto camera = SceneManager::getCameraFromScene();
		std::ostringstream tmp;
		tmp << "\n\nCamera Position: x - " << camera.getPosition().x << " y - " << camera.getPosition().y << " z - " << camera.getPosition().z;
		
		debugdisplay->setText(system->getSystemStatistics().getSystemInformation() + tmp.str());
		framedisplay->setText(system->getSystemStatistics().getFrameInformation());
	}
}

void DebugScript::drawGUI(void)
{
	if (debugVisible)
	{
		gui->draw();
	}
}
