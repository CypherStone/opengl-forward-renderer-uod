#include "stdafx.h"
#include "Script.h"

namespace CypherEngine
{
	Script::Script(string name, string scene)
	{
		m_name = name;
		m_scene = scene;
		ScriptManager::getInstance()->attachScript(this);
		System::writeDebugString("Script: " + name + " has attached to script manager.");
	}

	/* Public Functions */

	/* Get Functions */

	string Script::getScriptName(void)
	{
		return m_name;
	}

	string Script::getSceneName(void)
	{
		return m_scene;
	}

	bool Script::isActive(void)
	{
		return m_active;
	}

	/* Set Functions */
	void Script::setActive(bool active)
	{
		m_active = active;
	}

	/* Private Functions */

}