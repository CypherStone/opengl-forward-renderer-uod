#pragma once
#include "stdafx.h"
#include "System.h"

#define GUI_THEME "../Library/TGUI-0.6/widgets/Black.conf"

using namespace CypherEngine;

class DebugScript : CypherEngine::Script
{
private:
	bool screenshot;
	bool debugVisible;
	bool debugReleased;
	bool reloading;
	System * system;
	shared_ptr<sf::Font> guiFont;
	shared_ptr<tgui::TextBox> debugdisplay;
	shared_ptr<tgui::TextBox> framedisplay;
	shared_ptr<tgui::Button> vsyncbutton;
	shared_ptr<tgui::Gui> gui;

public:
	DebugScript();

	virtual void create(void);
	virtual void loaded(void);
	virtual void loadedGUI(void);
	virtual void update(float delta);
	virtual void updateInput(float delta);
	virtual void updateGUI(float delta);
	virtual void drawGUI(void);
};
