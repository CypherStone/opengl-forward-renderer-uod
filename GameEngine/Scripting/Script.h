#pragma once
#include "stdafx.h"

namespace CypherEngine
{
	class Script
	{
	private:		
		string m_name;
		string m_scene;
		bool m_active;

	public:
		
		Script(string name, string);

		/* Public Functions */
		virtual void create(void) { };
		virtual void loaded(void) { };
		virtual void loadedGUI(void) { };
		virtual void update(float delta) { };
		virtual void updateInput(float delta) { };
		virtual void updateGUI(float delta) { };
		virtual void drawGUI(void) { };
		virtual void destroy(void) { };
		virtual void reset(void) { };

		/* Get Functions */
		string getScriptName(void);
		string getSceneName(void);
		bool isActive(void);

		/* Set Functions */
		void setActive(bool active);
	};
}