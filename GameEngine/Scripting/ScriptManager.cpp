#include "stdafx.h"
#include "ScriptManager.h"

namespace CypherEngine
{
	ScriptManager * ScriptManager::m_instance = nullptr;

	/* Constructor Functions */

	ScriptManager::ScriptManager(void)
	{

	}

	ScriptManager::~ScriptManager(void)
	{

	}

	ScriptManager * ScriptManager::getInstance(void)
	{
		if (!m_instance)
		{
			m_instance = new ScriptManager();
		}

		return m_instance;
	}

	/* Public Functions */

	void ScriptManager::destroy(void)
	{
		for (auto & script : m_scripts)
		{
			delete script;
		}
	}

	void ScriptManager::resetScripts(void)
	{
		for (auto & script : m_scripts)
		{
			script->reset();
		}
	}

	shared_ptr<vector<shared_ptr<Light>>> ScriptManager::createLightVector(void)
	{
		return make_shared<vector<shared_ptr<Light>>>();
	}

	shared_ptr<Camera> ScriptManager::createCamera(void)
	{
		return make_shared<Camera>();
	}

	/* Get Functions */

	vector<Script *> ScriptManager::getScriptsForScene(string scene)
	{
		vector<Script *> scripts;
		for (const auto & script : m_scripts)
		{
			if (scene == script->getSceneName() || script->getSceneName() == "Scene_ALL")
			{
				scripts.push_back(script);
			}
		}

		return scripts;
	}

	bool ScriptManager::exists(void)
	{
		return (m_instance != nullptr);
	}

	/* Set Functions */

	void ScriptManager::attachScript(Script * scriptToAttach)
	{
		bool add = true;

		for (const auto & script : m_scripts)
		{
			if (scriptToAttach->getScriptName() == script->getScriptName())
			{
				add = false;
				System::getInstance()->showErrorMessageBox("Scripting Error", "Script " + scriptToAttach->getScriptName() + " already exists.");
				return;
			}
		}

		if (add)
		{
			m_scripts.push_back(scriptToAttach);
		}
	}	
}
