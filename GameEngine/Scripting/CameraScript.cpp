#include "stdafx.h"
#include "CameraScript.h"

CameraScript::CameraScript() : Script("CameraScript", "Scene_ALL")
{

}

void CameraScript::create(void)
{
	camera = ScriptManager::createCamera();
	m_system = System::getInstance();
	lockMouse = false;
	usedMouse = true;
	m_system->getWindow().setMouseCursorVisible(!lockMouse);
}

void CameraScript::loaded(void)
{
	auto size = m_system->getWindow().getSize();
	auto tmpCamera = SceneManager::getCameraFromScene();
	camera->setPosition(tmpCamera.getPosition());
	camera->setRotation(tmpCamera.getRotation());
	camera->setActive(tmpCamera.isActive());
	camera->setFieldOfView(90.0f);
	camera->setScreenSize(vec2(size.x, size.y));
	SceneManager::setCameraToScene(*camera.get());

	camera->update();
}

void CameraScript::update(float delta)
{

}

void CameraScript::updateInput(float delta)
{
	int controller = 1;
	auto tmpCamera = SceneManager::getCameraFromScene();
	camera->setPosition(tmpCamera.getPosition());
	camera->setRotation(tmpCamera.getRotation());
	camera->setActive(tmpCamera.isActive());

	auto mouseSpeed = 0.5f;
	auto speed = sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Joystick::isButtonPressed(controller, 4) ? 500.0f : 100.0f;
	auto fov = camera->getFOV();
	auto rotation = camera->getRotation();
	auto position = camera->getPosition();
	auto screensize = camera->getScreenSize();
	auto windowPos = m_system->getWindow().getPosition();

	if (lockMouse)
	{
		rotation.y += mouseSpeed * delta * float((windowPos.x + screensize.x / 2) - sf::Mouse::getPosition().x);
		rotation.x += mouseSpeed * delta * float((windowPos.y + screensize.y / 2) - sf::Mouse::getPosition().y);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Add))
	{
		if (fov < 120.0f)
		{
			fov += 5.0f * delta;
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract))
	{
		if (fov > 60.0f)
		{
			fov -= 5.0f * delta;
		}
	}

	const auto direction = glm::vec3(cos(rotation.x) * sin(rotation.y), sin(rotation.x),	cos(rotation.x) * cos(rotation.y));
	const auto right = glm::vec3(sin(rotation.y - 3.14f / 2.0f), 0,	cos(rotation.y - 3.14f / 2.0f));

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Joystick::getAxisPosition(controller, sf::Joystick::Y) < -50.0f)
	{
		position += direction * speed * delta;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Joystick::getAxisPosition(controller, sf::Joystick::Y) > 50.0f)
	{
		position -= direction * speed * delta;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Joystick::getAxisPosition(controller, sf::Joystick::X) < -50.0f)
	{
		position -= right * speed * delta;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Joystick::getAxisPosition(controller, sf::Joystick::X) > 50.0f)
	{
		position += right * speed * delta;
	}

	camera->setPosition(position);
	camera->setRotation(rotation);
	camera->setUp(glm::cross(right, direction));
	camera->setFieldOfView(fov);
	camera->setMovement(direction);

	if (lockMouse)
	{
		sf::Mouse::setPosition(sf::Vector2i((int)(windowPos.x + screensize.x / 2), (int)(windowPos.y + screensize.y / 2)));
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && usedMouse)
	{
		lockMouse = !lockMouse;
		if (lockMouse)
		{
			sf::Mouse::setPosition(sf::Vector2i((int)(windowPos.x + screensize.x / 2), (int)(windowPos.y + screensize.y / 2)));
		}

		m_system->getWindow().setMouseCursorVisible(!lockMouse);

		usedMouse = false;
	}
	else if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		usedMouse = true;
	}

	camera->update();

	SceneManager::setCameraToScene(*camera.get());
}
