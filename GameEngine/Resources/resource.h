//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GameEngine.rc
//
#define IDC_MYICON                      2
#define IDD_GAMEENGINE_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_GAMEENGINE                  107
#define IDI_SMALL                       108
#define IDC_GAMEENGINE                  109
#define IDR_MAINFRAME                   128
#define IDC_STATIC                      -1

// N default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_N_RESOURCE_VALUE        129
#define _APS_N_COMMAND_VALUE         32771
#define _APS_N_CONTROL_VALUE         1000
#define _APS_N_SYMED_VALUE           110
#endif
#endif
