#pragma once

#if defined(_MSC_VER)
#   pragma warning (disable:4503) // decorated name length exceeded
#endif

#if defined(_WIN32) | defined(_WIN64)
#define CE_WIN
#elif defined(_LINUX) | defined(_UNIX)
#define CE_LNX
#endif

#define BUFFER_OFFSET(i) ((char *) NULL + (i))

#define gl

#define MODEL_TEXTURE_START 10
#define SHADOW_TEXTURE_START MODEL_TEXTURE_START + 5

#include "Resources/targetver.h"

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <algorithm>
#include <time.h> 
#include <math.h>
#include <VersionHelpers.h>
#include <Psapi.h>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <map>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <memory>
#include <chrono>
#include <ctime>

#include <glew.h>
#include <gl/gl.h>
#include <gl/glu.h>

#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <tinyxml2.h>
#include <tgui/TGUI.hpp>

/* Engine Namespace */

namespace CypherEngine
{
	/* Alias for CypherEngine namespace */

	namespace CE = CypherEngine;

	using std::string;
	using std::vector;
	using std::unordered_map;
	using std::map;
	using std::pair;
	using std::thread;
	using std::mutex;
	using std::make_shared;
	using std::make_unique;
	using std::shared_ptr;
	using std::unique_ptr;

	using sf::Color;

	using glm::vec2;
	using glm::vec3;
	using glm::vec4;
	using glm::mat4;

	struct MeshMaterial
	{
		string materialName, diffuseTexture, normalTexture, specularTexture, opacityTexture; 
		Color ambient, diffuse, specular;
		float specularItensity, alpha, illumination;	
	};

	struct Vertex
	{
		vec3 position;
		vec3 normal;
		vec3 binormal;
		vec3 tangent;
		vec2 texel;
	};

	struct Vertex2D
	{
		vec3 position;
		vec2 texel;
	};

	enum ModelFormat
	{
		OBJ = 0,
		MD2 = 1
	};

	enum GBufferTextures
	{
		PositionTarget,
		DiffuseTarget,
		NormalsTarget,
		SpecularTarget,
		FinalTarget,
		DrawTargetCount = 4,
		TotalTargets = 5
	};

	struct DirectionalLightShaderBuffer
	{
		glm::mat4 shadowMatrix;
		vec4 colour = glm::vec4(1.0f);
		vec4 direction;
	};

	struct PointLightShaderBuffer
	{
		vec4 colour = glm::vec4(1.0f);
		vec4 position;
	};

	struct SpotLightShaderBuffer
	{
		glm::mat4 shadowMatrix;
		vec4 colour = glm::vec4(1.0f);
		vec4 position;
		vec4 direction;
	};

};

#include "Graphics/CubeDepthTexture.h"
#include "Graphics/DepthTexture.h"

#include "Objects/Camera.h"
#include "Objects/Lights/Light.h"
#include "Objects/Lights/SpotLight.h"
#include "Objects/Lights/PointLight.h"
#include "Objects/Lights/DirectionalLight.h"
#include "Objects/GameObjects/Mesh.h"
#include "Objects/GameObjects/GameObject.h"

#include "Graphics/Shader.h"
#include "Graphics/ComputeShader.h"
#include "Graphics/DrawShader.h"
#include "Graphics/GBuffer.h"

#include "Managers/Manager.h"
#include "Managers/TextureManager.h"
#include "Managers/GameObjectManager.h"
#include "Managers/FontManager.h"
#include "Managers/ResourceManager.h"
#include "Managers/LightManager.h"
#include "Graphics/ForwardPlus.h"
#include "Managers/ShaderManager.h"
#include "Graphics/RenderEngine.h"

#include "Utilities/Common.h"
#include "Utilities/ModelLoader.h"
#include "Utilities/PerformanceMeasurement.h"

#include "Scripting/Script.h"
#include "Scripting/ScriptStorage.h"
#include "Scripting/ScriptManager.h"
#include "Scenes/Scene.h"
#include "Scenes/SceneManager.h"

#include "Utilities/FrameClock.h"
#include "Utilities/SystemStatisticsWin.h"
#include "Utilities/SystemStatisticsImpl.h"
#include "Utilities/SystemStatistics.h"

#include "System.h"
#include "GameEngine.h"
